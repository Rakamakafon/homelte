<?$APPLICATION->IncludeComponent(
	"intec.universe:main.rates",
	"template.2",
	Array(
		"BUTTON_MODE" => "order",
		"BUTTON_SHOW" => "Y",
		"BUTTON_TEXT" => "Подробнее",
		"CACHE_TIME" => "0",
		"CACHE_TYPE" => "A",
		"COLUMNS" => "4",
		"COMPONENT_TEMPLATE" => "template.2",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"COUNTER_SHOW" => "N",
		"DESCRIPTION_POSITION" => "center",
		"DESCRIPTION_SHOW" => "Y",
		"DESCRIPTION_TEXT" => "Мы предлагаем широкий выбор тарифов от различных операторов",
		"ELEMENTS_COUNT" => "",
		"HEADER_POSITION" => "center",
		"HEADER_SHOW" => "Y",
		"HEADER_TEXT" => "Тарифы",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "content",
		"LAZYLOAD_USE" => "N",
		"ORDER_BUTTON" => "Заказать",
		"ORDER_BY" => "ASC",
		"ORDER_CONSENT" => "#SITE_DIR#company/consent/",
		"ORDER_FORM_CONSENT" => "#SITE_DIR#company/consent/",
		"ORDER_FORM_FIELD" => "form_text_20",
		"ORDER_FORM_ID" => "6",
		"ORDER_FORM_TEMPLATE" => ".default",
		"ORDER_FORM_TITLE" => "Заказать",
		"ORDER_USE" => "Y",
		"PREVIEW_SHOW" => "Y",
		"PRICE_LIST" => "",
		"PRICE_LIST_BUTTON_TEXT" => "",
		"PRICE_SHOW" => "Y",
		"PROPERTIES_SHOW" => "Y",
		"PROPERTY_CURRENCY" => "CURRENCY",
		"PROPERTY_DETAIL_URL" => "",
		"PROPERTY_DISCOUNT" => "",
		"PROPERTY_LIST" => array(0=>"G4_LTE",1=>"G3_SPEED",2=>"UNLIM_TRAFFIC",3=>"",),
		"PROPERTY_PRICE" => "PRICE",
		"SECTIONS" => array(0=>"2",1=>"",),
		"SECTION_DESCRIPTION_POSITION" => "center",
		"SECTION_DESCRIPTION_SHOW" => "Y",
		"SETTINGS_USE" => "N",
		"SLIDER_USE" => "Y",
"SLIDER_NAV" => "Y",
		"SORT_BY" => "SORT",
		"TABS_POSITION" => "center",
		"TABS_USE" => "N",
		"TABS_VIEW" => "1",
		"VIEW" => "tabs"
	)
);?> <br>
 <br>
 <style type="text/css">
    .page-company-items {}

    .page-company-items .page-company-item {
        margin-bottom: 15px;
        color: #888;
    }
    .page-company-items .page-company-item-header {
        margin-bottom: 10px;
        line-height: 1;
    }
    .page-company-items .page-company-item-header img,
    .page-company-items .page-company-item-header .page-company-item-picture,
    .page-company-items .page-company-item-title {
        display: inline-block;
        vertical-align: middle;
    }
    .page-company-items .page-company-item-header img,
    .page-company-items .page-company-item-header .page-company-item-picture {
        width: 50px;
        height: 50px;
    }
    .page-company-items .page-company-item-header .page-company-item-picture svg {
        max-width: 100%;
        max-height: 100%;
    }
    .page-company-items .page-company-item-title {
        padding-left: 8px;
        font-size: 40px;
    }
    .page-company-items .page-company-item-description {
        font-size: 14px;
    }

    @media all and (max-width: 768px) {
        .page-company-items .page-company-item-header,
        .page-company-items .page-company-item-description {
            text-align: center;
        }
    }

    @media all and (max-width: 500px) {
        .page-company-items .page-company-item-header,
        .page-company-items .page-company-item-description {
            text-align: left;
        }
    }
</style>
<div class="intec-content">
	<div class="intec-content-wrapper">
		<div class="intec-grid intec-grid-wrap intec-grid-a-v-center intec-grid-a-h-center intec-grid-i-h-25 intec-grid-i-v-10">
			<div class="intec-grid-item intec-grid-item-720-1">
				<h1 class="intec-ui-markup-header intec-ui-markup-h2">О нас</h1>
				<div class="intec-ui-markup-text">
					<p>
						 Наша компания осуществляет подключение и настройку интернета в загородных домах, дачах, офисах и прочих объектов. Так же оказываем услуги по установке и настройке ТВ, видеонаблюдения и усиления связи. Мы работаем в различных регионах России.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var inWindow = function (selector) {
            var $elements = $(selector);
            var scrollTop = $(window).scrollTop();
            var windowHeight = $(window).height();
            var result = [];
            $elements.each(function(){
                var $el = $(this);
                var offset = $el.offset();
                var elHeight = $el.height();
                if (((scrollTop - 50) <= offset.top && (elHeight + offset.top) <= (scrollTop + windowHeight + 50))
                    ||
                    ((scrollTop + 50) >= offset.top && (elHeight + offset.top) >= (scrollTop + windowHeight - 50) )
                ) {
                    result.push(this);
                }
            });
            return $(result);
        };

        var numberAnimate = function (el) {
            var $el = $(el);
            var newNumber = parseInt($el.data('new-number'));
            if (newNumber > 0) {
                $el.prop('number', 0)
                    .animateNumber({
                        number: newNumber,
                        numberStep: $.animateNumber.numberStepFactories.separator(' ')
                    }, 1500);
            }
            $el.addClass('show_animation');
        };

        $(window).scroll(function(){
            inWindow('.page-company-item-header [data-new-number]:not(.show_animation)').each(function(){
                numberAnimate(this);
            });
        });
        inWindow($('.page-company-item-header [data-new-number]').html('0')).each(function(){
            numberAnimate(this);
        });
    });
</script>