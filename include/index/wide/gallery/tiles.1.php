<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

use intec\core\collections\Arrays;
use intec\core\helpers\Html;
use intec\core\io\Path;

/**
 * @var Arrays $blocks
 * @var array $block
 * @var array $data
 * @var string $page
 * @var Path $path
 * @global CMain $APPLICATION
 */

?>
<?= Html::beginTag('div', ['style' => [
    'margin-top' => '-30px',
    'margin-bottom' => '50px',
]]) ?>
    <?php $APPLICATION->IncludeComponent(
	"intec.universe:widget", 
	"photo", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "27",
		"SETTINGS_USE" => "Y",
		"LAZYLOAD_USE" => "N",
		"SHOW_TITLE" => "Y",
		"TITLE" => "Наши работы",
		"ALIGHT_HEADER" => "N",
		"SHOW_DETAIL_LINK" => "N",
		"USE_CAROUSEL" => "Y",
		"COLUMNS_COUNT" => "4",
		"ITEMS_LIMIT" => "20",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"COMPONENT_TEMPLATE" => "photo",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"ROWS_COUNT" => "1"
	),
	false
); ?>
<?= Html::endTag('div') ?>