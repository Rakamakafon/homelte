<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

use intec\core\collections\Arrays;
use intec\core\helpers\Html;
use intec\core\io\Path;

/**
 * @var Arrays $blocks
 * @var array $block
 * @var array $data
 * @var string $page
 * @var Path $path
 * @global CMain $APPLICATION
 */

?>
<?= Html::beginTag('div', ['style' => [
    'margin-top' => '50px',
    'margin-bottom' => '50px',
]]) ?>
    <?php $APPLICATION->IncludeComponent(
	"intec.universe:main.gallery", 
	"template.1", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "27",
		"SETTINGS_USE" => "Y",
		"LAZYLOAD_USE" => "N",
		"HEADER_SHOW" => "Y",
		"HEADER_POSITION" => "center",
		"HEADER_TEXT" => "Наши работы",
		"DESCRIPTION_SHOW" => "N",
		"LINE_COUNT" => "4",
		"ALIGNMENT" => "center",
		"TABS_POSITION" => "center",
		"DELIMITERS" => "Y",
		"FOOTER_SHOW" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"SORT_BY" => "SORT",
		"ORDER_BY" => "ASC",
		"COMPONENT_TEMPLATE" => "template.1",
		"SECTIONS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENTS_COUNT" => "",
		"SECTIONS_ELEMENTS_COUNT" => "",
		"LIST_PAGE_URL" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
); ?>
<?= Html::endTag('div') ?>