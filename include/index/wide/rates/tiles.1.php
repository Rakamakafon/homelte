<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

use intec\core\collections\Arrays;
use intec\core\helpers\Html;
use intec\core\io\Path;

/**
 * @var Arrays $blocks
 * @var array $block
 * @var array $data
 * @var string $page
 * @var Path $path
 * @global CMain $APPLICATION
 */

?>
<?= Html::beginTag('div', ['style' => [
    'margin-top' => '50px',
    'margin-bottom' => '50px'
]]) ?>
    <?php $APPLICATION->IncludeComponent(
	"intec.universe:main.rates", 
	"template.6_custom", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "7",
		"ELEMENTS_COUNT" => "8",
		"PROPERTY_LIST" => array(    // Отображаемые свойства
        0 => "WIFI_MW52",
1 => "ROUTER2",
        2 => "MODEM_PREMIUM_2",
        3 => "MODEM_PREMIUM",
        4 => "PROPERTY_ROUTER",
        5 => "PROPERTY_MODEM",
        6 => "ANTENNA5",
        7 => "ANTENNA6",
        8 => "ANTENNA7",
        9 => "ANTENNA8",
        10 => "PROPERTY_CABLE",
        11 => "PROPERTY_CRONSHTEIN",
        12 => "PROPERTY_PIGTAIL",
        13 => "PROPERTY_MONTAG",
        14 => "PROPERTY_SIM",

    ),
		"PROPERTY_PRICE" => "PRICE",
		"PROPERTY_CURRENCY" => "CURRENCY",
		"PROPERTY_DISCOUNT" => "DISCOUNT",
		"PROPERTY_DISCOUNT_TYPE" => "",
		"PROPERTY_DETAIL_URL" => "",
		"HEADER_SHOW" => "Y",
		"HEADER_POSITION" => "center",
		"HEADER_TEXT" => "Комплекты для интернета в регионе: г. #SOTBIT_REGIONS_NAME#",
		"DESCRIPTION_SHOW" => "N",
		"COLUMNS" => "4",
		"VIEW" => "tabs",
		"TABS_POSITION" => "center",
		"SECTION_DESCRIPTION_SHOW" => "N",
		"SECTION_DESCRIPTION_POSITION" => "center",
		"COUNTER_SHOW" => "N",
		"COUNTER_TEXT" => "ТАРИФ",
		"PRICE_SHOW" => "Y",
		"DISCOUNT_SHOW" => "Y",
		"PREVIEW_SHOW" => "Y",
		"PROPERTIES_SHOW" => "Y",
		"BUTTON_SHOW" => "Y",
		"SLIDER_USE" => "Y",
		"SLIDER_NAV" => "Y",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600000",
		"SORT_BY" => "SORT",
		"ORDER_BY" => "ASC",
		"COMPONENT_TEMPLATE" => "template.6_custom",
		"SECTIONS" => array(
			0 => "1",
			1 => "",
		),
		"BUTTON_TEXT" => "Заказать",
		"BUTTON_MODE" => "order",
		"ORDER_FORM_ID" => "6",
		"ORDER_FORM_TEMPLATE" => ".default",
		"ORDER_FORM_FIELD" => "form_text_20",
		"ORDER_FORM_TITLE" => "Заказать",
		"ORDER_CONSENT" => "#SITE_DIR#company/consent/",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"SLIDER_DOTS" => "N",
		"SLIDER_LOOP" => "N",
		"SLIDER_AUTO_USE" => "N"
	),
	false
); ?>
<?= Html::endTag('div') ?>
<!--<div class="intec-template-title">
                        <div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="intec-header" id="premium">
            Премиум комплекты        </div>
    </div>
</div>                    </div>
<div class="hided" style="display:none;">
<?/*php $APPLICATION->IncludeComponent("intec.universe:main.rates", "template.6_custom", Array(
    "IBLOCK_TYPE" => "content",	// Тип инфоблока
    "IBLOCK_ID" => "7",	// Инфоблок
    "ELEMENTS_COUNT" => "8",	// Кол-во выводимых элементов
    "PROPERTY_LIST" => array(	// Отображаемые свойства
        0 => "ROUTER2",
        1 => "MODEM_PREMIUM_2",
        2 => "MODEM_PREMIUM",
        3 => "ANTENNA5",
        4 => "ANTENNA6",
        5 => "ANTENNA7",
		6 => "ANTENNA8",
        7 => "PROPERTY_CABLE",
        8 => "PROPERTY_CRONSHTEIN",
        9 => "PROPERTY_PIGTAIL",
        10 => "PROPERTY_MONTAG",
        11 => "PROPERTY_SIM",
    ),
    "PROPERTY_PRICE" => "PRICE",	// Свойство "Цена"
    "PROPERTY_CURRENCY" => "CURRENCY",	// Свойство "Валюта"
    "PROPERTY_DISCOUNT" => "DISCOUNT",	// Свойство "Скидка"
    "PROPERTY_DISCOUNT_TYPE" => "",	// Свойство "Тип скидки"
    "PROPERTY_DETAIL_URL" => "",	// Свойство "Ссылка"
    "HEADER_SHOW" => "Y",	// Показывать заголовок блока
    "HEADER_POSITION" => "center",	// Выравнивание текста заголовока блока
    "HEADER_TEXT" => "",	// Текст заголовка блока
    "DESCRIPTION_SHOW" => "N",	// Показывать описание блока
    "COLUMNS" => "4",	// Кол-во элементов в строке
    "VIEW" => "tabs",	// Вид отображения
    "TABS_POSITION" => "center",	// Расположение вкладок
    "SECTION_DESCRIPTION_SHOW" => "N",	// Отображать описание раздела
    "SECTION_DESCRIPTION_POSITION" => "center",
    "COUNTER_SHOW" => "N",	// Отображать счетчик элементов
    "COUNTER_TEXT" => "ТАРИФ",
    "PRICE_SHOW" => "Y",	// Отображать цену
    "DISCOUNT_SHOW" => "Y",	// Отображать "стикер" с величиной скидки
    "PREVIEW_SHOW" => "Y",	// Отображать описание элементов
    "PROPERTIES_SHOW" => "Y",	// Отображать выбранные свойства
    "BUTTON_SHOW" => "Y",	// Отображать кнопку
    "SLIDER_USE" => "N",	// Использовать слайдер
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CACHE_TIME" => "3600000",	// Время кеширования (сек.)
    "SORT_BY" => "SORT",	// Свойство для сортировки
    "ORDER_BY" => "ASC",	// Направление сортировки
    "COMPONENT_TEMPLATE" => "template.1",
    "SECTIONS" => array(	// Разделы
        0 => "174",
        1 => "",
    ),
    "BUTTON_TEXT" => "Заказать",	// Кнопка. Отображаемый текст
    "BUTTON_MODE" => "order",	// Кнопка. Режим работы
    "ORDER_FORM_ID" => "13",	// Форма для заказа
    "ORDER_FORM_TEMPLATE" => ".default",	// Форма. Шаблон
    "ORDER_FORM_FIELD" => "form_text_48",	// Форма. Поле для автозаполнения
    "ORDER_FORM_TITLE" => "Заказать",	// Форма. Заголовок
    "ORDER_CONSENT" => "#SITE_DIR#company/consent/",	// Ссылка на страницу с соглашением обработки персональных данных
    "COMPOSITE_FRAME_MODE" => "A",
    "COMPOSITE_FRAME_TYPE" => "AUTO"
),
    false
); */?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
$( "#premium" ).on( "click", function() {
	if($(document).find('.hided').hasClass('showed')){
$(document).find('.hided').removeClass('showed').hide(500);
	   }
				   else{
            $(document).find('.hided').addClass('showed').show(500);
	}

        });
    });


</script>!-->