<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

use intec\core\collections\Arrays;
use intec\core\helpers\Html;
use intec\core\io\Path;
use Bitrix\Main\Page\Asset;

$asset = Asset::getInstance();

/**
 * @var Arrays $blocks
 * @var array $block
 * @var array $data
 * @var string $page
 * @var Path $path
 * @global CMain $APPLICATION
 */

?>
	<?
		$asset->addCss(SITE_TEMPLATE_PATH . "/css/ion.rangeSlider.min.css");
		$asset->addCss(SITE_TEMPLATE_PATH . "/css/selectize.css");
		$asset->addJs(SITE_TEMPLATE_PATH . "/js/ion.rangeSlider.min.js");
		$asset->addJs(SITE_TEMPLATE_PATH . "/js/selectize.min.js");
		$asset->addJs(SITE_TEMPLATE_PATH . "/js/jquery.inputmask.min.js");
	?>
<?/*= Html::beginTag('div', ['style' => [
		'margin-top' => '50px',
		'margin-bottom' => '50px'
	]]) ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.form", 
	"fast_internet", 
	array(
		"ELEMENT_ASSOC" => "CREATED_BY",
		"GROUPS" => array(2),
		"IBLOCK_ID" => "32",
		"IBLOCK_TYPE" => "calc",
		"LEVEL_LAST" => "Y",
		"LIST_URL" => "",
		"PROPERTY_CODES" => array(
			0 => "164",
			1 => "165",
			2 => "166",
			3 => "167",
			4 => "168",
			5 => "169",
			6 => "170",
			7 => "171",
			8 => "172",
		),
		"PROPERTY_CODES_REQUIRED" => array(
		),
		"PROPERTIES_LINK" => array(
			"ROOM_TYPE" => "163",
			"SQUARE" => "164",
			"FLOOR_COUNT" => "165",
			"ROOM_COUNT" => "166",
			"DESIRED_SPEED" => "167",
			"DEVICE_COUNT" => "168",
			"EXTRA_SERVICES" => "169",
			"ADDRESS" => "170",
			"PERSONAL_NAME" => "171",
			"PERSONAL_PHONE" => "172",
		),
		"RESIZE_IMAGES" => "N",
		"SEF_MODE" => "N",
		"STATUS" => "ANY",
		"STATUS_NEW" => "N",
		"USER_MESSAGE_ADD" => "",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N",
		"COMPONENT_TEMPLATE" => "fast_internet",
		"DEFAULT_INPUT_SIZE" => "30",
		"MAX_USER_ENTRIES" => "100000",
		"MAX_LEVELS" => "100000",
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CUSTOM_TITLE_NAME" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => ""
	),
	false
);?>
<?= Html::endTag('div') */?>


<?= Html::beginTag('div', ['style' => [
    'margin-top' => '50px',
    'margin-bottom' => '50px'
]]) ?>
    <?php $APPLICATION->IncludeComponent(
	"intec.universe:main.stages", 
	"template.1", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "4",
		"SECTIONS_MODE" => "id",
		"ELEMENTS_COUNT" => "6",
		"HEADER_SHOW" => "Y",
		"HEADER_POSITION" => "center",
		"HEADER" => "Порядок подключения интернета за городом",
		"DESCRIPTION_SHOW" => "N",
		"COUNT_SHOW" => "N",
		"ELEMENT_DESCRIPTION_SHOW" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"SORT_BY" => "SORT",
		"ORDER_BY" => "ASC",
		"COMPONENT_TEMPLATE" => "template.1",
		"SECTIONS" => array(
			0 => "",
			1 => "",
		)
	),
	false
); ?>
<?= Html::endTag('div') ?>