<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

use intec\core\collections\Arrays;
use intec\core\helpers\Html;
use intec\core\io\Path;

/**
 * @var Arrays $blocks
 * @var array $block
 * @var array $data
 * @var string $page
 * @var Path $path
 * @global CMain $APPLICATION
 */

?>
<?= Html::beginTag('div') ?>
    <?php $APPLICATION->IncludeComponent(
	"intec.universe:main.widget", 
	"contact.1", 
	array(
		"SETTINGS_USE" => "Y",
		"MAP_VENDOR" => "yandex",
		"INIT_MAP_TYPE" => "MAP",
		"MAP_MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.75206875577001;s:10:\"yandex_lon\";d:37.59363536999489;s:12:\"yandex_scale\";i:14;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.59552364514138;s:3:\"LAT\";d:55.752068755776286;s:4:\"TEXT\";s:16:\"Мы здесь!\";}}}",
		"BLOCK_SHOW" => "Y",
		"BLOCK_TITLE" => "Наши контакты",
		"ADDRESS_SHOW" => "Y",
		"ADDRESS_CITY" => "",
		"ADDRESS_STREET" => "улица Белова, 5",
		"PHONE_SHOW" => "Y",
		"PHONE_VALUES" => array(
			0 => "+7 (812) 507-61-36",
			1 => "",
		),
		"FORM_SHOW" => "Y",
		"FORM_ID" => "1",
		"FORM_TEMPLATE" => ".default",
		"FORM_TITLE" => "Заказать звонок",
		"FORM_BUTTON_TEXT" => "Заказать звонок",
		"EMAIL_SHOW" => "Y",
		"EMAIL_VALUES" => array(
			0 => "homelte@yandex.ru",
			1 => "",
		),
		"CONSENT_URL" => "/company/consent/",
		"MAP_OVERLAY" => "Y",
		"WIDE" => "N",
		"BLOCK_VIEW" => "over",
		"MAP_CONTROLS" => array(
			0 => "ZOOM",
			1 => "SMALLZOOM",
			2 => "MINIMAP",
			3 => "TYPECONTROL",
			4 => "SCALELINE",
		),
		"MAP_OPTIONS" => array(
			0 => "ENABLE_SCROLL_ZOOM",
			1 => "ENABLE_DBLCLICK_ZOOM",
			2 => "ENABLE_RIGHT_MAGNIFIER",
			3 => "ENABLE_DRAGGING",
		),
		"MAP_MAP_ID" => "",
		"COMPONENT_TEMPLATE" => "contact.1",
		"CONTACT_TYPE" => "PARAMS",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "0",
		"MAP_API_KEY" => "",
		"MAP_COMPOSITE_FRAME_MODE" => "A",
		"MAP_COMPOSITE_FRAME_TYPE" => "AUTO",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
); ?>
<?= Html::endTag('div') ?>