<?AddEventHandler('iblock', 'OnIBlockElementAdd', 'IBFeedForm');
function IBFeedForm(&$arFields)
{
 $SITE_ID = 's1';
 $IBLOCK_ID = 32;
 $EVEN_TYPE = 'NEW_ELEMENT_ADDED';
  if ($arFields['IBLOCK_ID'] == $IBLOCK_ID) {
   $arFeedForm = array(
    "ADD_NAME" => $arFields['NAME'],
    "ADD_DETAIL" => $arFields['PROPERTY_VALUES']['ADDRESS'],     
    "ADD_SOURCE" => $arFields['PROPERTY_VALUES']['PERSONAL_NAME'],
    "ADD_AUTOR" => $arFields['PROPERTY_VALUES']['PERSONAL_PHONE'],
   );
   \CEvent::Send($EVEN_TYPE, $SITE_ID, $arFeedForm );
  }
}
AddEventHandler('iblock', 'OnIBlockElementAdd', 'IBFeedForm1');
function IBFeedForm1(&$arFields)
{
 $SITE_ID = 's1';
 $IBLOCK_ID = 36;
 $EVEN_TYPE = 'ELEMENT_VIDEO_ADD';
  if ($arFields['IBLOCK_ID'] == $IBLOCK_ID) {
   $arFeedForm = array(
    "ADD_NAME" => $arFields['PROPERTY_VALUES']['PERSONAL_NAME'],
            "ADD_PHONE" => $arFields['PROPERTY_VALUES']['PERSONAL_PHONE'],
            "ADD_COMPLECT" => $arFields['DETAIL_TEXT'],
   );
   \CEvent::Send($EVEN_TYPE, $SITE_ID, $arFeedForm );
  }
}
AddEventHandler('iblock', 'OnIBlockElementAdd', 'IBFeedForm2');
function IBFeedForm2(&$arFields)
{
 $SITE_ID = 's1';
 $IBLOCK_ID = 37;
 $EVEN_TYPE = 'NEW_INTERNET_ADD';
  if ($arFields['IBLOCK_ID'] == $IBLOCK_ID) {
   $arFeedForm = array(
    "ADD_NAME" => $arFields['NAME'],    
    "ADD_SOURCE" => $arFields['PROPERTY_VALUES']['PERSONAL_NAME'],
    "ADD_AUTOR" => $arFields['PROPERTY_VALUES']['PERSONAL_PHONE'],
   );
   \CEvent::Send($EVEN_TYPE, $SITE_ID, $arFeedForm );
  }
}
AddEventHandler('iblock', 'OnIBlockElementAdd', 'IBFeedForm3');
function IBFeedForm3(&$arFields)
{
    $SITE_ID = 's1';
    $IBLOCK_ID = 38;
    $EVEN_TYPE = 'NEW_CITY_ORDER_ADD';
    if ($arFields['IBLOCK_ID'] == $IBLOCK_ID) {
        $arFeedForm = array(
            "ADD_NAME" => $arFields['NAME'],
            "ADD_PHONE" => $arFields['PROPERTY_VALUES']['PHONE'],
            "ADD_CITY" => is_null($arFields['PROPERTY_VALUES']['CITY']) ? 'Город не определен' : $arFields['PROPERTY_VALUES']['CITY'],
        );
        \CEvent::Send($EVEN_TYPE, $SITE_ID, $arFeedForm );
    }
}
AddEventHandler('iblock', 'OnIBlockElementAdd', 'formCalc');
function formCalc(&$arFields)
{
    $SITE_ID = 's3';
    $IBLOCK_ID = 79;
    $EVEN_TYPE = 'NEW_FROM_CALC';
    if ($arFields['IBLOCK_ID'] == $IBLOCK_ID) {
        $arFeedForm = array(
            "ADD_NAME" => $arFields['PROPERTY_VALUES']['PERSONAL_NAME'],
            "ADD_PHONE" => $arFields['PROPERTY_VALUES']['PERSONAL_PHONE'],
            "ADD_COMPLECT" => $arFields['DETAIL_TEXT'],
        );
        \CEvent::Send($EVEN_TYPE, $SITE_ID, $arFeedForm );
    }
}
?>