$(document).ready(function () {
	$('#room_square').ionRangeSlider({
		skin: "homelte",
		min: 20,
		max: 800,
		grid: true,
		grid_num: 6,
		postfix: 'м2'
		// grid_snap:true
	});

	$('#speed-select').selectize({
		// create: true,
		// sortField: 'text'
	});

	$(document).on('click', '.counter-wrap .step-control', function () {
		let $that = $(this),
			$parent = $that.parent(),
			$steps = $parent.find('.steps'),
			limit = Number($steps.data('limit')) || 7,
			cur_cal = Number($steps.data('step')) || 1,
			new_val = ($that.hasClass('step-up')) ? cur_cal + 1 : cur_cal - 1;

		if (new_val > limit || new_val < 1) return;
		$steps.html(new_val).data('step', new_val);
	});

	$(document).on('click', '.radioContainer', function () {
		$(this).parent().find('.radioContainer').removeClass('active');
		$(this).parents('.room-type-row').find('.active').removeClass('active');
		$(this).addClass('active');

		$('.room_example').attr('src', $(this).data('src'));
	});

	$('.calc-request').on('click', function () {
		let $_parent = $(this).parents('.form-wrap'),
			$_checkbox = $_parent.find('input[type="checkbox"]'),
			$_phone = $_parent.find('input[name="phone"]'),
			$_name = $_parent.find('input[name="name"]'),
			$_address = $_parent.find('input[name="address"]');

		if (!$_phone.inputmask('isComplete')) {
			$_phone.addClass('alarm');
			removeAlarm($_phone, 'keyup');
		} else if (!$_checkbox.prop('checked')) {
			$_checkbox.parents('label').addClass('alarm');
			removeAlarm($_checkbox, 'change', $_checkbox.parents('label'));
		} else {
			let place = $('input[name="place"]:checked').val() || '',
				square = $('#room_square').val(),
				floors = $('.floor-steps').html() || '',
				rooms = $('.room-steps').html() || '',
				gadgets = $('.gadget-steps').html() || '',
				speed = $('#speed-select option:selected').val() || '',
				services = $('input[name="services[]"]:checked'),
				data = {
					'name': $_name.val() || 'Без имени',
					'address': $_address.val() || 'Не известно',
					'phone': $_phone.val(),
					'place': place,
					'square': square,
					'floors': floors,
					'rooms': rooms,
					'gadgets': gadgets,
					'speed': speed,
					'services': []
				};

			services.each(function(){
				data['services'].push($(this).val());
			});  

			$.ajax({
				data: data,
				url: '/ajax/fast_inet.php',
				method: 'POST',
				success: function (result) {
					var popup = BX.PopupWindowManager.create("popup-message", null, {
						content: result,
						closeIcon: {},
						autoHide: true,
						offsetLeft: 0,
						offsetTop: 0,
						width: 450,
						overlay: true
					});

					popup.show();
				}
			});
		}
	});

	$('input[name="phone"]').inputmask({ "mask": "+7 (999) 999-9999" });
});

function removeAlarm($elem, event, $class_elem) {
	$elem.on(event, function () {
		$(this).removeClass('alarm');
		if ($class_elem && $class_elem.length) $class_elem.removeClass('alarm');
	})
}