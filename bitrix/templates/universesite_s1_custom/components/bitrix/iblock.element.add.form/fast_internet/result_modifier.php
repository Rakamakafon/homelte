<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Iblock\ElementTable;
?>

<?php
$arResult['PROPERTIES']['ROOM_TYPE'] = $arResult["PROPERTY_LIST_FULL"][$arParams['PROPERTIES_LINK']['ROOM_TYPE']];
$arResult['PROPERTIES']['DESIRED_SPEED'] = $arResult["PROPERTY_LIST_FULL"][$arParams['PROPERTIES_LINK']['DESIRED_SPEED']];
$arResult['PROPERTIES']['EXTRA_SERVICES'] = $arResult["PROPERTY_LIST_FULL"][$arParams['PROPERTIES_LINK']['EXTRA_SERVICES']];

$size = ceil(count($arResult['PROPERTIES']['EXTRA_SERVICES']['ENUM']) / 2);

$arResult['PROPERTIES']['EXTRA_SERVICES']['ENUM'] = array_chunk($arResult['PROPERTIES']['EXTRA_SERVICES']['ENUM'], $size);

$roomTypeIblockID = $arResult['PROPERTIES']['ROOM_TYPE']['LINK_IBLOCK_ID'];
$obRoomType = ElementTable::getList(array('filter' => array('IBLOCK_ID' => $roomTypeIblockID)));

while ($arRoomType = $obRoomType->fetch()) {
	$arResult['PROPERTIES']['ROOM_TYPE']['VALUE'][] = $arRoomType;
}

$arResult['PROPERTIES']['ROOM_TYPE']['DEFAULT'] = $arResult['PROPERTIES']['ROOM_TYPE']['VALUE'][0];
