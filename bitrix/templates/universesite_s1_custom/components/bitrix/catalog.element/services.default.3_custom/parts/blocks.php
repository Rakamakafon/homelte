<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

use intec\core\helpers\Html;

/**
 * @var array $arParams
 * @var array $arResult
 */

$arBlocks = [];

foreach ($arResult['BLOCKS'] as $sKey => &$arBlock)
    if ($arBlock['ACTIVE'])
        $arBlocks[$sKey] = $arBlock;

unset($arBlock);
unset($sKey);

$iBlocksCount = count($arBlocks);
$iBlocksCurrent = 0;

if ($iBlocksCount <= 0)
    return;


foreach ($arBlocks as $key => $arBlock) {
    $SORT[$key]  = $arBlock['SORT'];
}
$res = array_multisort($SORT, SORT_ASC, $arBlocks);
?>
<div class="catalog-element-blocks">
    <?php foreach ($arBlocks as $sKey => $arBlock) { ?>
        <?= Html::beginTag('div', [
            'class' => [
                'catalog-element-block'
            ],
            'data' => [
                'block' => $sKey,
                'type' => $arBlock['TYPE']
            ]
        ]) ?>
            <?php include(__DIR__.'/blocks/'.$sKey.'.php') ?>
        <?= Html::endTag('div') ?>
        <?php $iBlocksCurrent++ ?>
    <?php } ?>
</div>
