<?
define('ELEMENT_ID', 331);
//define('ELEMENT_ID', 846);
define('IBLOCK_ID', 35);
define('IBLOCK_TYPE', calc);
?>
<?if($arResult['ID'] == 265):?>

<?$APPLICATION->IncludeComponent(
	"intec.universe:main.rates", 
	"template.6_custom", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "7",
		"ELEMENTS_COUNT" => "8",
		"PROPERTY_LIST" => array(	// Отображаемые свойства
        0 => "WIFI_MW52",
1 => "ROUTER2",
        2 => "MODEM_PREMIUM_2",
        3 => "MODEM_PREMIUM",
        4 => "PROPERTY_ROUTER",
        5 => "PROPERTY_MODEM",
        6 => "ANTENNA5",
        7 => "ANTENNA6",
        8 => "ANTENNA7",
		9 => "ANTENNA8",
        10 => "PROPERTY_CABLE",
        11 => "PROPERTY_CRONSHTEIN",
        12 => "PROPERTY_PIGTAIL",
        13 => "PROPERTY_MONTAG",
        14 => "PROPERTY_SIM",

    ),
		"PROPERTY_PRICE" => "PRICE",
		"PROPERTY_CURRENCY" => "CURRENCY",
		"PROPERTY_DISCOUNT" => "DISCOUNT",
		"PROPERTY_DISCOUNT_TYPE" => "",
		"PROPERTY_DETAIL_URL" => "",
		"HEADER_SHOW" => "Y",
		"HEADER_POSITION" => "center",
		"HEADER_TEXT" => "Комплекты на подключение в регионе: г. #SOTBIT_REGIONS_NAME#",
		"DESCRIPTION_SHOW" => "N",
		"COLUMNS" => "4",
		"VIEW" => "tabs",
		"TABS_POSITION" => "center",
		"SECTION_DESCRIPTION_SHOW" => "N",
		"SECTION_DESCRIPTION_POSITION" => "center",
		"COUNTER_SHOW" => "N",
		"COUNTER_TEXT" => "ТАРИФ",
		"PRICE_SHOW" => "Y",
		"DISCOUNT_SHOW" => "Y",
		"PREVIEW_SHOW" => "Y",
		"PROPERTIES_SHOW" => "Y",
		"BUTTON_SHOW" => "Y",
		"SLIDER_USE" => "Y",
		"SLIDER_NAV" => "Y",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600000",
		"SORT_BY" => "SORT",
		"ORDER_BY" => "ASC",
		"COMPONENT_TEMPLATE" => "template.6_custom",
		"SECTIONS" => array(
			0 => "1",
			1 => "",
		),
		"BUTTON_TEXT" => "Заказать",
		"BUTTON_MODE" => "order",
		"ORDER_FORM_ID" => "6",
		"ORDER_FORM_TEMPLATE" => ".default",
		"ORDER_FORM_FIELD" => "form_text_20",
		"ORDER_FORM_TITLE" => "Заказать",
		"ORDER_CONSENT" => "#SITE_DIR#company/consent/",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"SLIDER_DOTS" => "N",
		"SLIDER_LOOP" => "N",
		"SLIDER_AUTO_USE" => "N"
	),
	$component
); ?>
<?endif;?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"main_calc", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => ELEMENT_ID,
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"IBLOCK_ID" => IBLOCK_ID,
		"IBLOCK_TYPE" => IBLOCK_TYPE,
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "PARTS",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "main_calc"
	),
	$component
);?>
<?if($arResult['ID'] == 265):?>
<?$APPLICATION->IncludeComponent(
    "intec.universe:main.rates",
    "template.2",
    Array(
        "BUTTON_MODE" => "order",
        "BUTTON_SHOW" => "Y",
        "BUTTON_TEXT" => "Подробнее",
        "CACHE_TIME" => "0",
        "CACHE_TYPE" => "A",
        "COLUMNS" => "4",
        "COMPONENT_TEMPLATE" => "template.2",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "COUNTER_SHOW" => "N",
        "DESCRIPTION_POSITION" => "center",
        "DESCRIPTION_SHOW" => "Y",
        "DESCRIPTION_TEXT" => "Мы предлагаем широкий выбор тарифов от различных операторов",
        "ELEMENTS_COUNT" => "",
        "HEADER_POSITION" => "center",
        "HEADER_SHOW" => "Y",
        "HEADER_TEXT" => "Тарифы",
        "IBLOCK_ID" => "7",
        "IBLOCK_TYPE" => "content",
        "LAZYLOAD_USE" => "N",
        "ORDER_BUTTON" => "Заказать",
        "ORDER_BY" => "ASC",
        "ORDER_CONSENT" => "#SITE_DIR#company/consent/",
        "ORDER_FORM_CONSENT" => "#SITE_DIR#company/consent/",
        "ORDER_FORM_FIELD" => "form_text_20",
        "ORDER_FORM_ID" => "6",
        "ORDER_FORM_TEMPLATE" => ".default",
        "ORDER_FORM_TITLE" => "Заказать",
        "ORDER_USE" => "Y",
        "PREVIEW_SHOW" => "Y",
        "PRICE_LIST" => "",
        "PRICE_LIST_BUTTON_TEXT" => "",
        "PRICE_SHOW" => "Y",
        "PROPERTIES_SHOW" => "Y",
        "PROPERTY_CURRENCY" => "CURRENCY",
        "PROPERTY_DETAIL_URL" => "",
        "PROPERTY_DISCOUNT" => "",
        "PROPERTY_LIST" => array(0=>"G4_LTE",1=>"G3_SPEED",2=>"UNLIM_TRAFFIC",3=>"",),
        "PROPERTY_PRICE" => "PRICE",
        "SECTIONS" => array(0=>"2",1=>"",),
        "SECTION_DESCRIPTION_POSITION" => "center",
        "SECTION_DESCRIPTION_SHOW" => "Y",
        "SETTINGS_USE" => "N",
        "SLIDER_USE" => "Y",
"SLIDER_NAV" => "Y",
        "SORT_BY" => "SORT",
        "TABS_POSITION" => "center",
        "TABS_USE" => "N",
        "TABS_VIEW" => "1",
        "VIEW" => "tabs"
    ), $component
);
endif;?>