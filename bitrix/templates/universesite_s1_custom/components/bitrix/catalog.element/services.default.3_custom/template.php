<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use intec\core\bitrix\Component;
use intec\core\helpers\Html;

/**
 * @var array $arParams
 * @var array $arResult
 */

if (!Loader::includeModule('intec.core'))
    return;

$this->setFrameMode(true);
$sTemplateId = Html::getUniqueId(null, Component::getUniqueId($this));

Loc::loadMessages(__FILE__);

?>
<div id="<?= $sTemplateId ?>" class="ns-bitrix c-catalog-element c-catalog-element-services-default-3">
    <?php include(__DIR__.'/parts/blocks.php') ?>
    <div class="catalog-element-block" data-block="description.1" data-type="normal">            <div class="catalog-element-description widget">
    <div class="catalog-element-description-wrapper intec-content">
        <div class="catalog-element-description-wrapper-2 intec-content-wrapper">
                            <div class="catalog-element-description-header widget-header">
                    <div class="widget-title align-left">Карта подключений</div><br>                </div>
                       
        </div>
    </div>
</div>
        </div>
<div class="widget-content intec-content-wrap">
                    <div class="widget-content-wrapper intec-content">
                <div class="widget-content-wrapper-2 intec-content-wrapper">
                                <div class="widget-map">
            #SOTBIT_REGIONS_UF_REGION_MAP_CUSTOM#
                    </div>
                        </div>
            </div>
            </div>
    
</div>