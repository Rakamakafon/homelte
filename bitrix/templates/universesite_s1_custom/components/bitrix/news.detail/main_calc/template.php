<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="complectation-block-wrap block-wrap intec-content">
        <div class="row-flex page-rows complectations" data-complect="<?=$arResult['ID']?>">
            <h2 class="block-title">Комплект подключения включает:</h2>
            <?foreach($arResult['PARTS'] as $arPart):?>
                <div class="product-col <?if (!empty($arPart['TYPES']) && $arPart['ID'] !== '316'):?>long-card<?endif?>">
                    <div class="product-card" data-product_key="<?=$arPart['CODE']?>" data-product="<?=$arPart['ID']?>" <?if (empty($arPart['TYPES'])):?>prices="<?=$arPart['PRICE']?>"<?endif?>>
                        <div class="left-col">
                            <div class="pic-wrap">
                                <?if (!empty($arPart['TYPES'])):?>
                                <div class ="picture_types">
                                <?foreach($arPart['TYPES'] as $arType):?>
                                    <div class="descr_picture <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>active<?endif?>" data-index = '<?=$arType['ID']?>'>
                                        <img src="<?=CFile::GetPath($arType['DETAIL_PICTURE'])?>">
                                    </div>
                                <?endforeach?>
                                </div>
                                <?else:?>
                                <img src="<?=CFile::GetPath($arPart['DETAIL_PICTURE'])?>">
                                <?if (!empty($arPart['PROPERTY_PART_TYPE_VALUE'])):?>
                                    <i class="ic_info"></i>
                                <?endif?>
                                <?endif;?>
                            </div>
                        </div>
                        <div class="right-col">
                            <div class="title"><?=$arPart['NAME']?></div>
                            <div class="price">
                                <?if ($arPart['PRICE'] === '0'):?>
                                    Бесплатно
                                <?else:?>
                                    <?=$arPart['PRICE']?> ₽
                                <?endif?>
                            </div>
                            <?if (!empty($arPart['TYPES'])):?>
                                <div class="packages">
                                    <?foreach($arPart['TYPES'] as $arType):?>
                                         <label class="radioContainer <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>active<?else:?>intec-ui-mod-transparent<?endif;?> intec-ui intec-ui-control-button intec-ui-mod-round-half intec-ui-scheme-current intec-ui-size-2">
                                            <span class='type-name'><?=$arType['NAME']?></span>
                                            <input type="radio" name="<?=$arPart['CODE']?>" value="<?=$arType['PRICE']?>"
                                                    data-type="<?=$arType['ID']?>" <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>checked<?endif?>>
                                        </label>
                                    <?endforeach?>
                                    <div class="descr_types" id="descr_types">
                                        <?foreach($arPart['TYPES'] as $arType):?>
                                           <div class="descr <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>active<?endif?>" data-index = '<?=$arType['ID']?>'>
                                               <?=$arType['DETAIL_TEXT']?>
                                           </div>
                                        <?endforeach?>
                                    </div>
                                </div>
                            <?else:?>
                                <div class="descr">
                                    <?=$arPart['DETAIL_TEXT']?>
                                </div>
                            <?endif?>
                        </div>
                    </div>
                </div>
            <?endforeach?>
            <div class="price-calc-col">
                <div class="total">Итого:</div>
                <span class="price"></span>
                <div class="btn-wrap">
                    <button class="intec-ui intec-ui-control-button intec-ui-mod-round-half intec-ui-scheme-current intec-ui-size-5 order-complect">Заказать этот
                        комплект
                    </button>
                </div>
            </div>
        </div>
</div>