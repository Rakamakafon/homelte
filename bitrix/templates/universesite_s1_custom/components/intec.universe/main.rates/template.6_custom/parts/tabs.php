<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\Html;

/**
 * @var array $arResult
 * @var array $arVisual
 * @var string $sTemplateId
 * @var Closure $vItems()
 */

?>
<div class="widget-tabs-content intec-ui intec-ui-control-tabs-content">
    <?php $iCounter = 0 ?>
    <?php foreach ($arResult['SECTIONS'] as $arSection) { ?>
        <?= Html::beginTag('div', [
            'id' => $sTemplateId.'-tab-'.$iCounter,
            'class' => Html::cssClassFromArray([
                'intec-ui-part-tab' => true,
                'fade' => true,
                'in active' => $iCounter === 0
            ], true),
            'role' => 'tabpanel'
        ]) ?>
        <?php if ($arVisual['SECTION']['DESCRIPTION']['SHOW'] && !empty($arSection['DESCRIPTION'])) { ?>
            <div class="widget-section-description" data-align="<?= $arVisual['SECTION']['DESCRIPTION']['POSITION'] ?>">
                <?= $arSection['DESCRIPTION'] ?>
            </div>
        <?php } ?>
        <?php $vItems($arSection['ITEMS']) ?>
        <?= Html::endTag('div') ?>
        <?php $iCounter++ ?>
    <?php } ?>
</div>