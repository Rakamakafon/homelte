<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use intec\core\bitrix\Component;
use intec\core\helpers\Html;
use intec\core\helpers\JavaScript;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = Html::getUniqueId(null, Component::getUniqueId($this));

?>
<div class="widget c-form c-form-template-1 custom" id="<?= $sTemplateId ?>">

                    <?php if ($arResult['BUTTON']['SHOW']) {

                        $sForm = JavaScript::toObject([
                            'id' => $arResult['ID'],
                            'template' => $arResult['TEMPLATE'],
                            'parameters' => [
                                'AJAX_OPTION_ADDITIONAL' => $sTemplateId . '_FORM_TEMPLATE_1',
                                'CONSENT_URL' => $arResult['CONSENT']
                            ],
                            'settings' => [
                                'title' => $arResult['NAME']
                            ]
                        ]);

                    ?>
                        <?= Html::beginTag('div', [
                            'class' => Html::cssClassFromArray([
                                'widget-form-buttons' => true,
                                'intec-grid-item' => [
                                    'auto' => $arResult['VIEW'] === 'left' || $arResult['VIEW'] === 'right',
                                    '1024-1' => $arResult['VIEW'] === 'left' || $arResult['VIEW'] === 'right',
                                    '1' => $arResult['VIEW'] === 'vertical'
                                ]
                            ], true)
                        ]) ?>
                            <div class="widget-form-buttons-wrap">
                                <?= Html::beginTag('div', [
                                    'class' => [
                                        'search-title-button intec-cl-text'
                                    ],
                                    'onclick' => '(function() {
                                        universe.forms.show('.$sForm.');
                                        if (window.yandex && window.yandex.metrika) {
                                           window.yandex.metrika.reachGoal(\'forms.open\');
                                           window.yandex.metrika.reachGoal('.JavaScript::toObject('forms.'.$arResult['ID'].'.open').');
                                       }
                                    })()'
                                ]) ?>
                                <i class="glyph-icon-loop"></i>
                                <?= Html::endTag('div') ?>
                            </div>
                        <?= Html::endTag('div') ?>
                    <?php } ?>
                </div>
    <?= Html::endTag('div') ?>
</div>
