<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;

$arSearchParams = !empty($arSearchParams) ? $arSearchParams : [];

$sPrefix = 'SEARCH_';
$arParameters = [];

foreach ($arParams as $sKey => $sValue)
    if (StringHelper::startsWith($sKey, $sPrefix)) {
        $sKey = StringHelper::cut($sKey, StringHelper::length($sPrefix));
        $arParameters[$sKey] = $sValue;
    }

$arParameters = ArrayHelper::merge($arParameters, $arSearchParams);
$arParameters['PAGE'] = $arResult['SEARCH']['MODE'] === 'site' ? $arResult['URL']['SEARCH'] : $arResult['URL']['CATALOG'];
$arParameters['INPUT_ID'] = $arParameters['INPUT_ID'].'-input-1';

?>
<!--noindex-->
<?$APPLICATION->IncludeComponent("bitrix:sale.location.selector.search", "custom_header_loc", Array(
    "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CODE" => "",	// Символьный код местоположения
    "FILTER_BY_SITE" => "N",	// Фильтровать по сайту
    "ID" => "",	// ID местоположения
    "INITIALIZE_BY_GLOBAL_EVENT" => "",	// Инициализировать компонент только при наступлении указанного javascript-события на объекте window.document
    "INPUT_NAME" => "LOCATION",	// Имя поля ввода
    "JS_CALLBACK" => "",	// Javascript-функция обратного вызова
    "JS_CONTROL_GLOBAL_ID" => "",	// Идентификатор javascript-контрола
    "PROVIDE_LINK_BY" => "id",	// Сохранять связь через
    "SHOW_DEFAULT_LOCATIONS" => "N",	// Отображать местоположения по-умолчанию
    "SUPPRESS_ERRORS" => "N",	// Не показывать ошибки, если они возникли при загрузке компонента
),
    false
);?>
<!--/noindex-->
<?php unset($arParameters) ?>
<?php unset($arSearchParams) ?>