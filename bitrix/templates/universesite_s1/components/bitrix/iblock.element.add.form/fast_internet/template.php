<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="connection-calc intec-content">
    <div class="row-flex page-rows">
        <h2 class="block-title">Получите быстрый интернет там, где это необходимо</h2>
        <div class="calc-col-wrap column">
            <?if (!empty($arResult['PROPERTIES']['ROOM_TYPE']['VALUE'])):?>
                <div class="calc-block">
                    <div class="title"><?=$arResult['PROPERTIES']['ROOM_TYPE']['NAME']?></div>
                    <div class="content">
                        <div class="room-type-row" data-dir_link="https://homelte-wifi.ru/wp-content/themes/inet_homelte">
                            <?foreach($arResult['PROPERTIES']['ROOM_TYPE']['VALUE'] as $key => $roomType):?>
                                <div class="col">
                                    <label class="radioContainer <?if($key == 0):?>active<?endif?>" data-src="<?=CFile::GetPath($roomType['DETAIL_PICTURE'])?>">
                                        <span><i class="ic_<?=$roomType['CODE']?>"></i></span>
                                        <div class="bottom-row">
                                            <input type="radio" name="place" value="<?=$roomType['ID']?>" <?if($key == 0):?>checked<?endif?>>
                                            <span class="checkmark"></span><?=$roomType['NAME']?>
                                        </div>
                                    </label>
                                </div>
                            <?endforeach?>
                        </div>
                    </div>
                </div>
            <?endif?>

            <div class="img-wrap small-view"><img class="room_example" src="<?=CFile::GetPath($arResult['PROPERTIES']['ROOM_TYPE']['DEFAULT']['DETAIL_PICTURE'])?>"></div>

            <div class="calc-block range-wrap">
                <div class="title">Площадь</div>
                <div class="content">
                    <input type="hidden" value="50" id="room_square" class="irs-hidden-input" tabindex="-1" readonly="">
                </div>
            </div>
            <div class="calc-block">
                <div class="row-flex">
                    <div class="left-column column">
                        <div class="title">
                            Количество этажей
                        </div>
                        <div class="content">
                            <div class="counter-wrap">
                                <span class="step-control step-down"></span>
                                <span class="steps floor-steps" data-step="2" data-limit="7">2</span>
                                <span class="step-control step-up"></span>
                            </div>
                        </div>
                    </div>
                    <div class="right-column column">
                        <div class="title">
                            Количество комнат
                        </div>
                        <div class="content">
                            <div class="counter-wrap">
                                <span class="step-control step-down"></span>
                                <span class="steps room-steps" data-step="4" data-limit="20">4</span>
                                <span class="step-control step-up"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="calc-block">
                <div class="row-flex">
                    <div class="left-column column">
                        <div class="title">
                            Желаемая скорость
                        </div>
                        <div class="content">
                            <select id="speed-select">
                                <?foreach ($arResult['PROPERTIES']['DESIRED_SPEED']['ENUM'] as $speed):?>
                            	   <option value="<?=$speed['ID']?>"><?=$speed['VALUE']?></option>
                                <?endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="right-column column">
                        <div class="title">
                            Количество устройств
                        </div>
                        <div class="content">
                            <div class="counter-wrap">
                                <span class="step-control step-down"></span>
                                <span class="steps gadget-steps" data-step="12" data-limit="100">12</span>
                                <span class="step-control step-up"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="calc-block additional-big">
                <div class="title">Дополнительные услуги</div>
                <div class="content row-flex">
                    <?foreach ($arResult['PROPERTIES']['EXTRA_SERVICES']['ENUM'] as $index => $group):?>
                        <?if ($index === 0):?>
                            <div class="left-column column">
                                <?foreach ($group as $service):?>
                                    <label class="checkbox">
                                        <span><?=$service['VALUE']?></span>
                                        <input type="checkbox" name="services[]" value="<?=$service['ID']?>">
                                        <span class="checkmark"></span>
                                    </label>
                                <?endforeach?>
                            </div>
                        <?else:?>
                            <div class="right-column column">
                                <?foreach ($group as $service):?>
                                    <label class="checkbox">
                                        <span><?=$service['VALUE']?></span>
                                        <input type="checkbox" name="services[]" value="<?=$service['ID']?>">
                                        <span class="checkmark"></span>
                                    </label>
                                <?endforeach?>
                            </div>
                        <?endif?>
                    <?endforeach?>
                </div>
            </div>
        </div>
        <div class="right-column column room_example-wrap">
            <div class="img-wrap big-view"><img class="room_example" src="<?=CFile::GetPath($arResult['PROPERTIES']['ROOM_TYPE']['DEFAULT']['DETAIL_PICTURE'])?>"></div>
            <div class="calc-block additional-small">
                <div class="title">Дополнительные услуги</div>
                <div class="content row-flex">
                    <?foreach ($arResult['PROPERTIES']['EXTRA_SERVICES']['ENUM'] as $index => $group):?>
                        <?if ($index === 0):?>
                            <div class="left-column column">
                                <?foreach ($group as $service):?>
                                    <label class="checkbox">
                                        <span><?=$service['VALUE']?></span>
                                        <input type="checkbox" name="services[]" value="<?=$service['ID']?>">
                                        <span class="checkmark"></span>
                                    </label>
                                <?endforeach?>
                            </div>
                        <?else:?>
                            <div class="right-column column">
                                <?foreach ($group as $service):?>
                                    <label class="checkbox">
                                        <span><?=$service['VALUE']?></span>
                                        <input type="checkbox" name="services[]" value="<?=$service['ID']?>">
                                        <span class="checkmark"></span>
                                    </label>
                                <?endforeach?>
                            </div>
                        <?endif?>
                    <?endforeach?>
                </div>
            </div>
            <div class="form-wrap">
                <div class="form-control-custom">
                    <input type="text" name="address" placeholder="Адрес подключения">
                </div>
                <div class="form-control-custom double">
                    <div class="left">
                        <input type="text" name="name" placeholder="Ваше имя">
                    </div>
                    <div class="right">
                        <input type="text" name="phone" placeholder="Номер телефона">
                    </div>
                </div>
                <div class="button-wrap">
                    <button class="btn btn-primary calc-request">Рассчитать стоимость</button>
                    <div class="">
                        <label class="checkbox agreement">
                            <a>Я принимаю условия передачи информации</a>
                            <input type="checkbox" checked="">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="result"></div>