<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php
if (!empty($arResult['PROPERTIES']['PARTS']['VALUE']))
{
	$arSelect = array("ID", "DETAIL_TEXT", "NAME", "CODE", "DETAIL_PICTURE", "CODE", "PROPERTY_PART_PRICE", "PROPERTY_PART_TYPE", "PROPERTY_BLOCK_LAYOUT"); 
	$arFilter = array('ID' => $arResult['PROPERTIES']['PARTS']['VALUE']);
	$parts = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, array(), $arSelect);

	$arPartTypeIDs = array();

	while($arPart = $parts->Fetch())
	{
		$arResult['PARTS'][$arPart['ID']] = $arPart;

		if (!empty($arPart["PROPERTY_PART_TYPE_VALUE"]))
		{
			$arPartTypeIDs = array_merge($arPartTypeIDs, $arPart["PROPERTY_PART_TYPE_VALUE"]);
		}
		else{
			$arResult['PARTS'][$arPart['ID']]['PRICE'] = $arPart["PROPERTY_PART_PRICE_VALUE"];
		}
	}

	if (!empty($arPartTypeIDs))
	{
		$arSelect = array("ID", "DETAIL_TEXT", "NAME", "DETAIL_PICTURE", "PROPERTY_PART_TYPE_PRICE", "PROPERTY_DEFAULT_TYPE"); 
		$arFilter = array('ID' => $arPartTypeIDs);
		$obPartTypes = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);

		while($arPartType = $obPartTypes->Fetch())
		{
			$arPartTypes[$arPartType['ID']] = $arPartType;
		}

		foreach ($arResult['PARTS'] as &$arPart)
		{
			if (empty($arPart["PROPERTY_PART_TYPE_VALUE"]))
				continue;

			foreach ($arPart["PROPERTY_PART_TYPE_VALUE"] as $index => $arPartTypeID)
			{
				$arPartTypes[$arPartTypeID]['PRICE'] = $arPartTypes[$arPartTypeID]['PROPERTY_PART_TYPE_PRICE_VALUE'];
				$arPart['TYPES'][$index] = $arPartTypes[$arPartTypeID];

				if ($arPartTypes[$arPartTypeID]['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y')
				{
					$arPart['PRICE'] = $arPartTypes[$arPartTypeID]['PROPERTY_PART_TYPE_PRICE_VALUE'];
				}
			}
		}
	}
}