let complectPopup;

	$(document).ready(function () {

	$('.act_cam').click(function(){
		let cam = $(this).data('sign'),
			dnum = $(this).data('num'),
			sum = 0,
			mon = 0,
			form = $(this).closest('.your-kit');

		let q = parseInt($("#comp"+dnum+"_q").val());

		if (cam == "+") 
			q++;
		else
			q--;

		if (q >= 16) 
			q = 16;

     	let campr = parseInt($(this).closest('.constructor-item__buttons').data('price'));

		if (q < 5) { camView(dnum, "1", form); }
		if (q > 4) { camView(dnum, "2", form); }
		if (q > 8) { camView(dnum, "3", form); }

		$(this).closest('.kit-constructor').find('.constructor-item').each(function() {
			if ($(this).data('type') === 'dependency')
			{
				let price = $(this).find('.constructor-item__buttons .reg_checked input[type=radio]').val();

				$(this).find('input[type=hidden][name="price"]').val(price);
			}
			else if ($(this).data('type') === 'simple')
			{
				if (q != 0)
				{
					let price = $(this).find('.current-price').data('price') * q;
					$(this).find('input[type=hidden][name="price"]').val(price);
					$(this).find('.current-price').html(price + ' руб.');
				}
			}
			else if($(this).data('type') === 'calc')
			{
				let price = q * campr;
				$(this).find('input[type=hidden][name="price"]').val(price);
				$(this).find('input[type=hidden][name="quantity"]').val(q);
			}
			else
			{
				mon = $(this).find('input[type=hidden][name="price"]').val();
			}

			sum += parseInt($(this).find('input[type=hidden][name=price]').val());
		});

		if (q != 0)
		{ 
			$('#k_cam_'+dnum).html(q);

			saleSum = parseInt(sum*0.7);

			$("#result_"+dnum).html(saleSum);
			$("#result_"+dnum+"_1").html(sum);
			$("#comp"+dnum+"_sum").val(sum-mon);
		}
	});

	$('.sinfo').click(function(){
		let data = {};
			
		data.product = $(this).closest('.constructor-item').data('product');

		$.ajax({
			data: data,
			method: 'POST',
			url: '/ajax/get_description.php',
			success: function (result) {
				if (complectPopup)
					complectPopup.destroy();

				complectPopup = new BX.PopupWindow('complect-popup-second', null, {
					content: result,
				    closeIcon: {},
				    autoHide: true,
				    offsetLeft: 0,
		            offsetTop: 0,
		            width: 716,
		            overlay: true
				});

				complectPopup.show();
			}
		});
	});

	$('.act_mon').click(function(){
		let mon = this.getAttribute("value"),
			monp = parseInt($(this).data('value')),
			dnum = $(this).data('num'),
			form = $(this).closest('.your-kit');

		monView(dnum, mon, form);

		let code = $(this).closest('.constructor-item').data('code');
		$('#comp'+dnum+'_'+code).val(monp);

		SUM = parseInt($("#comp"+dnum+"_sum").val())+monp;

		saleSum = parseInt(SUM*0.7);
		$("#result_"+dnum).html(saleSum);
		$("#result_"+dnum+"_1").html(SUM);

	});

	$('.get-consultation').on('click', function (e) {
		e.preventDefault();

		$.ajax({
			url: '/ajax/complect_order_modal_alt.php',
			success: function (result) {
				if (complectPopup)
					complectPopup.destroy();

				complectPopup = new BX.PopupWindow('complect-popup-second', null, {
					content: result,
				    closeIcon: {},
				    autoHide: true,
				    offsetLeft: 0,
		            offsetTop: 0,
		            width: 520,
		            overlay: true
				});

				complectPopup.show();

				$('#complect-popup-second').find('input[name=phone]').inputmask({ "mask": "+7 (999) 999-9999" });

				$('#post_submit').on('click', function () {
					let form = $(this).closest('.form__inputs'),
	                    phone = form.find('input[name="phone"]'),
	                    name = form.find('input[name="name"]').val(),
	                    data = {name: name, phone: phone.val()};

					sendAjaxRequest(phone, data, '/ajax/get_consultation.php');
	            });
			}
		});
	});


	$('.order-complect').on('click', function () {
		let complectForm = $(this).closest('.your-kit');

		$.ajax({
			url: '/ajax/complect_order_modal_alt.php',
			success: function (result) {
				if (complectPopup)
					complectPopup.destroy();

				complectPopup = new BX.PopupWindow('complect-popup-second', null, {
					content: result,
				    closeIcon: {},
				    autoHide: true,
				    offsetLeft: 0,
		            offsetTop: 0,
		            width: 520,
		            overlay: true
				});

				complectPopup.show();

				$('#complect-popup-second').find('input[name=phone]').inputmask({ "mask": "+7 (999) 999-9999" });

				$('#post_submit').on('click', function () {
					let form = $(this).closest('.form__inputs'),
	                    phone = form.find('input[name="phone"]'),
	                    name = form.find('input[name="name"]').val(),
	                    data = {name: name, phone: phone.val(), complect: complectForm.data('complect'), desc: ''};

	                complectForm.find('.constructor-item').each(function () {
	                	data.desc += $(this).find('.constructor-item__name').html() + ' - ';

	                	if ( $(this).data('type') === 'calc')
	                	{
	                		data.desc += $(this).find('input[type=hidden][name=quantity]').val() + ' (';
	                	}
	                	else if ( $(this).data('type') === 'dependency')
	                	{
	                		data.desc += $(this).find('.reg_checked .type-name').html() + ' (';
	                	}
	                	else if ( $(this).data('type') === 'select')
	                	{
	                		data.desc += $(this).find('.constructor-item__button input:checked').siblings('span').html() + ' (';
	                	}

	                	data.desc += $(this).find('input[type=hidden][name=price]').val() + ' руб.), ';
	                });

					sendAjaxRequest(phone, data, '/ajax/complect_order.php');
	            });
			}
		});
	});


	$('#pnr_1').html(parseInt($("#comp1_q").val()) * parseInt($("#comp1_PNR").val()) + " руб.");
});
function camView(dnum, cam, form) {
	form.find('.chcam').removeClass('camOn').addClass('camOf');
	$('.chcam_'+cam+'_'+dnum).removeClass('camOf').addClass('camOn');

	$('.constructor-item__button'+dnum+'-reg').removeClass('reg_checked');	
	$('.chcam'+cam+'_'+dnum).addClass('reg_checked');
}

function monView(dnum, mon, form) {
	form.find('.chmon').removeClass('camOn').addClass('camOf');	
	$('.chmon_'+mon+'_'+dnum).removeClass('camOf').addClass('camOn');
} 

function sendAjaxRequest(phone, data, url){
	if (!phone.inputmask('isComplete')) {
        phone.addClass('alarm');
        removeAlarm(phone, 'keyup');
    } 
    else {
		$.ajax({
			data: data,
			method: 'POST',
			url: url,
			success: function (result) {
				$('#complect-popup-second .content').append(result);
			}
		});
	}
}

function removeAlarm($elem, event, $class_elem) {
    $elem.on(event, function () {
        $(this).removeClass('alarm');
        if ($class_elem && $class_elem.length) $class_elem.removeClass('alarm');
    })
}