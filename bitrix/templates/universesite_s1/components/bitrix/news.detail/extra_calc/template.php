<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$sum = 0;
?>
<div class="your-kit intec-content" id="calc<?=$arResult['ID']?>" data-complect="<?=$arResult['ID']?>">
    <h3><b><?=$arResult['NAME']?></b></h3>
    <div class="container">
        <div class="your-kit__left">
            <div class="kit-constructor">
                <?foreach($arResult['PARTS'] as $arPart):?>
                    <div class="constructor-item" data-product="<?=$arPart['ID']?>" data-code="<?=strtolower($arPart['CODE'])?>"
                        data-type="<?if(empty($arPart['TYPES'])):?>simple<?elseif(empty($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'])):?>select<?else:?><?=strtolower($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'])?><?endif?>"
                    >
                        <?if (empty($arPart['TYPES'])):?>
                            <?$sum += intval($arPart['PRICE']) * 4?>
                            <input type="hidden" name="price" id="comp<?=$arResult['ID']?>_<?=strtolower($arPart['CODE'])?>" value="<?=intval($arPart['PRICE']) * 4?>">
                        <?elseif($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'] === 'CALC'):?>
                            <?$sum += intval($arPart['PRICE']) * 4?>
                            <input type="hidden" name="price" id="comp<?=$arResult['ID']?>_<?=strtolower($arPart['CODE'])?>" value="<?=intval($arPart['PRICE']) * 4?>">
                            <input type="hidden" name="quantity" id="comp<?=$arResult['ID']?>_q" value="4">    
                        <?else:?>
                            <?$sum += intval($arPart['PRICE'])?>
                            <input type="hidden" name="price" id="comp<?=$arResult['ID']?>_<?=strtolower($arPart['CODE'])?>" value="<?=intval($arPart['PRICE'])?>">
                        <?endif?>
                        <div class="constructor-item__image">
                            <?if (!empty($arPart['TYPES'])):?>
                                <?if ($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'] === 'CALC'):?>
                                    <?foreach($arPart['TYPES'] as $index => $arType):?>
                                        <div class="cam<?=$index+1?> <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>camOn<?else:?>camOf<?endif?>">
                                            <img src="<?=CFile::GetPath($arType['DETAIL_PICTURE'])?>" alt="<?=$arType['NAME']?>">
                                        </div>
                                    <?endforeach?>
                                <?elseif($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'] === 'DEPENDENCY'):?>
                                    <?foreach($arPart['TYPES'] as $index => $arType):?>
                                        <div class="chcam_<?=$index+1?>_<?=$arResult['ID']?> chcam <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>camOn<?else:?>camOf<?endif?>">
                                            <img src="<?=CFile::GetPath($arType['DETAIL_PICTURE'])?>" alt="<?=$arType['NAME']?>">
                                        </div>
                                    <?endforeach?>
                                <?else:?>
                                    <?foreach($arPart['TYPES'] as $index => $arType):?>
                                        <div class="chmon_<?=$index?>_<?=$arResult['ID']?> chmon <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>camOn<?else:?>camOf<?endif?>">
                                            <img src="<?=CFile::GetPath($arType['DETAIL_PICTURE'])?>" alt="<?=$arType['NAME']?>">
                                        </div>
                                    <?endforeach?>
                                <?endif?>
                            <?else:?>
                                <img src="<?=CFile::GetPath($arPart['DETAIL_PICTURE'])?>" alt="<?=$arPart['NAME']?>">
                            <?endif?>
                        </div>
                        <div class="constructor-item__text">
                            <i class="fa fa-info-circle sinfo i" data-info="<?=$arPart['CODE']?>" aria-hidden="true"></i>
                            <div class="constructor-item__name constructor-item__name--ml"><?=$arPart['NAME']?></div>
                            <div class="constructor-item__price">
                                <?if (!empty($arPart['TYPES'])):?>
                                    <?if ($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'] === 'CALC'):?>
                                        <span><?=$arType['PRICE']?> руб.</span>
                                    <?elseif($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'] === 'DEPENDENCY'):?>
                                        <?foreach($arPart['TYPES'] as $index => $arType):?>
                                            <span class="chcam_<?=$index+1?>_<?=$arResult['ID']?> chcam <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>camOn<?else:?>camOf<?endif?>">
                                                <?=$arType['PRICE']?> руб.
                                            </span>
                                        <?endforeach?>
                                    <?else:?>
                                        <?foreach($arPart['TYPES'] as $index => $arType):?>
                                            <span class="chmon_<?=$index?>_<?=$arResult['ID']?> chmon <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>camOn<?else:?>camOf<?endif?>">
                                                <?=$arType['PRICE']?> руб.
                                            </span>
                                        <?endforeach?>
                                    <?endif?>
                                <?else:?>
                                    <span data-price="<?=$arPart['PRICE']?>" class='current-price'><?=intval($arPart['PRICE']) * 4?> руб.</span>
                                <?endif?>
                            </div>
                            <?if (!empty($arPart['TYPES'])):?>
                                <?if ($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'] === 'CALC'):?>
                                    <div class="constructor-item__buttons" data-price="<?=$arPart['PRICE']?>">
                                        <label class="constructor-item__button">
                                            <input type="" name="cam-" data-value="cam1" class="CAM">
                                            <span class="act_cam constructor-item__button_span" data-sign="-" data-num="<?=$arResult['ID']?>">-</span>
                                        </label>
                                        <label class="constructor-item__button">
                                            <input type="radio" name="camk<?=$arResult['ID']?>" data-value="cam1" value="4" class="CAM" checked="" disabled="">
                                            <span class="constructor-item__button_span " id="k_cam_<?=$arResult['ID']?>">4</span>
                                        </label>
                                        <label class="constructor-item__button">
                                            <input type="" name="cam+" data-value="cam2" class="CAM">
                                            <span class="act_cam constructor-item__button_span" data-sign="+" data-num="<?=$arResult['ID']?>">+</span>
                                        </label>
                                    </div>
                                <?elseif($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'] === 'DEPENDENCY'):?>
                                    <div class="constructor-item__buttons constructor-item__buttons-reg">
                                        <?foreach($arPart['TYPES'] as $index => $arType):?>
                                            <label class="chcam<?=$index+1?>_<?=$arResult['ID']?> constructor-item__button constructor-item__button<?=$arResult['ID']?>-reg
                                                <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>reg_checked<?endif?>"
                                            >
                                                <input type="radio" name="reg" value="<?=$arType['PRICE']?>" disabled="">
                                                <span class="">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    <span class="type-name"><?=$arType['NAME']?></span>
                                                </span>
                                            </label>
                                        <?endforeach?>
                                    </div>
                                <?else:?>
                                    <div class="constructor-item__buttons">
                                        <?foreach($arPart['TYPES'] as $index => $arType):?>
                                            <label class="constructor-item__button">
                                                <input type="radio" name="chmon<?=$arResult['ID']?>" value="<?=$index?>" <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>checked<?endif?>>
                                                <span class="act_mon" value="<?=$index?>" data-value="<?=$arType['PRICE']?>" data-num="<?=$arResult['ID']?>"><?=$arType['NAME']?></span>
                                            </label>
                                        <?endforeach?>
                                    </div>
                                <?endif?>
                            <?endif?>
                            <?if ($arPart['PROPERTY_BLOCK_LAYOUT_VALUE'] === 'CALC'):?>
                                <div class="constructor-item__descr descr--font">Укажите кол-во камер для предварительной оценки стоимости комлпекта</div>
                            <?endif?>
                        </div>
                    </div>
                <?endforeach?>
                 <input type="hidden" id="comp<?=$arResult['ID']?>_sum" value="<?=$sum?>">   
            </div>
        </div>
        <div class="your-kit__right">
            <div class="kit-constructor__result">
                <div class="kit-constructor__discount" data-tooltip="" data-discount="0.30" data-tippy="" data-original-title="Скидка до конца октября">
                    <span>-30%</span>
                </div>
                <div class="kit-constructor__old-price">
                    <span id="result_<?=$arResult['ID']?>_1"><?=$sum?></span> руб.
                </div>
                <div class="kit-constructor__total">
                    <span id="result_<?=$arResult['ID']?>"><?=$sum * 0.7?></span> руб.
                </div>
                <a href="javascript:;" class="btn btn--green calc_btn order-complect">Заказать этот комплект</a><br>
            </div>
            <div class="what-to-choose">
                <p class="what-to-choose__title">Не знаете, что выбрать?</p>
                <a class="btn btn--border get-consultation" href="#"><strong>Получить консультацию</strong></a>
                <div class="what-to-choose__phone">
                    <p>Или позвоните нам по номеру:</p>
                    <a href="tel:#SOTBIT_REGIONS_UF_PHONE#">#SOTBIT_REGIONS_UF_PHONE#</a>
                </div>
            </div>
        </div>
    </div>
</div>