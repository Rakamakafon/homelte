let complectOrderPopup,
	complectMessagePopup;

$(document).ready(function () {
	$(document).on('click', '.radioContainer', function () {
        $(this).parent().find('.radioContainer').removeClass('active');
        $(this).parents('.row').find('.active').removeClass('active');
        $(this).addClass('active');
    });

	$('.complectation-block-wrap [type="radio"]').on('change', function () {
        _calPrice($('.complectation-block-wrap'));
        $(this).parents('.right-col').find('.price').html($(this).val() + '  ₽')
    });

	$('.get-consultation').on('click', function () {
		let checkbox = $(this).closest('.feedback-wrap').find('input[type="checkbox"]'),
			phone = $(this).closest('.feedback-wrap').find('input[name=phone]'),
			data = {};

    	data.phone = phone.val();

		sendAjaxRequest(phone, checkbox, data, '/ajax/get_consultation.php');
	});

	$('.order-complect').on('click', function () {
		let complectForm = $(this).closest('.complectations');

		$.ajax({
			url: '/ajax/complect_order_modal.php',
			success: function (result) {
				let innerText = '',
					totalPrice = $('.complectations .price-calc-col .price').html();

				result = $(result);

				complectForm.find('.product-col').each(function(){

					innerText += $(this).find('.title').html();

					if ($(this).hasClass('long-card'))
					{
						innerText += ' ' + $(this).find('input[type=radio]:checked').siblings('.type-name').html();
					}


					innerText += ', ';
				});

				result.find('.under-title').html(innerText);
				result.find('.price-calc-col .price').html(totalPrice);

				complectOrderPopup = createPopupWindow(complectOrderPopup, 'complect-order', {
					content: result.html(),
				    closeIcon: {},
				    autoHide: true,
				    offsetLeft: 0,
		            offsetTop: 0,
		            width: 716,
		            height: 511,
		            overlay: true
				});

				complectOrderPopup.show();

                $('#complect-order').find('input[name=phone]').inputmask({ "mask": "+7 (999) 999-9999" });
  
				$('#complect-order').find('.call-me').on('click', function () {
					let form = $(this).closest('.form'),
						checkbox = form.find('input[type="checkbox"]'),
	                    phone = form.find('input[name="phone"]'),
	                    name = form.find('input[name="name"]').val(),
	                    data = {name: name, phone: phone.val(), complect: complectForm.data('complect'), desc: form.closest('.form-wraper').find('.under-title').html()};

					sendAjaxRequest(phone, checkbox, data, '/ajax/complect_order.php', complectOrderPopup);
	            });
			}
		});
	});

	$('input[name="phone"]').inputmask({ "mask": "+7 (999) 999-9999" });

	_calPrice($('.complectation-block-wrap'));

	$('.ic_info').click(function(){
		let types = [],
			data = {},
			inputs = $(this).closest('.product-card').find('.radioContainer input');

		inputs.each(function(){
			types.push($(this).data('type'));
		});
		
		data.types = types;

		$.ajax({
			data: data,
			method: 'POST',
			url: '/ajax/get_types_description.php',
			success: function (result) {
				complectMessagePopup = createPopupWindow(complectMessagePopup, 'popup-message', {
					content: result,
				    closeIcon: {},
				    autoHide: true,
				    offsetLeft: 0,
		            offsetTop: 0,
		            width: 716,
		            overlay: true
				});

				complectMessagePopup.show();
			}
		});
	});
});

function removeAlarm($elem, event, $class_elem) {
    $elem.on(event, function () {
        $(this).removeClass('alarm');
        if ($class_elem && $class_elem.length) $class_elem.removeClass('alarm');
    })
}

function _calPrice(block) {
    let $_price = $(block).find('.price-calc-col .price'),
        $_radios = $(block).find('[type="radio"]'),
        radio_names = [],
        $_prices = $(block).find('[prices]'),
        price_arr = [],
        total_price = 0;

    $_prices.each(function () {
        if ($(this).parents('.product-col:visible').length) price_arr.push($(this).attr('prices'));
    });

    $_radios.each(function () {
        if ($(this).parents('.radioContainer:visible').length) radio_names.push($(this).attr('name'));
    });

    radio_names = unique(radio_names);

    radio_names.forEach(function (e) {
        price_arr.push($(block).find('[name="' + e + '"]:checked').val());
    });

    price_arr.forEach(function (e) {
        total_price += Number(e);
    });

    $_price.html(numberToPretty(parseInt(total_price)) + ' ₽').data('price', total_price);
}

function unique(arr) {
    var obj = {};

    for (var i = 0; i < arr.length; i++) {
        var str = arr[i];
        obj[str] = true;
    }

    return Object.keys(obj);
}

function numberToPretty(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
}

function sendAjaxRequest(phone, checkbox, data, url, curPopup){
	if (!phone.inputmask('isComplete')) {
        phone.addClass('alarm');
        removeAlarm(phone, 'keyup');
    } 
    else if (!checkbox.prop('checked')) {
        checkbox.parents('label').addClass('alarm');
        removeAlarm(checkbox, 'change', checkbox.parents('label'));
    }
    else {
		$.ajax({
			data: data,
			method: 'POST',
			url: url,
			success: function (result) {
				if (curPopup)
					curPopup.close();

				complectMessagePopup = createPopupWindow(complectMessagePopup, 'popup-message', {
					content: result,
				    closeIcon: {},
				    autoHide: true,
				    offsetLeft: 0,
		            offsetTop: 0,
		            width: 716,
		            overlay: true
				});

				complectMessagePopup.show();
			}
		});
	}
}

function createPopupWindow(popup, name, params)
{
	if (popup)
		popup.destroy();

	return new BX.PopupWindow(name, null, params);
}