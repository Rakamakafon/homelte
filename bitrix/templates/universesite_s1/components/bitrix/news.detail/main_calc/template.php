<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="complectation-block-wrap block-wrap intec-content">
        <div class="row-flex page-rows complectations" data-complect="<?=$arResult['ID']?>">
            <h2 class="block-title">Комплект подключения включает:</h2>
            <?foreach($arResult['PARTS'] as $arPart):?>
                <div class="product-col <?if (!empty($arPart['TYPES'])):?>long-card<?endif?>">
                    <div class="product-card" data-product_key="<?=$arPart['CODE']?>" data-product="<?=$arPart['ID']?>" <?if (empty($arPart['TYPES'])):?>prices="<?=$arPart['PRICE']?>"<?endif?>>
                        <div class="left-col">
                            <div class="pic-wrap">
                                <img src="<?=CFile::GetPath($arPart['DETAIL_PICTURE'])?>">
                                <?if (!empty($arPart['PROPERTY_PART_TYPE_VALUE'])):?>
                                    <i class="ic_info"></i>
                                <?endif?>
                            </div>
                        </div>
                        <div class="right-col">
                            <div class="title"><?=$arPart['NAME']?></div>
                            <div class="price">
                                <?if ($arPart['PRICE'] === '0'):?>
                                    Бесплатно
                                <?else:?>
                                    <?=$arPart['PRICE']?> ₽
                                <?endif?>
                            </div>
                            <?if (!empty($arPart['TYPES'])):?>
                                <div class="packages">
                                    <?foreach($arPart['TYPES'] as $arType):?>
                                         <label class="radioContainer">
                                            <span class='type-name'><?=$arType['NAME']?></span>
                                            <input type="radio" name="<?=$arPart['CODE']?>" value="<?=$arType['PRICE']?>" 
                                                    data-type="<?=$arType['ID']?>" <?if($arType['PROPERTY_DEFAULT_TYPE_VALUE'] === 'Y'):?>checked<?endif?>>
                                            <span class="checkmark"></span>
                                        </label>
                                    <?endforeach?>
                                </div>
                            <?else:?>
                                <div class="descr">
                                    <?=$arPart['DETAIL_TEXT']?>
                                </div>
                            <?endif?>
                        </div>
                    </div>
                </div>
            <?endforeach?>
            <div class="price-calc-col">
                <div class="total">Итого:</div>
                <span class="price"></span>
                <div class="btn-wrap">
                    <button class="btn btn-primary order-complect">Заказать этот
                        комплект
                    </button>
                </div>
            </div>
            <div class="feedback-col">
                <div class="feedback-wrap">
                    <div class="title">Не знаете, что выбрать?</div>
                    <div class="descr">поможем выбрать подходяй для&nbsp;вас комплект</div>
                    <div class="bottom-row">
                        <div class="left-col">
                            <div class="form-control-custom">
                                <input type="text" name="phone" placeholder="Номер телефона">
                            </div>
                            <div class="sub-descr">или позвоните нам по номеру #SOTBIT_REGIONS_UF_PHONE#</div>
                        </div>
                        <div class="right-col">
                            <div><span class="btn btn-green get-consultation">Получить консультацию</span></div>
                            <div class=""><label class="checkbox agreement"><a>Я принимаю условия передачи
                                        информации</a><input type="checkbox" checked=""><span class="checkmark"></span></label></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>