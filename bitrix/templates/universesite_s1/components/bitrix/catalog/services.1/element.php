<?php if (defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true) ?>
<?php

use Bitrix\Main\Loader;
use intec\core\helpers\ArrayHelper;
use Bitrix\Main\Page\Asset;

$asset = Asset::getInstance();

$asset->addJs(SITE_TEMPLATE_PATH . "/js/jquery.inputmask.min.js");
$asset->addCss("/bitrix/css/main/font-awesome.css");

/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 * @var CBitrixComponentTemplate $this
 */

if (!Loader::includeModule('iblock'))
    return;

if (!Loader::includeModule('intec.core'))
    return;

$this->setFrameMode(true);

global $USER;
$arParams['DETAIL_TEMPLATE'] = 'default.3_custom';

include(__DIR__.'/parts/element/element.php');

?>
<div class="ns-bitrix c-catalog c-catalog-catalog-1 p-element">
    <?php $APPLICATION->IncludeComponent(
        'bitrix:catalog.element',
        $arElement['TEMPLATE'],
        $arElement['PARAMETERS'],
        $component
    ) ?>
</div>