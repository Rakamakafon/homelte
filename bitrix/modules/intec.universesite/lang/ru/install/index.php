<?php
$MESS['intec.universesite.installer.name'] = 'Решение Universe SITE';
$MESS['intec.universesite.installer.description'] = 'Мастер создания корпоративного сайта';
$MESS['intec.universesite.installer.modules.title'] = 'Нет необходимых модулей';
$MESS['intec.universesite.installer.modules.message'] = 'Не установлены следующие модули:';
$MESS['intec.universesite.installer.modules.back'] = 'К списку модулей';
$MESS['intec.universesite.installer.install.title'] = 'Установка модуля';
$MESS['intec.universesite.installer.install.message'] = 'Поздравляем, модуль успешно установлен!<br />
    Для установки готового сайта, пожалуйста перейдите <a href="/bitrix/admin/wizard_list.php?lang=ru">в список мастеров</a> <br />и выберите пункт «Установить» в меню мастера intec:universesite;';
$MESS['intec.universesite.installer.install.wizards'] = 'Открыть список мастеров';
$MESS['intec.universesite.installer.uninstall.title'] = 'Удаление модуля';
$MESS['intec.universesite.installer.uninstall.message'] = 'Удаление модуля успешно завершено';
$MESS['intec.universesite.installer.uninstall.modules.title'] = 'Выберите доп. модули для удаления';
$MESS['intec.universesite.installer.uninstall.modules.description'] = 'Вы можете удалить модули, приведенные ниже, если они не используются в других решениях.<br />Для их удаления отметьте их.';
$MESS['intec.universesite.installer.uninstall.modules.continue'] = 'Выберите доп. модули для удаления';
$MESS['intec.universesite.installer.uninstall.warning'] = 'Сейчас будет произведено удаление модуля';
$MESS['intec.universesite.installer.uninstall.go'] = 'Продолжить';
$MESS['intec.universesite.installer.uninstall.back'] = 'К списку модулей';