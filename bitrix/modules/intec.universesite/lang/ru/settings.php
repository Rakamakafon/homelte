<?php

$MESS['intec.universesite.settings.settingsDisplay'] = 'Отображать настройки сайта';
$MESS['intec.universesite.settings.settingsDisplay.none'] = 'Не отображать';
$MESS['intec.universesite.settings.settingsDisplay.admin'] = 'Только для администратора';
$MESS['intec.universesite.settings.settingsDisplay.all'] = 'Для всех';
$MESS['intec.universesite.settings.yandexMetrikaUse'] = 'Использовать Яндекс.Метрику';
$MESS['intec.universesite.settings.yandexMetrikaId'] = 'Идентификатор Яндекс.Метрики';
$MESS['intec.universesite.settings.yandexMetrikaClickMap'] = 'Включить карту кликов в Яндекс.Метрике';
$MESS['intec.universesite.settings.yandexMetrikaTrackHash'] = 'Включить отслеживание хеша браузера в Яндекс.Метрике';
$MESS['intec.universesite.settings.yandexMetrikaTrackLinks'] = 'Включить отслеживание ссылок в Яндекс.Метрике';
$MESS['intec.universesite.settings.yandexMetrikaWebvisor'] = 'Включить вебвизор в Яндекс.Метрике';