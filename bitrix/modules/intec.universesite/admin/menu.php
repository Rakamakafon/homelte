<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\Core;

/**
 * @var $arUrlTemplates
 */

Loc::loadMessages(__FILE__);

if (!CModule::IncludeModule('intec.core'))
    return;

Core::$app->web->css->addFile(Core::getAlias('@resources/intec.universesite/css/icons.css'));

$arMenu = [
    'parent_menu' => 'global_intec',
    'text' => Loc::getMessage('intec.universesite.menu'),
    'icon' => "intec-universesite-menu-icon",
    'page_icon' => 'intec-universesite-menu-icon',
    'url' => '/bitrix/admin/intec_universesite.php',
    'items_id' => 'intec_universesite',
    'items' => []
];

return $arMenu;