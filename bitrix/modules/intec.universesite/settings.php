<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

return [
    'settingsDisplay' => [
        'type' => 'list',
        'name' => Loc::getMessage('intec.universesite.settings.settingsDisplay'),
        'default' => 'admin',
        'values' => [
            'none' => Loc::getMessage('intec.universesite.settings.settingsDisplay.none'),
            'admin' => Loc::getMessage('intec.universesite.settings.settingsDisplay.admin'),
            'all' => Loc::getMessage('intec.universesite.settings.settingsDisplay.all')
        ]
    ],
    'yandexMetrikaUse' => [
        'type' => 'boolean',
        'name' => Loc::getMessage('intec.universesite.settings.yandexMetrikaUse'),
        'default' => 0
    ],
    'yandexMetrikaId' => [
        'type' => 'string',
        'name' => Loc::getMessage('intec.universesite.settings.yandexMetrikaId'),
        'default' => ''
    ],
    'yandexMetrikaClickMap' => [
        'type' => 'boolean',
        'name' => Loc::getMessage('intec.universesite.settings.yandexMetrikaClickMap'),
        'default' => 1
    ],
    'yandexMetrikaTrackHash' => [
        'type' => 'boolean',
        'name' => Loc::getMessage('intec.universesite.settings.yandexMetrikaTrackHash'),
        'default' => 1
    ],
    'yandexMetrikaTrackLinks' => [
        'type' => 'boolean',
        'name' => Loc::getMessage('intec.universesite.settings.yandexMetrikaTrackLinks'),
        'default' => 1
    ],
    'yandexMetrikaWebvisor' => [
        'type' => 'boolean',
        'name' => Loc::getMessage('intec.universesite.settings.yandexMetrikaWebvisor'),
        'default' => 0
    ]
];