<?php
namespace intec\core\base\condition\modifiers;

use Closure;
use intec\core\base\InvalidParamException;
use intec\core\base\condition\ResultModifier;

/**
 * Класс, представляющий модификатор результата условий на основе функции.
 * Class ConditionResultModifier
 * @package intec\core\base\condition\modifiers
 * @author apocalypsisdimon@gmail.com
 */
class ClosureResultModifier extends ResultModifier
{
    /**
     * Функция для обработки.
     * @var Closure
     */
    protected $_closure;

    /**
     * Возвращает функцию для обработки.
     * @return Closure
     */
    public function getClosure()
    {
        return $this->_closure;
    }

    /**
     * @inheritdoc
     * @param Closure $closure Функция для обработки.
     */
    public function __construct($closure, $config = [])
    {
        if (!($closure instanceof Closure))
            throw new InvalidParamException('Closure is not instance of function');

        $this->_closure = $closure;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function modify($condition, $data, $result)
    {
        return $this->_closure->call($this, $condition, $data, $result);
    }
}