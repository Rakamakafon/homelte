<?php
namespace intec\core\base\condition\providers;

use Closure;
use intec\core\base\InvalidParamException;
use intec\core\base\condition\DataProvider;

/**
 * Класс, представляющий провайдер данных на основе функций.
 * Class ConditionClosureDataProvider
 * @property Closure $closure Функция для обработки. Только для чтения.
 * @package intec\core\base\condition\providers
 * @author apocalypsisdimon@gmail.com
 */
class ClosureDataProvider extends DataProvider
{
    /**
     * Функция для обработки.
     * @var Closure
     */
    protected $_closure;

    /**
     * Возвращает функцию для обработки.
     * @return Closure
     */
    public function getClosure()
    {
        return $this->_closure;
    }

    /**
     * @inheritdoc
     * @param Closure $closure Функция для обработки.
     */
    public function __construct($closure, $config = [])
    {
        if (!($closure instanceof Closure))
            throw new InvalidParamException('Closure is not instance of function');

        $this->_closure = $closure;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function receive($condition)
    {
        return $this->_closure->call($this, $condition);
    }
}