<?
$MESS['GD_SW24_RS_DATE_SUPPORT_TO'] = 'Обновления модуля доступны до <span>#DATE#</span>';
$MESS['GD_SW24_RS_EXPIRED_SOON'] = 'Техническая поддержка заканчивается <span>#DAYS_STR#</span>';
$MESS['GD_SW24_RS_EXPIRED'] = 'Для установки обновлений требуется продлить лицензию';
$MESS['GD_SW24_RS_BUY'] = 'Купить продление';
$MESS['GD_SW24_RS_UPDATENOW'] = 'Доступны обновления';
$MESS['GD_SW24_RS_ERROR'] = 'Не удалось получить информацию об установке решения';
$MESS['GD_SW24_RS_THROUGH'] = 'через ';
$MESS['GD_SW24_RS_DAYS0'] = 'день';
$MESS['GD_SW24_RS_DAYS0_TODAY'] = 'сегодня';
$MESS['GD_SW24_RS_DAYS1'] = 'дня';
$MESS['GD_SW24_RS_DAYS2'] = 'дней';
$MESS['GD_SW24_RS_CURRENT_VERSION'] = 'Текущая версия';

$MESS['SKWB24_HI__DOCUMENTATION'] = 'Документация';
$MESS['SKWB24_HI__REVIEW'] = 'Оставить отзыв';
$MESS['SKWB24_HI__QUESTION'] = 'Задать вопрос';
$MESS['SKWB24_HI__PAYMENT'] = 'Заказать доработку';
$MESS['SKWB24_HI__MODULES'] = 'Все модули СкайВеб24';
?>