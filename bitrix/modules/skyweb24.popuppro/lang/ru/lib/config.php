<?
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_TIME_FAST']   = 'Быстро';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_TIME_NORMAL'] = 'Средне';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_TIME_SLOW']   = 'Медленно';

$MESS['skyweb24.popuppro_NOT_EFFECTS']   = 'Без анимации';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME_BOUNCE'] = 'Прыжок';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME_FLASH']  = 'Вспышка';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME_PULSE']  = 'Пульс';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME_RUBBERBAND'] = 'Резина';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME_SHAKE']   = 'Тряска';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME_SWING']   = 'Качание';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME_TADA']    = 'Та да!';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME_WOBBLE']  = 'Колебание';

$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEIN']       = 'Прыжок';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEINDOWN']   = 'Прыжок сверху';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEINLEFT']   = 'Прыжок слева';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEINRIGHT']  = 'Прыжок справа';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEINUP']     = 'Прыжок снизу';

$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEINDOWNBIG']     = 'Движение сверху';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEINLEFTBIG']     = 'Движение слева';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEINRIGHTBIG']    = 'Движение справа';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEINUPBIG']       = 'Движение снизу';

$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEIN']          = 'Вращение';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEINDOWNLEFT']  = 'Вращение слева вниз';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEINDOWNRIGHT'] = 'Вращение справа вниз';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEINUPLEFT']    = 'Вращение слева вверх';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEINUPRIGHT']   = 'Вращение справа вверх';

$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMIN']       = 'Увеличение';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMINDOWN']   = 'Увеличение сверху';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMINLEFT']   = 'Увеличение слева';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMINRIGHT']  = 'Увеличение справа';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMINUP']     = 'Увеличение снизу';

$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROLLIN']     = 'Специальный';



$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUT']       = 'Прыжок';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUTDOWN']   = 'Прыжок вниз';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUTLEFT']   = 'Прыжок влево';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUTRIGHT']  = 'Прыжок вправо';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUTUP']     = 'Прыжок вверх';

$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEOUTDOWNBIG']     = 'Движение вниз';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEOUTLEFTBIG']     = 'Движение влево';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEOUTRIGHTBIG']    = 'Движение вправо';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEOUTUPBIG']       = 'Движение вверх';

$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUT']           = 'Вращение';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUTDOWNLEFT']   = 'Вращение слева вниз';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUTDOWNRIGHT']  = 'Вращение справа вниз';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUTUPLEFT']     = 'Вращение слева вверх';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUTUPRIGHT']    = 'Вращение справа вверх';

$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUT']      = 'Уменьшение';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUTDOWN']  = 'Уменьшение вниз';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUTLEFT']  = 'Уменьшение влево';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUTRIGHT'] = 'Уменьшение вправо';
$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUTUP']    = 'Уменьшение вверх';

$MESS['skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROLLOUT'] = 'Специальный';

?>