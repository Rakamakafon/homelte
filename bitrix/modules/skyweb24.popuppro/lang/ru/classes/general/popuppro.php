<?
$MESS['skyweb24.popuppro_TYPE_DESCRIPTION']='Описание';
$MESS['skyweb24.popuppro_TYPE_TARGET']='Целевое действие';


/* === ТИПЫ И ШАБЛОНЫ ВСПЛЫВАЮЩИХ ОКОН === */
/* 1. Баннер */
$MESS['skyweb24.popuppro_TYPE_NAME_BANNER']='1. Баннер';
	$MESS['skyweb24.popuppro_TYPE_NAME_BANNER_T1']='Стандартный шаблон';

$MESS['skyweb24.popuppro_TYPE_NAME_BANNER_DESCRIPTION']='Данный тип всплывающего окна предназначен для показа изображения';
$MESS['skyweb24.popuppro_TYPE_NAME_BANNER_TARGET']='Переход по ссылке';
$MESS['skyweb24.popuppro_TYPE_NAME_BANNER_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson44/';
	
/* 2. Видео */
$MESS['skyweb24.popuppro_TYPE_NAME_VIDEO']='2. Видео';
	$MESS['skyweb24.popuppro_TYPE_NAME_VIDEO_T1']='Видео с YouTube';

$MESS['skyweb24.popuppro_TYPE_NAME_VIDEO_DESCRIPTION']='Данный тип всплывающего окна предназначен для показа видео';
$MESS['skyweb24.popuppro_TYPE_NAME_VIDEO_TARGET']='Просмотр видео';
$MESS['skyweb24.popuppro_TYPE_NAME_VIDEO_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson45/';
/* 3. Акция */
$MESS['skyweb24.popuppro_TYPE_NAME_ACTION']='3. Акция';
	$MESS['skyweb24.popuppro_TYPE_NAME_ACTION_T1']='Картинка слева';
	$MESS['skyweb24.popuppro_TYPE_NAME_ACTION_T2']='Картинка справа';
	$MESS['skyweb24.popuppro_TYPE_NAME_ACTION_T3']='Картинка сверху';

$MESS['skyweb24.popuppro_TYPE_NAME_ACTION_DESCRIPTION']='Данный тип всплывающего окна предназначен для отображения акции';
$MESS['skyweb24.popuppro_TYPE_NAME_ACTION_TARGET']='Переход по ссылке';
$MESS['skyweb24.popuppro_TYPE_NAME_ACTION_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson46/';

/* 4. Социальные сети */
$MESS['skyweb24.popuppro_TYPE_NAME_SOCIAL']='4. Социальные сети';
	$MESS['skyweb24.popuppro_TYPE_NAME_SOCIAL_T1']='Одна соц сеть';
	$MESS['skyweb24.popuppro_TYPE_NAME_SOCIAL_T2']='Все соц сети';

$MESS['skyweb24.popuppro_TYPE_NAME_SOCIAL_DESCRIPTION']='Данный тип всплывающего окна предназначен для показа виджетова социальных сетей';
$MESS['skyweb24.popuppro_TYPE_NAME_SOCIAL_TARGET']='Отсутствует';
$MESS['skyweb24.popuppro_TYPE_NAME_SOCIAL_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson47/';

/* 5. Сборщик контактов */
$MESS['skyweb24.popuppro_TYPE_NAME_CONTACT']='5. Сборщик контактов';
	$MESS['skyweb24.popuppro_TYPE_NAME_CONTACT_T1']='Деловой';
	$MESS['skyweb24.popuppro_TYPE_NAME_CONTACT_T2']='Анимированный';
	$MESS['skyweb24.popuppro_TYPE_NAME_CONTACT_T3']='Модный';
	$MESS['skyweb24.popuppro_TYPE_NAME_CONTACT_T4']='Простой';

$MESS['skyweb24.popuppro_TYPE_NAME_CONTACT_DESCRIPTION']='Данный тип всплывающего окна предназначен для отображения формы обратной связи';
$MESS['skyweb24.popuppro_TYPE_NAME_CONTACT_TARGET']='Получение заявки';
$MESS['skyweb24.popuppro_TYPE_NAME_CONTACT_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson48/';

/* 6. Поделиться в соц сети */
$MESS['skyweb24.popuppro_TYPE_NAME_SHARE']='6. Поделиться в соц сети';
	$MESS['skyweb24.popuppro_TYPE_NAME_SHARE_T1']='Стандартный шаблон';

$MESS['skyweb24.popuppro_TYPE_NAME_SHARE_DESCRIPTION']='Данный тип всплывающего окна предназначен для отображения расшаривания страницы сайта в социальные сети';
$MESS['skyweb24.popuppro_TYPE_NAME_SHARE_TARGET']='Отсутствует';
$MESS['skyweb24.popuppro_TYPE_NAME_SHARE_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson49/';

/* 7. HTML */
$MESS['skyweb24.popuppro_TYPE_NAME_HTML']='7. HTML';
	$MESS['skyweb24.popuppro_TYPE_NAME_HTML_T1']='Стандартный шаблон';

$MESS['skyweb24.popuppro_TYPE_NAME_HTML_DESCRIPTION']='Данный тип всплывающего окна предназначен для вставки стороннего HTML кода';
$MESS['skyweb24.popuppro_TYPE_NAME_HTML_TARGET']='Отсутствует';
$MESS['skyweb24.popuppro_TYPE_NAME_HTML_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson50/';

/* 8. 18+ */
$MESS['skyweb24.popuppro_TYPE_NAME_AGE']='8. Окно 18+';
	$MESS['skyweb24.popuppro_TYPE_NAME_AGE_T1']='Стандартный шаблон';

$MESS['skyweb24.popuppro_TYPE_NAME_AGE_DESCRIPTION']='Данный тип всплывающего окна предназначен для уведомления о возрастном контенте';
$MESS['skyweb24.popuppro_TYPE_NAME_AGE_TARGET']='Отсутствует';
$MESS['skyweb24.popuppro_TYPE_NAME_AGE_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson51/';
	
/* 9. Купоны */
$MESS['skyweb24.popuppro_TYPE_NAME_COUPON']='9. Купон на скидку';
	$MESS['skyweb24.popuppro_TYPE_NAME_COUPON_T1']='Стандартный шаблон';

$MESS['skyweb24.popuppro_TYPE_NAME_COUPON_DESCRIPTION']='Данный тип всплывающего окна предназначен для выдачи скидочного купона';
$MESS['skyweb24.popuppro_TYPE_NAME_COUPON_TARGET']='Получение заявки -> Выдача купона';
$MESS['skyweb24.popuppro_TYPE_NAME_COUPON_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson52/';

/* 10. Рулетка */
$MESS['skyweb24.popuppro_TYPE_NAME_ROULETTE']='10. Рулетка';
	$MESS['skyweb24.popuppro_TYPE_NAME_ROULETTE_T1']='Стандартный шаблон';

$MESS['skyweb24.popuppro_TYPE_NAME_ROULETTE_DESCRIPTION']='Данный тип всплывающего окна предназначен для розыгрыша призов';
$MESS['skyweb24.popuppro_TYPE_NAME_ROULETTE_TARGET']='Раздача призов случайным образом';
$MESS['skyweb24.popuppro_TYPE_NAME_ROULETTE_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson163/';

/* 11. Дисконтная карта */
$MESS['skyweb24.popuppro_TYPE_NAME_DISCOUNT']='11. Дисконтная карта';
	$MESS['skyweb24.popuppro_TYPE_NAME_DISCOUNT_T1']='Стандартный шаблон';

$MESS['skyweb24.popuppro_TYPE_NAME_DISCOUNT_DESCRIPTION']='Данный тип всплывающего окна предназначен для выдачи скидочного купона';
$MESS['skyweb24.popuppro_TYPE_NAME_DISCOUNT_TARGET']='Получение заявки -> Выдача купона';
$MESS['skyweb24.popuppro_TYPE_NAME_DISCOUNT_LINK_DOCUMENT']='https://skyweb24.ru/documentation/popuppro/lesson164/';


/* === ЦВЕТОВЫЕ СХЕМЫ ВСПЛЫВАЮЩИХ ОКОН === */
/* Все цветовые схемы формируются на основе https://flatuicolors.com/ */

/* Flat UI Palette v1 */
$MESS['skyweb24.popuppro_COLOR_MAIN']='Основная палитра';
$MESS['skyweb24.popuppro_ACTION_COLOR_GREEN']='Зелёная';
$MESS['skyweb24.popuppro_ACTION_COLOR_RED']='Красная';
$MESS['skyweb24.popuppro_ACTION_COLOR_BLUE']='Синяя';
$MESS['skyweb24.popuppro_ACTION_COLOR_WISTERIA']='Фиолетовая';
$MESS['skyweb24.popuppro_ACTION_COLOR_ORANGE']='Жёлтая';
$MESS['skyweb24.popuppro_ACTION_COLOR_PUMPKIN']='Темно-оранжевая';
$MESS['skyweb24.popuppro_ACTION_COLOR_GREENSEA']='Зелёное море';
$MESS['skyweb24.popuppro_ACTION_COLOR_MIDNIGHTBLUE']='Мокрый асфальт';
$MESS['skyweb24.popuppro_ACTION_COLOR_DARK']='Серебрянная';
$MESS['skyweb24.popuppro_ACTION_COLOR_ASBESTOS']='Серая';

/* Gradient Flat UI Palette v1 */
$MESS['skyweb24.popuppro_COLOR_GRAD']='Градиентная палитра';
$MESS['skyweb24.popuppro_ACTION_COLOR_GRAD_BLUE-WISTERIA']='Градиент: синий - фиолетовый';
$MESS['skyweb24.popuppro_ACTION_COLOR_GRAD_GREEN-BLUE']='Градиент: зелёный - синий';
$MESS['skyweb24.popuppro_ACTION_COLOR_GRAD_GREENSEA-BLUE']='Градиент: зелёное море - синий';
$MESS['skyweb24.popuppro_ACTION_COLOR_GRAD_GREENSEA-GREEN']='Градиент: зелёное море - зелёный';
$MESS['skyweb24.popuppro_ACTION_COLOR_GRAD_RED-ORANGE']='Градиент: красный - жёлтый';
$MESS['skyweb24.popuppro_ACTION_COLOR_GRAD_WISTERIA-RED']='Градиент: фиолетовый - красный';

/* Aussie Palette */
$MESS['skyweb24.popuppro_COLOR_AU']='Австралийская палитра';
$MESS['skyweb24.popuppro_au_Turbo']='Жёлтая';
$MESS['skyweb24.popuppro_au_QuinceJelly']='Оранжевая';
$MESS['skyweb24.popuppro_au_CarminePink']='Красная';
$MESS['skyweb24.popuppro_au_PureApple']='Зелёная';
$MESS['skyweb24.popuppro_au_HintOfIcePack']='Небесная';
$MESS['skyweb24.popuppro_au_GreenlandGreen']='Голубая';
$MESS['skyweb24.popuppro_au_SteelPink']='Фиолетовая';
$MESS['skyweb24.popuppro_au_Blurple']='Синяя';
$MESS['skyweb24.popuppro_au_DeepCove']='Тёмно-синяя';
$MESS['skyweb24.popuppro_au_WizardGrey']='Серая';

/* Canadian Palette */
$MESS['skyweb24.popuppro_COLOR_CA'] = 'Канадская палитра';
$MESS['skyweb24.popuppro_ca_LianHongLotusPink']='Розовая';
$MESS['skyweb24.popuppro_ca_DoubleDragonSkin']='Оранжевая';
$MESS['skyweb24.popuppro_ca_Amour']='Красная';
$MESS['skyweb24.popuppro_ca_Cyanite']='Голубая';
$MESS['skyweb24.popuppro_ca_DarkMountainMeadow']='Зелёная';
$MESS['skyweb24.popuppro_ca_AquaVelvet']='Морская';
$MESS['skyweb24.popuppro_ca_BleuDeFrance']='Светло-синяя';
$MESS['skyweb24.popuppro_ca_Bluebell']='Синяя';
$MESS['skyweb24.popuppro_ca_StormPetrel']='Серая';
$MESS['skyweb24.popuppro_ca_ImperialPrimer']='Тёмно-синяя';

/* Russian Palette */
$MESS['skyweb24.popuppro_COLOR_RU']='Российская палитра';
$MESS['skyweb24.popuppro_ru_SawtoothAak']='Персиковая';
$MESS['skyweb24.popuppro_ru_Summertime']='Жёлтая';
$MESS['skyweb24.popuppro_ru_Cornflower']='Синяя';
$MESS['skyweb24.popuppro_ru_Tigerlily']='Оранжевая';
$MESS['skyweb24.popuppro_ru_DeepRose']='Малиновая';
$MESS['skyweb24.popuppro_ru_PurpleCorallite']='Фиолетовая';
$MESS['skyweb24.popuppro_ru_FlamingoPink']='Розовая';
$MESS['skyweb24.popuppro_ru_BlueCuracao']='Голубая';
$MESS['skyweb24.popuppro_ru_PorcelainRose']='Красная';
$MESS['skyweb24.popuppro_ru_Biscay']='Тёмно-синяя';



/* === СВОЙСТВА ВСПЛЫВАЮЩИХ ОКОН === */
/* 1. Баннер */
$MESS['skyweb24.popuppro_BANNER_CONTENT_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_BANNER_CONTENT_IMG_1_SRC']='Изображение';
$MESS['skyweb24.popuppro_BANNER_CONTENT_LINK_HREF']='Ссылка';

/* 2. Видео */
$MESS['skyweb24.popuppro_VIDEO_CONTENT_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_VIDEO_CONTENT_LINK_VIDEO']='Код видео с YouTube';
$MESS['skyweb24.popuppro_VIDEO_CONTENT_LINK_VIDEO_HINT']='Нужно вставить <b>уникальный код видео</b> с YouTube.<br><br>Например:<br>Адрес видео - https://www.youtube.com/watch?v=IRjXFGdiPV8 <b>Нужно вставить - IRjXFGdiPV8</b><br>Адрес видео - https://youtu.be/IRjXFGdiPV8 <b>Нужно вставить - IRjXFGdiPV8</b>';
$MESS['skyweb24.popuppro_VIDEO_SERVICE_VIDEO_SIMILAR']='Показывать похожие?';
$MESS['skyweb24.popuppro_VIDEO_SERVICE_VIDEO_SIMILAR_HINT']='Позволяет <b>включить/выключить показ похожих видео</b> после завершения вашего.';
$MESS['skyweb24.popuppro_VIDEO_SERVICE_VIDEO_AUTOPLAY']='Автоматическое воспроизведение';
$MESS['skyweb24.popuppro_VIDEO_SERVICE_VIDEO_AUTOPLAY_HINT']='Позволяет <b>включить/выключить автоматическое воспроизведение</b> видео при появлении всплывающего окна.';
	
/* 3. Акция */
$MESS['skyweb24.popuppro_ACTION_CONTENT_IMG_1_SRC']='Изображение';
$MESS['skyweb24.popuppro_ACTION_CONTENT_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_ACTION_CONTENT_SUBTITLE']='Подзаголовок';
$MESS['skyweb24.popuppro_ACTION_CONTENT_CONTENT']='Содержание';
$MESS['skyweb24.popuppro_ACTION_CONTENT_LINK_TEXT']='Текст кнопки';
$MESS['skyweb24.popuppro_ACTION_CONTENT_LINK_HREF']='Ссылка';

/* 4. Социальные сети */
$MESS['skyweb24.popuppro_SOCIAL_SERVICE_COLOR_BG']='Цвет фона';
$MESS['skyweb24.popuppro_SOCIAL_SERVICE_ID_VK']='ID группы в ВКонтакте';
$MESS['skyweb24.popuppro_SOCIAL_SERVICE_ID_INST']='Имя аккаунта в Instagram';
$MESS['skyweb24.popuppro_SOCIAL_SERVICE_ID_ODNKL']='ID группы в Одноклассники';
$MESS['skyweb24.popuppro_SOCIAL_SERVICE_TYPE_VIEW']='Тип показа';
	$MESS['skyweb24.popuppro_SOCIAL_SERVICE_TYPE_VIEW_ALL']='Оба виджета';
	$MESS['skyweb24.popuppro_SOCIAL_SERVICE_TYPE_VIEW_RAND']='Случайный виджет';

/* 5. Сборщик контактов */
$MESS['skyweb24.popuppro_CONTACT_CONTENT_MAIN_IMG']='Изображение';
$MESS['skyweb24.popuppro_CONTACT_CONTENT_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_CONTACT_CONTENT_SUBTITLE']='Подзаголовок';
$MESS['skyweb24.popuppro_CONTACT_CONTENT_BUTTON_TEXT']='Текст кнопки';

$MESS['skyweb24.popuppro_CONTACT_TITLE']='Название поля';
$MESS['skyweb24.popuppro_CONTACT_PLACEHOLDER']='Текст подсказки';
$MESS['skyweb24.popuppro_CONTACT_ADD']='Добавлять в список адресов?';
$MESS['skyweb24.popuppro_CONTACT_ADD_HINT']='<b>Email</b> пользователя будет добавлен в список адресов модуля Email-маркетинг<br><br><a href="/bitrix/admin/sender_contacts.php?lang=ru" target="_blank">Перейти</a> к списку адресов.';
$MESS['skyweb24.popuppro_CONTACT_EMAIL_TO']='Отправлять письмо пользователю?';
$MESS['skyweb24.popuppro_CONTACT_EMAIL_TO_HINT']='При успешном заполнении формы пользовалю будет <b>отправлено письмо</b> с купоном на указанный им Email.';
$MESS['skyweb24.popuppro_CONTACT_TEMPLATE']='Шаблон письма';
$MESS['skyweb24.popuppro_CONTACT_TEMPLATE_HINT']='Вы можете отредактировать шаблон письма, нажав на ссылку рядом.';
$MESS['skyweb24.popuppro_CONTACT_UNIQUE']='Проверять на уникальность?';
$MESS['skyweb24.popuppro_CONTACT_UNIQUE_HINT']='Перед добавлением <b>Email пользователя будет проверен на уникальность</b> в списоке адресов модуля Email-маркетинг.';
$MESS['skyweb24.popuppro_CONTACT_CONSENT']='Соглашение';
$MESS['skyweb24.popuppro_CONTACT_CONSENT_LIST']='Выберите соглашение';

$MESS['skyweb24.popuppro_REGISTER_USER']='Регистрировать пользователя?';
$MESS['skyweb24.popuppro_REGISTER_USER_HINT']='Регистрация будет происходить <b>только в случае сбора Email</b> пользователя так как используется стандартный шаблон автоматической регистрации по Email';

$MESS['skyweb24.popuppro_CONTACT_EMAIL_SHOW']='Email';
$MESS['skyweb24.popuppro_CONTACT_EMAIL_REQUIRED']='Обязательное поле?';
$MESS['skyweb24.popuppro_CONTACT_EMAIL_TITLE']='Ваш email';
$MESS['skyweb24.popuppro_CONTACT_EMAIL_PLACEHOLDER']='Ваш почтовый адрес';

$MESS['skyweb24.popuppro_CONTACT_NAME_SHOW']='Имя';
$MESS['skyweb24.popuppro_CONTACT_NAME_REQUIRED']='Обязательное поле?';
$MESS['skyweb24.popuppro_CONTACT_NAME_TITLE']='Ваше имя';
$MESS['skyweb24.popuppro_CONTACT_NAME_PLACEHOLDER']='Имя';

$MESS['skyweb24.popuppro_CONTACT_PHONE_SHOW']='Телефон';
$MESS['skyweb24.popuppro_CONTACT_PHONE_REQUIRED']='Обязательное поле?';
$MESS['skyweb24.popuppro_CONTACT_PHONE_TITLE']='Ваш телефон';
$MESS['skyweb24.popuppro_CONTACT_PHONE_PLACEHOLDER']='Телефон';

$MESS['skyweb24.popuppro_CONTACT_DESCRIPTION_SHOW']='Комментарий';
$MESS['skyweb24.popuppro_CONTACT_DESCRIPTION_REQUIRED']='Обязательное поле?';
$MESS['skyweb24.popuppro_CONTACT_DESCRIPTION_TITLE']='Дополнительно';
$MESS['skyweb24.popuppro_CONTACT_DESCRIPTION_PLACEHOLDER']='Дополнительная информация';
$MESS['skyweb24.popuppro_CONTACT_DESCRIPTION_TITLE3']='Адрес';
$MESS['skyweb24.popuppro_CONTACT_DESCRIPTION_PLACEHOLDER3']='Введите адрес';
$MESS['skyweb24.popuppro_CONTACT_DESCRIPTION_TITLE4']='Адрес';

/* 6. Поделиться в соц сети */
$MESS['skyweb24.popuppro_SHARE_CONTENT_MAIN_IMG']='Изображение';
$MESS['skyweb24.popuppro_SHARE_CONTENT_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_SHARE_CONTENT_SUBTITLE']='Подзаголовок';
$MESS['skyweb24.popuppro_SHARE_CONTENT_VK']='Делиться в ВК';
$MESS['skyweb24.popuppro_SHARE_SERVICE_FB']='Делиться в Facebook';
$MESS['skyweb24.popuppro_SHARE_SERVICE_OD']='Делиться в Одноклассники';
$MESS['skyweb24.popuppro_SHARE_SERVICE_TW']='Делиться в Twitter';
$MESS['skyweb24.popuppro_SHARE_SERVICE_GP']='Делиться в Google+';
$MESS['skyweb24.popuppro_SHARE_SERVICE_MR']='Делиться в Mail.ru';

/* 7. HTML */
$MESS['skyweb24.popuppro_TYPE_NAME_HTML_SOMECODE']='Произвольный HTML-код';
$MESS['skyweb24.popuppro_HTML_CONTENT_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_HTML_CONTENT_TEXTAREA']='Вставьте HTML код';
$MESS['skyweb24.popuppro_HTML_CONTENT_TEXTAREA_HINT']='В данное поле вы можете <b>вставить свободный HTML код</b>.';

/* 8. 18+ */
$MESS['skyweb24.popuppro_AGE_CONTENT_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_AGE_CONTENT_BUTTON_Y']='Текст кнопки подтверждения';
$MESS['skyweb24.popuppro_AGE_CONTENT_BUTTON_Y_HINT']='Текст, который будет отображаться <b>на кнопке согласия</b> с возрастными ограничесниями.';
$MESS['skyweb24.popuppro_AGE_CONTENT_BUTTON_N']='Текст кнопки отрицания';
$MESS['skyweb24.popuppro_AGE_CONTENT_BUTTON_N_HINT']='Текст, который будет отображаться <b>на кнопке НЕ согласия</b> с возрастными ограничесниями.';
$MESS['skyweb24.popuppro_AGE_CONTENT_HREF_LINK']='Ссылка отрицания';
$MESS['skyweb24.popuppro_AGE_CONTENT_HREF_LINK_HINT']='<b>Ссылка, на которую переводит</b> пользователя <b>в случае НЕ согласия</b> с возрастными ограничесниями.';
$MESS['skyweb24.popuppro_AGE_CONTENT_MAIN_IMG']='Изображение';

/* 9. Купон на скидку */
$MESS['skyweb24.popuppro_COUPON_CONTENT_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_COUPON_CONTENT_SUBTITLE']='Подзаголовок';
$MESS['skyweb24.popuppro_COUPON_CONTENT_MAIN_IMG']='Фоновое изображение';
$MESS['skyweb24.popuppro_COUPON_CONTENT_BUTTON_TEXT']='Текст кнопки';
$MESS['skyweb24.popuppro_COUPON_CONTENT_MAIN_RULE_ID']='Правило работы с корзиной';
$MESS['skyweb24.popuppro_COUPON_CONTENT_MAIN_RULE_ID_HINT']='Выберите <b>правило работы с корзиной</b>, на основе которого модуль будет генерировать купоны.<br><br><a href="/bitrix/admin/sale_discount.php?lang=ru" target="_blank">Перейти</a> к правилам работы с корзиной.';
$MESS['skyweb24.popuppro_COUPON_CONTENT_MAIN_TIMING']='Время действия купона в днях';
$MESS['skyweb24.popuppro_COUPON_CONTENT_MAIN_TIMING_HINT']='Укажите на <b>сколько дней будет действовать</b> сгенерированный купон.';
$MESS['skyweb24.popuppro_COUPON_CONTENT_EMAIL_NOT_NEW']="Сообщение о неуникальности";
$MESS['skyweb24.popuppro_COUPON_CONTENT_EMAIL_NOT_NEW_HINT']="Данное <b>сообщение будет выводиться</b> если пользователь указал <b>Email, который уже существует</b> в списке адресов модуля Email-маркетинг.";

/*10. Рулетка*/
$MESS['skyweb24.popuppro_ROULETTE_CONTENT_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_ROULETTE_CONTENT_SUBTITLE']='Подзаголовок';
$MESS['skyweb24.popuppro_ROULETTE_CONTENT_BUTTON_TEXT']='Текст кнопки';
$MESS['skyweb24.popuppro_ROULETTE_CONTENT_MAIN_IMG']='Картинка';
$MESS['skyweb24.popuppro_ROULETTE_CONTENT_MAIN_RULE_ID']='Правило работы с корзиной';
$MESS['skyweb24.popuppro_ROULETTE_CONTENT_MAIN_RULE_ID_HINT']='Выберите правило работы с корзиной, на основе которого модуль будет генерировать купоны.<br><br><a href="/bitrix/admin/sale_discount.php?lang=ru" target="_blank">Перейти</a> к правилам работы с корзиной';
$MESS['skyweb24.popuppro_ROULETTE_CONTENT_EMAIL_NOT_NEW']="Сообщение о неуникальности";
$MESS['skyweb24.popuppro_ROULETTE_CONTENT_EMAIL_NOT_NEW_HINT']="Данное <b>сообщение будет выводиться</b> если пользователь указал <b>Email, который уже существует</b> в списке адресов модуля Email-маркетинг.";
$MESS['skyweb24.popuppro_ROULETTE_RESULT_TITLE']="Текст успеха";
$MESS['skyweb24.popuppro_ROULETTE_RESULT_HINT']="Текст, который увидит пользователь в случае выпадения <b>положительного результата</b>.";
$MESS['skyweb24.popuppro_ROULETTE_NOTHING_TITLE']="Текст неудачи";
$MESS['skyweb24.popuppro_ROULETTE_NOTHING_HINT']="Текст, который увидит пользователь в случае выпадения <b>отрицательного результата</b>.";
$MESS['skyweb24.popuppro_ROULETTE_TEMPLATE']="Шаблон письма";
$MESS['skyweb24.popuppro_ROULETTE_TEMPLATE_HINT']="Шаблон письма, который <b>будет отправлен пользователю</b>.";

/*11. Дисконтная карта */
$MESS['skyweb24.popuppro_DISCOUNT_USERGROUP_TITLE']='Группа пользователей';
$MESS['skyweb24.popuppro_DISCOUNT_USERGROUP_TITLE_HINT']='<b>Выберите группу пользователей</b>, в которую будет добавлен пользователь после заполнения формы.';
$MESS['skyweb24.popuppro_DISCOUNT_MASK_TITLE']='Маска генерации купона';
$MESS['skyweb24.popuppro_DISCOUNT_MASK_TITLE_HINT']='Укажите <b>маску для генерации</b> порядкового номера купона.<br><br><b>Обратите внимание!</b> Вы можете <b>изменить размерность и шаблон</b> генерации номера купона.<hr>В нашем примере:<br><b>0 - Строго указаная цифра</b> (вы можете указать любую другую)<br><b># - Автоматически изменяемое значение</b> (будет автоматически увеличиться на 1 при генерации купона)<br>Т.е. по нашему примеру будут создаваться следующие купоны:<br>0000<b>00001</b><br>0000<b>00002</b><br>0000<b>00003</b><br>...<br>0000<b>00010</b><br>...<br>0000<b>99999</b>';
$MESS['skyweb24.popuppro_DISCOUNT_LASTNAME_SHOW']='Фамилия';
$MESS['skyweb24.popuppro_DISCOUNT_LASTNAME_REQUIRED']='Обязательное поле?';
$MESS['skyweb24.popuppro_DISCOUNT_LASTNAME_TITLE']='Фамилия';
$MESS['skyweb24.popuppro_DISCOUNT_EMAIL_SHOW']='Email';
$MESS['skyweb24.popuppro_DISCOUNT_EMAIL_REQUIRED']='Обязательное поле?';
$MESS['skyweb24.popuppro_DISCOUNT_EMAIL_TITLE']='E-mail';
$MESS['skyweb24.popuppro_DISCOUNT_EMAIL_NOT_NEW']='На этот Email уже зарегистрирована дисконтная карта';
$MESS['skyweb24.popuppro_DISCOUNT_NAME_SHOW']='Имя';
$MESS['skyweb24.popuppro_DISCOUNT_NAME_REQUIRED']='Обязательное поле?';
$MESS['skyweb24.popuppro_DISCOUNT_NAME_TITLE']='Имя';
$MESS['skyweb24.popuppro_DISCOUNT_PHONE_SHOW']='Телефон';
$MESS['skyweb24.popuppro_DISCOUNT_PHONE_REQUIRED']='Обязательное поле?';
$MESS['skyweb24.popuppro_DISCOUNT_PHONE_TITLE']='Телефон';



/* === ЗНАЧЕНИЯ ПО УМОЛЧАНИЮ ВСПЛЫВАЮЩИХ ОКОН === */
/* 1. Баннер */

/* 2. Видео */
$MESS['skyweb24.popuppro_BANNER_LINK_VIDEO']='5bRPTb3z2rU';

/* 3. Акция */
$MESS['skyweb24.popuppro_ACTION_TITLE']='Уже уходите?';
$MESS['skyweb24.popuppro_ACTION_SUBTITLE']='Хотите его получить?';
$MESS['skyweb24.popuppro_ACTION_CONTENT']='Мы подготовили подарок специально для Вас';
$MESS['skyweb24.popuppro_ACTION_LINK_TEXT']='Забрать подарок';

/* 4. Социальные сети */
$MESS['skyweb24.popuppro_SOCIAL_TITLE']='Будьте в курсе наших новостей';

/* 5. Сборщик контактов */
$MESS['skyweb24.popuppro_CONTACT_TITLE1']='Чёрная пятница';
$MESS['skyweb24.popuppro_CONTACT_TITLE2']='Оставьте свои данные мы с вами свяжемся!';
$MESS['skyweb24.popuppro_CONTACT_TITLE3']='Бесплатная консультация специалиста';
$MESS['skyweb24.popuppro_CONTACT_TITLE4']='Не уходите с пустыми руками!';
$MESS['skyweb24.popuppro_CONTACT_SUBTITLE']='Запишись и получи скидку до 70%';
$MESS['skyweb24.popuppro_CONTACT_SUBTITLE2']='15% скидка на услуги мастера';
$MESS['skyweb24.popuppro_CONTACT_SUBTITLE3']='Оставьте свои данные';
$MESS['skyweb24.popuppro_CONTACT_SUBTITLE4']='Подпишитесь на наши Email рассылки и вы найдете для себя интересную и полезную информацию';
$MESS['skyweb24.popuppro_CONTACT_SEND_BUTTON']='Записаться';
$MESS['skyweb24.popuppro_CONTACT_SEND_BUTTON2']='отправить';
$MESS['skyweb24.popuppro_CONTACT_SEND_BUTTON3']='Подать заявку';
$MESS['skyweb24.popuppro_CONTACT_SEND_BUTTON4']='Подписаться';

/* 6. Поделиться в соц сети */
$MESS['skyweb24.popuppro_SHARE_TITLE']='Понравилась статья?';
$MESS['skyweb24.popuppro_SHARE_SUBTITLE']='Расскажите о ней друзьям!';

/* 7. HTML */


/* 8. 18+ */
$MESS['skyweb24.popuppro_AGE_CONTENT_TITLE_DEF']='Внимание! Данная страница содержит материалы для взрослых!';
$MESS['skyweb24.popuppro_AGE_CONTENT_BUTTON_Y_DEF']='Да, мне больше 18 :)';
$MESS['skyweb24.popuppro_AGE_CONTENT_BUTTON_N_DEF']='Нет, мне меньше 18 :(';

/* 9. Купон на скидку */
$MESS['skyweb24.popuppro_COUPON_CONTENT_TITLE_DEFAULT']='Не торопитесь уходить!';
$MESS['skyweb24.popuppro_COUPON_CONTENT_SUBTITLE_DEFAULT']='Мы приготовили для Вас специальный подарок - купон на скидку!';
$MESS['skyweb24.popuppro_COUPON_CONTENT_BUTTON_TEXT_DEFAULT']='Получить купон';
$MESS['skyweb24.popuppro_COUPON_CONTENT_MAIN_RULE_ID_DEFAULT']='';
$MESS['skyweb24.popuppro_COUPON_CONTENT_MAIN_TIMING_DEFAULT']='30';
$MESS['skyweb24.popuppro_COUPON_CONTENT_EMAIL_NOT_NEW_DEFAULT']='Данный Email уже используется';

/*10. Рулетка */
$MESS['skyweb24.popuppro_TYPE_ROULETTE_TITLE_DEFAULT']='Испытайте удачу!';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_RESULT_DEFAULT']='Поздравляем, Вы выиграли!<br>Ваш подарок был отправлен на указанный вами Email';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_NOTHING_DEFAULT']="К сожалению удача обошла вас стороной";
$MESS['skyweb24.popuppro_TYPE_ROULETTE_SUBTITLE_DEFAULT']='У вас есть шанс получить скидку!';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_PLACEHOLDER_DEFAULT']='Введите ваш Email';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_BUTTON_DEFAULT']='Испытать удачу';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_COLOR_1_DEFAULT']='#ff9ff3';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_COLOR_2_DEFAULT']='#01a3a4';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_COLOR_3_DEFAULT']='#feca57';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_COLOR_4_DEFAULT']='#54a0ff';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_COLOR_5_DEFAULT']='#ff6b6b';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_COLOR_6_DEFAULT']='#5f27cd';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_COLOR_7_DEFAULT']='#48dbfb';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_COLOR_8_DEFAULT']='#1dd1a1';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_TEXT_1_DEFAULT']='Не удача';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_TEXT_2_DEFAULT']='Скидка 30%';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_TEXT_3_DEFAULT']='Не повезло';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_TEXT_4_DEFAULT']='Скидка 20%';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_TEXT_5_DEFAULT']='Не в этот раз';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_TEXT_6_DEFAULT']='Без выигрыша';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_TEXT_7_DEFAULT']='Скидка 10%';
$MESS['skyweb24.popuppro_TYPE_ROULETTE_TEXT_8_DEFAULT']='Увы';

/*11. Дисконтная карта */
$MESS['skyweb24.popuppro_TYPE_DISCOUNT_TITLE_DEFAULT']='<span>70%</span><span>Скидка-</span><br>это реально!';
$MESS['skyweb24.popuppro_TYPE_DISCOUNT_SUBTITLE_DEFAULT']='<b>Закажите vip карту!</b> Именная дисконтная карта дает возможность получать реальные скидки до 70%';
$MESS['skyweb24.popuppro_TYPE_DISCOUNT_BUTTON_DEFAULT']='Подписаться';

/* Окно успеха */



$MESS['skyweb24.popuppro_WINDOW_SUCCESS_TEXTAREA']='Текстовое поле';
$MESS['skyweb24.popuppro_WINDOW_SUCCESS_TEXTAREA_VALUE_DEFAULT']='В ближайшее время с вами свяжется наш специалист';
$MESS['skyweb24.popuppro_WINDOW_SUCCESS_TEXTAREA_HINT']='В данном поле вы можете написать <b>любой текст</b> в тело окна успеха, а также HTML разметку';

$MESS['skyweb24.popuppro_WINDOW_SUCCESS_TITLE']='Заголовок';
$MESS['skyweb24.popuppro_WINDOW_SUCCESS_TITLE_VALUE_DEFAULT']='Ваша заявка принята';
$MESS['skyweb24.popuppro_WINDOW_SUCCESS_TITLE_HINT']='В данном поле вы можете написать <b>любой текст</b> в заголовок окна успеха, а также HTML разметку';

$MESS['skyweb24.popuppro_WINDOW_SUCCESS_ACTIVE']='Автивность';
$MESS['skyweb24.popuppro_WINDOW_SUCCESS_ACTIVE_VALUE_DEFAULT']='Заголовок окна успеха';
$MESS['skyweb24.popuppro_WINDOW_SUCCESS_ACTIVE_HINT']='Служит для <b>включения/выключения</b> "работа окно успеха"';





$MESS['skyweb24.popuppro_BUTTON_WINDOW_POPUP_TITLE_VALUE_DEFAULT']='Заголовок окна успеха';

/* === ГРУППЫ ПОЛЕЙ И НАСТРОЕК ВСПЛЫВАЮЩИХ ОКОН === */
$MESS['skyweb24.popuppro_SET_CONTENT']='Контент';
$MESS['skyweb24.popuppro_SET_SETTINGS']='Настройки';
$MESS['skyweb24.popuppro_SET_EFFECTS']='Эффекты';
$MESS['skyweb24.popuppro_SET_POSITION']='Позиционирование';
$MESS['skyweb24.popuppro_SET_TIMER']='Таймер';



/* === НАСТРОЙКИ ВСПЛЫВАЮЩИХ ОКОН === */
$MESS['skyweb24.popuppro_HREF_TARGET']='Открывать ссылку';
$MESS['skyweb24.popuppro_HREF_TARGET_BLANK']='в новой вкладке';
$MESS['skyweb24.popuppro_HREF_TARGET_SELF']='в текущей вкладке';

$MESS['skyweb24.popuppro_GOOGLE_FONT_NAME']='Название шрифта с Google.Fonts';
$MESS['skyweb24.popuppro_GOOGLE_FONT_HINT']='Для настройки шрифта вам необходимо на сайте <a href="https://fonts.google.com" target="_blank">Google Fonts</a> выбрать нужный вам шрифт и вставить в поле <b>только его название</b> как показано на рисунке:<hr><img width="380px" src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABJAAD/4QMqaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0MiA3OS4xNjA5MjQsIDIwMTcvMDcvMTMtMDE6MDY6MzkgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTc3MjFERTQ1RjA3MTFFODlENzc5REE5RTk2REU2MUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTc3MjFERTU1RjA3MTFFODlENzc5REE5RTk2REU2MUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5NzcyMURFMjVGMDcxMUU4OUQ3NzlEQTlFOTZERTYxRCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5NzcyMURFMzVGMDcxMUU4OUQ3NzlEQTlFOTZERTYxRCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uACZBZG9iZQBkwAAAAAEDABUEAwYKDQAACRQAAA5sAAAUawAAGx3/2wCEAAQCAgIDAgQDAwQFAwMDBQYEBAQEBgcGBgYGBgcJBwgHBwgHCQkKCwsLCgkMDAwMDAwPDw8PDxEREREREREREREBBAQEBwYHDQkJDRMODQ4TFBEREREUEREREREREREREREREREREREREREREREREREREREREREREREREREREREREf/CABEIAFEBkAMBEQACEQEDEQH/xADfAAEAAgMBAQAAAAAAAAAAAAAAAwQBAgUGBwEBAAMBAQAAAAAAAAAAAAAAAAECAwQFEAABBQABAgQFAwUAAAAAAAABAAIDBBQFERMQUBIVIHAhMjNBIhcwQGAxBhEAAgAGAAQDBQYEBwAAAAAAAQIAESExEgNBIhMEUXEyEGGBkaEgULFCIxRSYtIzMEBgweFDUxIAAgEBBgcBAAAAAAAAAAAAAAExEVBw8CFhcSCQQYGh4TICEwEAAgIBAgUEAgMBAQAAAAABABEhMWFBURDwkdHxUHGBsaHBIDBw4WD/2gAMAwEAAhEDEQAAAfv4AAAAAAAAAAAAAAAAAAAAAAAABVpv5vn9q1bn5ufbbthBGlm2PqerwtYnh4+ppFsJmnOOLdPXiu35gAAAAAAAAAAAAAAAAAAAABFF5ZoAAAAAAAAAAAAAAAAAAANSAAAAAAAAAAAAAAAAAAAAAAAExuCIomhg2BkGhqTAGCMkMgAiJDJgyAYMmhqSkBORGSQGDQyREgJAC4TgiKJSOURnbOeVy8blMpHTLhSITYlKZkiJygdIvFEiLJoSlQGxsecp0cuLdIwei05eUdchOae3OWTl8uE4IiiUDcplsoEoITqmxMAAAAAAAAAADw3N7PPruB9F6vBkmoAAAuE4IiiUywVgREpkwWjJsAAAAAAAAAAAAAAAAAXCcGhXAAAAAAAAAAAAAAAAAAAAAABOSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/9oACAEBAAEFAvM7vXIY/Xx73Tsvt3tgDpO1NseyD1ak49Gskf62usMfHBYKIlEleJ5dQZ6anljYY2u8oc7o3QFoC0BaAtAWgLQFoC0BaAtAWgLQFoC0BaAtAWgLQFoC0BaAtAWgLQFoC0BaAtAWgLQFoC0BaAtAWgLQFoC0BaAtAWgLQFoC0BaAmO9Q8Jfxp72sZHLE+Nz2tTnBrR9Qf9RSxysbPA74OoUk0UbP0+CaaKGIEELqOvwl7A9k0T3p1mu1670PcdLGJPAkAdyP1Mex7Raqld2LrHJG8eMH2eEv416LTbEUN0RycbyJhfG91GLj5Qvbrr3YrDZLtISqDjn6G1LLFDXtNm7bzNJx/ehmqX3S+3OFilFciVrvnknU7/bjNgzityTrWbkmgV7nfotsx1slvtV6HINsPq2DG+rc70FKQxy8ZOzj4KN4TxMvtpRQ25WWxBHy0kzXNdaqyvZfrBclQtWuPfwPJvtcPXtU697iJpVT4DkY7TWNaLFSw5cbDPHUUH2eEv41olbaZPJKJeXaINjO77xFBWm5RoXvVLu+/V3Q+ucOgnimj/tuU/4Hir1/+MOEX8YcIv4w4RRRiOL+hB9nhL+NGjXNiKGKJnt9Xo2m3Szi6jWGhVMgo1hIOPrCEsaWNa1o8jg+zwe3q3OVnKzlZys5WcrOVnKzlZys5WcrOVnKzlZys5WcrOVnKzlZys5WcrOVnKzlZys5WcrOVnKzlZys5WcrOVnKzlZys5WcrOVnKzlZyo2+lv8Akv8A/9oACAECAAEFAvMyv0/X6+I+Hp4BN+bnqC9QXqC9Q8vMYK7IXZC7Q+an/9oACAEDAAEFAvM4/u69HjoWfsJ+nVvpCd9vgQugKLgvp0e5Sn93lhd8rxE8rsSLsSLsSeXx3XMb7i9e4vXuL0T1+af/2gAIAQICBj8C5E3/2gAIAQMCBj8CtNC9GcvM7or+umEbs0p54KYgrsaXvQfLPlkOz6EIhEK9T//aAAgBAQEGPwL7z2SLKcboJsPKNklaaOuJUvIzInjOvnAfWj/tu3I03pzXMrmsoRG6jA6tzq1Zjl9J+NofX242HXuCaxf1VzkW931hQUZtmjU6tzMJFSOYSuZRyF239YTPNI6sROfD2g1yPGtbQNgybFSCvm5ga6sQNlWJFcrxIlm3hlwNZY0n7vGJOCFZXNzzV4+EJ4kAtPx+7ZqJT+6ZxaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRP2n2FmMlWpMK6MGTZ6SOPlAyMsjIT8YJNAKkxMcYnwgPrOamxEUcGZxHn4fZLuwVVuT9o7NrYa1uxiYqD7JcfseXsClgGayk1PlDKrBm10YDh7EVnAbbRP5j7vZ0816l8JjL5QqEgPsnivEyvL2zNAOJjHIZSylOsvGMkIdTxUzENLbrPT9cmHL5wBks3qonce6JoQw932z7G25dXXLk1DlkfwPxjs59u/U0MepLYlFr/NIwoR8Nk1ZmZsuYFp390oKSxdtZXEmdZeMDW2XRDhpZm3Trxn6o7obmL69ysEE5Az9P5p08h8Y09J8NOkAMmb1+tfjGsKJKrlmxOP5SOEO+1SzdxoXW+zM+oCRpP6iO37dEdG16v/AEopDrzmtfKO5LBimyePPU+VafIRrmJJqWfjzGn0Ea+ovP1hufmKy+R4CNmMxlPmz9SzEgBwlGjYUOzoPsA/UNEb0mprKNxZS0zNMnmW+pH4eUHXrJ6rAlWD0VcOK+fGOUMqzbBOrXWaSYmdeNI7lEDbtnT2K0noWZuWhMlpGjZXVrRFBSYoRcGTcfIwE2q7dXYSqLsl/wBZ45HjW/wjbyP+poVDtGwVceAnT5RrR0rMz5p4r82/GGBV2OQOz9X+6Jn015fpCbdhLY+rnNqcPnGvlLTLPsVXwObWM/dHcSyx2rynP6LX/YecaOquJ1GbcTT0icyfqYbV2wId9jMedjQkyNWH4wNu3J/4wNl+U++Udtr/AG75atgLg7EMlE+Jat43YbKK3T0tkwpOZNK+74R1NvedumzV0wQ+1Mgyowy5uehIpxhD+805a88R+9XJWbWFzynbKuMdym7v9BTdr2Jl+6WTZSwkoPLjxjtQnd6dK6lQEfvFOBDcxPNz5CgjuNWjFR3DFv1HLjYhXhL0g+EFuVZ6iMwaTOjp4/xXr4Rhs117jcTipHIMbnGnCO52IiLsZtY1qMeZEYOZ8OY+MdqzSx04MxmOXEvy+P5uFI5QF8o7h9bY7XULpORkKVpAXaSWmTXgPC7fj9k+zarybWmsbFwU5XIlcztCPqEkJ5uqCrfKNuzXqfb0GClRiCZmXFvxjpSZdpkMT71nOhtCN3ZGbM6cslBwMp8zfScbBrUs2pkWsq5MBSs+PGNeozXZtpi2MwZlaifiOE4Ozt0buMXRCqFPzmQPq/5gsR+kBMAVeM9bZLb4j/L7e727dy7N5yITGXhxEf3+4+af0x/f7j5p/TH9/uPmn9MLrFtahB8BL/EPsbc2TNsXBlZiUI8MDyxhrVdaD8qAAfSNgkT1qNNj9PD4R1iZldfSS+UuMzAUZ8rFwTscmZvWc6+EFyCS0rsaSOVPCogOAQwmaE1mZ18amG1SODVlk1JWxrT4RgeZZSM4kBICwH3PKLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeLxeJf6m//2gAIAQEDAT8h+plggQffAdZppff4hU40yXCWENKx1TQsuGdzzlCuazL/AMzZM6lFZ6mO9RViHKRQmO5zH3RXTIAde3PgiJlBaiSWqSmRFvVNpXYg/lkhuqejX4irUynyirH8R2assxLMadVwFZwX3Meix07w8FL4K4Zu/prC42pazxr6TdbVOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdOdBwq8fM+/hVGiroHWCkAXYLkjBCp3y0ESMOo0BtYgMgLE6jEFmBlXsQ6PDTFAbxjsl/lRP6hksyeCG2rwX3lcJxmr0QbCZHIzrXXxphzC3qhzCKULE7MpnUO6uteFMqCN1nRrp4X5rqCjdNsYcG0t2ZTGSSmujKeDuZKaTvldTJnBOndHBuOC3B3fBkoC3AB3YPUvQX4W65gzRAifZI+GQoXQ3vj8wQ7FUN94dTklPqat2X/h+98fM+/hn5ZsC7BdWd1iK4AMAwGwzbHq4ixkWlbQCxkMf1A/WTBKtd+esGZaRv0CzBtVyr5WO4vIqpiDIxELafkQpltGPJm6u0ggcaqFCg7soPSFNq66MsvRLrJp0QiI5c1uiqBWBsOdyzvs7pY31bM8y2tgQ7LVleAikrmFgjXmZG2i+bj1CLKzJTTqG37zGy4Wi226B+IV43EFmVb9+PXd4igZYeVM/wBRFk712ATaBwjBUdLWDFzbKZldG0pjk/DC3RRmOddYJov0QJ4ikh21m3urrz3mtFi2ZEW7OhQvvCyZJKUWatK66VzEYyMGEArKno1nca1db1QrYs1rP2YtxcGGrZWgYvf3F2ULKZ99WdLNodi6mTaPQQLlllPyzLijebhQUFtdo+keJYJU+ouIIqKz8bxZYXOMfBfbBXrVZw6TrvZL1YdR+106TaR9TwRdDY3fWW1SQ9+W1MJmuIHS8qs1myK7PWMVxaMNjrAPVhzDDRqiOdArep37zBqLjLEmVnZguW/Et9uXW1hNcM9JVAN0A/UfYYPQ+qBb1mBcRW0cYavv8P3vj5n38MWiahtC/AUEu44EcCaX53E6yI3DwV9qPFS8XoIjV12ClI07xEld+zTQO1WCe1w1GxSmgvwKA9LJnlTIGFMlyGr8SzhuT1sND7KdwRBvZGyjFFnfUpivbHTAiPU/zt8Lf91rXLVtGFr0/wAAACA9qsjtCF+n+n974+Z9/D8oCI5D/CBMtAKvgBMgEcpaGwa4DkwpgNxEqCbSqqvWXpQBzlN8XVamKPZZJRwrWAtbj90wMsOt1kJeoEBc7lXZlV1hUprZSzs5vcAmNQKA4D6J+98bLS55hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJ5hPMJtF/wD03//aAAgBAgMBPyH6nrKuGzgYmnrPt57xtm3N/wAeIzJBT9wQY+m1/wAvQ6zmnNOb6e5f+AR/1P8A/9oACAEDAwE/IfqfE311DcGRu8utXWPtBUR/U1nRi8faNlK0V+dn9yqKLs9NYyrn+JeclAmDSOM6LxK7V+Dqfz/54G4NcfPm+Yi03+h/7+Yfs4B21BWFYN63mue1QAx3O2MfzLl9NRKf+Xm2NPsz4BnwDPgmJX044Qo+884+884+88w+8uL3/wCp/wD/2gAMAwEAAhEDEQAAEAAAAAAAAAAAAAAAAAAAAAAAAAAx4qBoQvAAAAAAAAAAAAAAAAAAAAAAKAAAAAAAAAAAAAAAAAAAAJJJJJJJJJJJJJJJJJJJJJJJABBAJIIAIIAIBIJIJIBIAJJIAIBAAAJABABJBJIAAJAJe+QABAIBAJJIIIAABBAAAAAAApAgAAAIBAAJIBIAAAAAAAAAAAAAAAAAIBJJJJJJJJJJJJJJJJJJJJJJJIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//aAAgBAQMBPxD6mr/UVwqqijdDfbMJiK1iK2BZEQsjLaB/CrdlKsiBk3AsaM01p2AjmmiF1p0C9DUhbHQzcZzjUd8uCIPVTElwyPSskso30+ATVR3IWEZ4Ux0Tu/bbR3CLBWoV509Qfthu0pWEIqpmnbUrp28MGNjLdf4me0qPCw0DmmjGZt/VRtao/TcmDUBmtS9Fd0fSQZKVwctT5InyRPkifJE+SJ8kT5InyRPkifJE+SJ8kT5InyRPkifJE+SJ8kT5InyRPkifJE+SJ8kT5InyRPkifJE+SJ8kT5InyRPkifJE+SJ8kT5InyRPkifJE+SJ8kT5InyRPkifJErZC0p48f1v08BuX6qNaqvAQZGFEmxdbC8RsoklWY2trTiJHEWgrR0ALYwFZnAFicJEShXBAFqr0CWF7zWhpq66kcVAjVBjgjXaCWllgU6g6X0mBkCxMiPW5T23qMgTeQFroXtje8BAdLKttso2wsTYDSJYkaAsK6HbW6OIib6ymrrEyacSwBjkvVaziAmEzwhYj2ScDBHRQHcolBrdKOYiNOGaMOdS1XWI+GNUobUNNaaZT23qWzGVF7spQboxBzPcs5Z0L4nA5hidm4KkWKA4uUxMobNJdv1qqgL3ZQANsbhpoiFYEVwAG1WGSzJV2ap0xajHAAtRoAOrLZaa3swVuXhUuIxiEabKNONx6pEZFIDSO2lR/wANZJSmbK+oRVbZIABqzrTKausREacP+Lfrfp4OgMiIG4hGvlWCB4WRVVRYJQFtOE8ZN4is2b2tbpYbTllPlpcFu52zIMdmocSztXUKiL0FjpRSlej7u0bjo/2LSjZVv6Q09FxNRr+mbl4Evft6tpg6cpySfApmh1Ht0G6mBpphK7EkdCusiYSOe1Ga8h6m0JHu6wu7KgMt1iHK7xSV9wX7o0EOzwFvoMh0PitIGrEshklSSm9qkUKaPqIozYGq0saIuvocQbgx2zlqLajn8aFhqodjQivBsGfvEFBsSMh3EAR6ovgGNuRHFuyJAFTTEIQ28ot7zYeXihCr1p2xkzUa4Ssn3/UcpeWDJs+xl5zh5RncVxVRUTaIGboMWgyzUBaIxApMXA0Ev7rM1XoCGtaHeoKcdlzYEXsJRMHNm49heCIyoAo2F9LXvxQPO4wM62pmAuneI/RUFHbUzEwpxA0plDjDaUXzmR1uGJAfe4oyetKPZRQ8OwG4Mp+KHFqai9cH8pDjAZKiLRkF5c4bE8MYloTXLaIDKfT57Q8vYZQ/QTOLzTLYVKITNLYatVdAziNSsNKioaByL6iQslE6Vt9sGvV/xb9b9PBfqT3gCWXK36Z3HjjOEFlHXQziIIqpAIYEc5cpLLggI4MwbhCzoU7yhDFQF/EoKxrQgw9zUXwSGrPdEDA9TUjrcPngFWpRjh53Au4JLeMOhmYVfxQot0bpVulXBjk6wsUwImET/PBVtdvBQBVDR4W/61XeYXFAkKoaaupfG3baJup4sNmFALTKj/Y3636eAfUF1bbUS1ea940NFUytQgrvEKYoQ8xorKC2CX5AVAZfMKwTO1uL44o5oKtZzDZGYWbuCtQQBBGYoZcu54agAKWxUprRliDVAhKs1GWUROPSOzY3e4BrRYzAAADj6OzGtq5c6bnD9XvOH6vecP1e84fq95w/V7zh+r3nD9XvOH6vecP1e84fq95w/V7zh+r3nD9XvOH6vecP1e84fq95w/V7zh+r3nD9XvOH6vecP1e84fq95w/V7zh+r3nD9XvOH6vecP1e84fq95w/V7zh+r3nD9XvOH6vecP1e84fq95w/V7zh+r3nD9XvOH6vecP1e84fq95w/V7zh+r3nD9XvOH6vecP1e84fq94ty21sK3/wDTf//aAAgBAgMBPxD6nv3rpGxhwnfjXX7xWAf7G/6hQDel/GvaW0l6aPz2Xx/MrGGwer0d42y2t/k6PTwdRL5+IKXnX9sRRvffvERzeRX283FXPZ75zKD9NCNn/L0KQP3nD9ScP1Jw/Ug39OUpbZyv8Tlf4nO/xBQHb/qf/9oACAEDAwE/EPqbBkRTDpcL0/Uy6ikFFGTkzsU1VkdVS6ZvRTYL9SxiNReFIDipMcy3cbkLPtYwjfPW/slmCzIWgv7oemLIdM30bmMFbHXp1eBEDCsxS8YwYa3ZQ9S9kbpFhvGLD6kBUUFwRi95fzzLAA6Gxocux7xIsU6ODaux69p0/QoVVVfH00Ml153v/l4l00iD+QnmL+p5i/qeav6iJHCfTl3xovL9+IADEEV2l9W/+p//2Q==">';

/* === ПОЗИЦИОНИРОВАНИЕ ВСПЛЫВАЮЩИХ ОКОН === */
$MESS['skyweb24.popuppro_POSITION_LEFT']='Выровнять по левому краю';
$MESS['skyweb24.popuppro_POSITION_RIGHT']='Выровнять по правому краю';
$MESS['skyweb24.popuppro_POSITION_TOP']='Выровнять по верхнему краю';
$MESS['skyweb24.popuppro_POSITION_BOTTOM']='Выровнять по нижнему краю';



$MESS['skyweb24.popuppro_PROP_CONDITION_PREVENT_PAGE_LABEL']='Процент прокрутки страницы';
$MESS['skyweb24.popuppro_TABCOND_PREVENT_PAGE_HINT']='Всплывающее окно будет показоно при достижении определенного <b>процента высоты страницы</b>.';
$MESS['skyweb24.popuppro_PROP_CONDITION_PRECENT_PAGE']='Процент прокрутки страницы';



/* === НОВОЕ ПОЗИЦИОНИРОВАНИЕ ВСПЛЫВАЮЩИХ ОКОН === */
$MESS['skyweb24.popuppro_POSITION_LeftTop']='Слева сверху';
$MESS['skyweb24.popuppro_POSITION_Top']='Сверху';
$MESS['skyweb24.popuppro_POSITION_RightTop']='Справа сверху';
$MESS['skyweb24.popuppro_POSITION_Right']='Справа';
$MESS['skyweb24.popuppro_POSITION_RightBottom']='Справа снизу';
$MESS['skyweb24.popuppro_POSITION_Bottom']='Снизу';
$MESS['skyweb24.popuppro_POSITION_LeftBottom']='Слева снизу';
$MESS['skyweb24.popuppro_POSITION_Left']='Слева';
$MESS['skyweb24.popuppro_POSITION_Center']='По центру';
$MESS['skyweb24.popuppro_POSITION_fixed']='Фиксировать окно при прокрутке?';
$MESS['skyweb24.popuppro_POSITION_fixed_HINT']='Позволяет <b>включить/выключить залипание</b> всплывающего окна при прокрутке страницы.';

$MESS['skyweb24.popuppro_POPUP_example']='Ваше всплывающее окно';


/* === ЭФФЕКТЫ ВСПЛЫВАЮЩИХ ОКОН === */
$MESS['skyweb24.popuppro_EFFECTS_SHOW_CLOSEBUTTON']='Отображать крестик вверху окна?';
$MESS['skyweb24.popuppro_EFFECTS_SHOW_CLOSEBUTTON_HINT']='<b>Добавляет крестик</b> в правый верхний угол всплывающего окна.<br>При нажатии закрывает всплывающее окно.';
$MESS['skyweb24.popuppro_EFFECTS_CLOSE_AUTOHIDE']='Закрывать окно при клике вне окна?';
$MESS['skyweb24.popuppro_EFFECTS_CLOSE_AUTOHIDE_HINT']='Позволяет <b>включать/выключать закрытие</b> всплывающего окна при <b>клике вне окна</b>, т.е. при клике на любую область затемненной страницы.';
$MESS['skyweb24.popuppro_EFFECTS_CLOSE_TEXTBOX']='Закрывать окно при клике на текст закрытия?';
$MESS['skyweb24.popuppro_EFFECTS_CLOSE_TEXTBOX_HINT']='Позволяет <b>включать/выключать закрытие</b> всплывающего окна при клике специальный <b>текст закрытия</b>.';
$MESS['skyweb24.popuppro_EFFECTS_CLOSE_TEXTAREA']='Текст закрытия окна';
$MESS['skyweb24.popuppro_EFFECTS_CLOSE_TEXTAREA_HINT']='Напишите специальный <b>текст закрытия</b> всплывающего окна, чтобы пользователь не хотел его закрыть!<br><br>Например, Нет, я хочу купить этот товар дороже!';

$MESS['skyweb24.popuppro_EFFECTS_SHOW']='Эффект появления окна';
$MESS['skyweb24.popuppro_EFFECTS_SHOW_HINT']='Выберите <b>анимацию появления</b> всплывающего окна из списка доступных анимаций.';
	$MESS['skyweb24.popuppro_EFFECT_show_none']='Без эффекта';
	$MESS['skyweb24.popuppro_EFFECT_show_fromBottom']='Движение снизу';
	$MESS['skyweb24.popuppro_EFFECT_show_fromUp']='Движение сверху';
	$MESS['skyweb24.popuppro_EFFECT_show_fromLeft']='Движение слева';
	$MESS['skyweb24.popuppro_EFFECT_show_fromRight']='Движение справа';
	
$MESS['skyweb24.popuppro_EFFECTS_HIDE']='Эффект скрытия окна';
$MESS['skyweb24.popuppro_EFFECTS_HIDE_HINT']='Выберите <b>анимацию исчезновения</b> всплывающего окна из списка доступных анимаций.';
	$MESS['skyweb24.popuppro_EFFECT_hide_none']='Без эффекта';
	$MESS['skyweb24.popuppro_EFFECT_hide_attenuation']='Затухание';

$MESS['skyweb24.popuppro_EFFECT_BACKGROUND_COLOR']='Цвет затемнения страницы';
$MESS['skyweb24.popuppro_EFFECT_BACKGROUND_COLOR_HINT']='Позволяет указать <b>цвет затемнения страницы</b>, который будет накладывать при появлении всплывающего окна.';
$MESS['skyweb24.popuppro_EFFECT_BACKGROUND_OPACITY']='Прозрачность затемнения';
$MESS['skyweb24.popuppro_EFFECT_BACKGROUND_OPACITY_HINT']='Позволяет указать <b>процент прозрачности</b> затемнения страницы.';



$MESS['skyweb24.popuppro_POPUP_IMGBLOCKTITLE']='Выберите изображение';
$MESS['skyweb24.popuppro_MESSAGE_UPDATE_SUCCESS'] = 'Настройки успешно изменены.';


$MESS['skyweb24.popuppro_CONDITIONS_SITEALL'] = 'На всех сайтах';
$MESS['skyweb24.popuppro_CONDITIONS_GROUPSUNREGISTER'] = 'Только незарегистрированные пользователи';
$MESS['skyweb24.popuppro_CONDITIONS_GROUPSFIRSTVISIT'] = 'Посетители впервые зашедшие на сайт';

$MESS['skyweb24.popuppro_YES'] = 'Да';
$MESS['skyweb24.popuppro_NO'] = 'Нет';

$MESS['skyweb24.popuppro_TIME_DAYS'] = 'дней';
$MESS['skyweb24.popuppro_TIME_HOURS'] = 'ч';
$MESS['skyweb24.popuppro_TIME_MINUTES'] = 'мин';
$MESS['skyweb24.popuppro_TIME_SECONDS'] = 'сек';


$MESS['skyweb24.popuppro_PROP_CONDITION_GROUP']='Группа условий';
$MESS['skyweb24.popuppro_PROP_CONDITION_GROUP_DEFAULT']='Выберите значение...';

$MESS['skyweb24.popuppro_PROP_CONDITION_ALL']='Все условия';
$MESS['skyweb24.popuppro_PROP_CONDITION_OR']='Любое из условий';

$MESS['skyweb24.popuppro_PROP_CONDITION_TRUE']='выполнено(ы)';
$MESS['skyweb24.popuppro_PROP_CONDITION_FALSE']='не выполнено(ы)';

$MESS['skyweb24.popuppro_PROP_CONDITION_LOGIC_AND']='И';
$MESS['skyweb24.popuppro_PROP_CONDITION_LOGIC_AND_NOT']='И НЕ';
$MESS['skyweb24.popuppro_PROP_CONDITION_LOGIC_OR']='ИЛИ';
$MESS['skyweb24.popuppro_PROP_CONDITION_LOGIC_OR_NOT']='ИЛИ НЕ';
$MESS['skyweb24.popuppro_PROP_CONDITION_LOGIC_EQUAL']='равен';
$MESS['skyweb24.popuppro_PROP_CONDITION_LOGIC_NOT']='не равен';

/* === ГРУППЫ УСЛОВИЙ === */
$MESS['skyweb24.popuppro_PROP_CONDITION_GROUP_BASIC']='Основные условия';
$MESS['skyweb24.popuppro_PROP_CONDITION_GROUP_ADDITIONAL']='Дополнительные условия';
$MESS['skyweb24.popuppro_PROP_CONDITION_GROUP_SPECIAL']='Специальные условия';
$MESS['skyweb24.popuppro_PROP_CONDITION_GROUP_SALE']='Условия для интернет-магазина';

$MESS['skyweb24.popuppro_PROP_CONDITION_SITES_LABEL']='Сайт';
$MESS['skyweb24.popuppro_PROP_CONDITION_SITES_TEXT']='Сайт';

$MESS['skyweb24.popuppro_PROP_CONDITION_DATE_LABEL']='Период показа';
$MESS['skyweb24.popuppro_PROP_CONDITION_DATE_TEXT_1']='Период показа';
$MESS['skyweb24.popuppro_PROP_CONDITION_DATE_TEXT_2']='с';
$MESS['skyweb24.popuppro_PROP_CONDITION_DATE_TEXT_3']='по';

$MESS['skyweb24.popuppro_PROP_CONDITION_USER_GROUP_LABEL']='Группа пользователей';
$MESS['skyweb24.popuppro_PROP_CONDITION_USER_GROUP_TEXT']='Группа пользователей';

$MESS['skyweb24.popuppro_PROP_CONDITION_SHOW_PAGE_LABEL']='Адрес страницы';
$MESS['skyweb24.popuppro_PROP_CONDITION_SHOW_PAGE_TEXT']='Адрес страницы';

$MESS['skyweb24.popuppro_PROP_CONDITION_SHOW_DOMAIN_LABEL']='Домен страницы';

$MESS['skyweb24.popuppro_PROP_CONDITION_AFTER_SHOW_COUNT_PAGES_LABEL']='Просмотр нескольких страниц';
$MESS['skyweb24.popuppro_PROP_CONDITION_AFTER_SHOW_COUNT_PAGES_TEXT_1']='При просмотре';
$MESS['skyweb24.popuppro_PROP_CONDITION_AFTER_SHOW_COUNT_PAGES_TEXT_2']='страниц';

$MESS['skyweb24.popuppro_PROP_CONDITION_AFTER_TIME_SECOND_LABEL']='Через несколько секунд';
$MESS['skyweb24.popuppro_PROP_CONDITION_AFTER_TIME_SECOND_TEXT_1']='Время просмотра сайта';
$MESS['skyweb24.popuppro_PROP_CONDITION_AFTER_TIME_SECOND_TEXT_2']='секунд';

$MESS['skyweb24.popuppro_PROP_CONDITION_AFTER_TIME_SECOND_PAGE_LABEL']='Через несколько секунд на странице';
$MESS['skyweb24.popuppro_PROP_CONDITION_AFTER_TIME_SECOND_PAGE_TEXT_1']='Время просмотра страницы';
$MESS['skyweb24.popuppro_PROP_CONDITION_AFTER_TIME_SECOND_PAGE_TEXT_2']='секунд';


$MESS['skyweb24.popuppro_PROP_CONDITION_TIME_INTERVAL_LABEL']='Часы показа';
$MESS['skyweb24.popuppro_PROP_CONDITION_TIME_INTERVAL_TEXT_1']='Ежедневно';
$MESS['skyweb24.popuppro_PROP_CONDITION_TIME_INTERVAL_TEXT_2']='с';
$MESS['skyweb24.popuppro_PROP_CONDITION_TIME_INTERVAL_TEXT_3']='до';


$MESS['skyweb24.popuppro_PROP_CONDITION_ANCHOR_VISIBLE_LABEL']='При достижении якоря';
$MESS['skyweb24.popuppro_PROP_CONDITION_ANCHOR_VISIBLE_TEXT_1']='Имя якоря a name =';


$MESS['skyweb24.popuppro_PROP_CONDITION_ON_CLICK_CLASS_LINK_LABEL']='При клике на ссылку';
$MESS['skyweb24.popuppro_PROP_CONDITION_ON_CLICK_CLASS_LINK_TEXT_1']='Класс ссылки a class =';


$MESS['skyweb24.popuppro_PROP_CONDITION_ALREADY_GOING_LABEL']='При попытке уйти с сайта';
$MESS['skyweb24.popuppro_PROP_CONDITION_ALREADY_GOING_TEXT_1']='При попытке уйти с сайта';


$MESS['skyweb24.popuppro_PROP_CONDITION_REPEAT_SHOW_LABEL']='Повторный показ';
$MESS['skyweb24.popuppro_PROP_CONDITION_REPEAT_SHOW_TEXT_1']='Повторный показ через';


$MESS['skyweb24.popuppro_PROP_CONDITION_DEVICE_TYPE_LABEL']='Тип устройства';
$MESS['skyweb24.popuppro_PROP_CONDITION_OS_TYPE_LABEL']='Операционная система';
$MESS['skyweb24.popuppro_PROP_CONDITION_BROWSER_TYPE_LABEL']='Браузер';


$MESS['skyweb24.popuppro_PROP_CONDITION_DAY_LABEL']='День недели';
$MESS['skyweb24.popuppro_PROP_CONDITION_DAY_TEXT_1']='Понедельник';
$MESS['skyweb24.popuppro_PROP_CONDITION_DAY_TEXT_2']='Вторник';
$MESS['skyweb24.popuppro_PROP_CONDITION_DAY_TEXT_3']='Среда';
$MESS['skyweb24.popuppro_PROP_CONDITION_DAY_TEXT_4']='Четверг';
$MESS['skyweb24.popuppro_PROP_CONDITION_DAY_TEXT_5']='Пятница';
$MESS['skyweb24.popuppro_PROP_CONDITION_DAY_TEXT_6']='Суббота';
$MESS['skyweb24.popuppro_PROP_CONDITION_DAY_TEXT_7']='Воскресенье';


$MESS['skyweb24.popuppro_PROP_CONDITION_CART_COUNT_LABEL']='Количество товаров в корзине';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_COUNT_TEXT_1']='В корзине';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_COUNT_TEXT_2']='товаров';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_COUNT_TEXT_3']='более либо равно';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_COUNT_TEXT_4']='менее';


$MESS['skyweb24.popuppro_PROP_CONDITION_CART_SUMM_LABEL']='Общая стоимость товаров в корзине';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_SUMM_TEXT_1']='Общая стоимость товаров в корзине';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_SUMM_TEXT_2']='более либо равна';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_SUMM_TEXT_3']='менее';


$MESS['skyweb24.popuppro_PROP_CONDITION_CART_PRODUCT_LABEL']='Товар в корзине';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_PRODUCT_TEXT_1']='Товар в корзине';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_PRODUCT_TEXT_2']='равен';
$MESS['skyweb24.popuppro_PROP_CONDITION_CART_PRODUCT_TEXT_3']='не равен';


$MESS['skyweb24.popuppro_PROP_CONDITION_SECTION_LABEL']='Товар из раздела';
$MESS['skyweb24.popuppro_PROP_CONDITION_SECTION_TEXT_1']='Раздел товара в корзине';
$MESS['skyweb24.popuppro_PROP_CONDITION_SECTION_TEXT_2']='равен';
$MESS['skyweb24.popuppro_PROP_CONDITION_SECTION_TEXT_3']='не равен';


$MESS['skyweb24.popuppro_HOUR']='Час';
$MESS['skyweb24.popuppro_DAY']='День';
$MESS['skyweb24.popuppro_WEEK']='Неделя';
$MESS['skyweb24.popuppro_MONTH']='Месяц';
$MESS['skyweb24.popuppro_YEAR']='Год';
$MESS['skyweb24.popuppro_SITES_ALL_NAME']='Все';



/* === ОБЩИЕ УСЛОВИЯ ПОКАЗА === */
$MESS['skyweb24.popuppro_COND_ACTIVE'] = 'Активность';
$MESS['skyweb24.popuppro_COND_SORT'] = 'Сортировка';
$MESS['skyweb24.popuppro_COND_SITES'] = 'Показывать на сайтах';
$MESS['skyweb24.popuppro_COND_SITES_ALL'] = 'На всех';
$MESS['skyweb24.popuppro_COND_PATHSSHOW'] = 'Маска включения показа по части URL адреса';


$MESS['skyweb24.popuppro_BUTTON_METRIC'] = 'Атрибут onclick кнопки';
$MESS['skyweb24.popuppro_BUTTON_METRIC_HINT'] = 'В данное поле <b>можно вставить код</b> цели Яндекс Метрики или Google Analytics.<br><br>Например, yaCounter40490205.reachGoal(...<br>Например, ga(...';

$MESS['skyweb24.popuppro_PERSON_HINT'] = 'Данное поле поддерживает персонализацию';
$MESS['skyweb24.popuppro_IMG_1_SRC_HINT'] = 'Загрузите или выберите ранее загруженное <b>изображение</b>.';
$MESS['skyweb24.popuppro_LINK_HREF_HINT'] = 'Укажите полный <b>адрес ссылки</b>, например, https://skyweb24.ru';
$MESS['skyweb24.popuppro_ACTION_CONTENT_LINK_HREF_HINT'] = 'Укажите полный <b>адрес ссылки</b>, на которую будет осуществляться переход при клике на кнопку, например, https://skyweb24.ru';
$MESS['skyweb24.popuppro_HREF_TARGET_HINT'] = 'Выберите <b>как будет открываться</b> ссылка.';
$MESS['skyweb24.popuppro_CONTENT_TITLE_HINT'] = 'В данном поле вы можете <b>написать любой текст</b>, а также использовать HTML разметку.';
$MESS['skyweb24.popuppro_SOCIAL_SERVICE_ID_VK_HINT'] = 'Укажите <b>ID вашей группы</b> в ВКонтакте.<br><br><b>Оставьте это поле пустым</b> если у вас нет аккаунта в данной соц сети.';
$MESS['skyweb24.popuppro_SOCIAL_SERVICE_ID_INST_HINT'] = 'Укажите <b>имя вашего аккаунта</b> в Instagram.<br><br><b>Оставьте это поле пустым</b> если у вас нет аккаунта в данной соц сети.';
$MESS['skyweb24.popuppro_SOCIAL_SERVICE_ID_ODNKL_HINT'] = 'Укажите <b>ID вашей группы</b> в Одноклассниках.<br><br><b>Оставьте это поле пустым</b> если у вас нет аккаунта в данной соц сети.';
$MESS['skyweb24.popuppro_INPUT_REQUIRED_HINT'] = 'Позволяет <b>включить/выключить обязательность</b> заполнения поля.';
$MESS['skyweb24.popuppro_INPUT_NAME_HINT'] = 'Данный текст будет <b>отображаться рядом</b> с полем.';
$MESS['skyweb24.popuppro_INPUT_PLECAHOLDER_HINT'] = 'Данный текст будет <b>отображаться подсказкой</b> в поле, до того как пользователь введет в него свой текст.';
$MESS['skyweb24.popuppro_INPUT_CONSENT_HINT'] = '<b>Выберите нужное соглашение</b> из списка соглашений вашего сайта.';
$MESS['skyweb24.popuppro_DOCUMENTATION']='Документация';


$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME']='Анимация целевой кнопки';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_NAME_HINT']='Выберите <b>анимацию</b> целевой кнопки из списка доступных анимаций.';

$MESS['skyweb24.popuppro_BUTTON_ANIMATION_TIME']='Скорость анимации целевой кнопки';
$MESS['skyweb24.popuppro_BUTTON_ANIMATION_TIME_HINT']='Выберите <b>скорость анимации</b> целевой кнопки из списка.';





$MESS['skyweb24.popuppro_BWP_ACTIVE']='Показ кнопки после закрытия окна';
$MESS['skyweb24.popuppro_BWP_ACTIVE_HINT']='Служит для <b>включения/выключения показа кнопки</b> после закрытия всплывающего окна.<br>При нажатии на данную кнопку всплывающее окно покажется вновь.';

$MESS['skyweb24.popuppro_BWP_ANIMATION'] = 'Анимация кнопки';
$MESS['skyweb24.popuppro_BWP_ANIMATION_HINT'] = '<b>Выберите анимацию кнопки</b> из предложенного списка.';

$MESS['skyweb24.popuppro_BWP_BACKGROUND'] = 'Цвет фона кнопки';
$MESS['skyweb24.popuppro_BWP_BACKGROUND_HINT'] = 'Позволяет указать <b>цвет фона кнопки</b>.';

$MESS['skyweb24.popuppro_BWP_ICON'] = 'Иконка кнопки';
$MESS['skyweb24.popuppro_BWP_ICON_HINT'] = 'Позволяет <b>выбрать иконку</b> которая будет отображаться на кнопке.<br>Для этого просто <b>укажите класс иконки</b> от сервиса FontAwesome.<br>Например, fa-check<hr>Посмотреть <b>список доступных иконок</b> можно по <a target="_blank" href="https://fontawesome.com/v4.7.0/icons/">адресу</a>';

$MESS['skyweb24.popuppro_BWP_ICON_COLOR'] = 'Цвет иконки';
$MESS['skyweb24.popuppro_BWP_ICON_COLOR_HINT'] = 'Позволяет указать <b>цвет иконки</b>.';

$MESS['skyweb24.popuppro_INTEG_CRM_ACTIVE'] = 'Сохранять заявку в Битрикс24?';
$MESS['skyweb24.popuppro_INTEG_CRM_ACTIVE_HINT'] = 'Позволяет <b>включить/выключить</b> сохранение заявки в Битрикс24.';

$MESS['skyweb24.popuppro_INTEG_CRM_SERVER'] = 'Портал Битрикс24';
$MESS['skyweb24.popuppro_INTEG_CRM_SERVER_HINT'] = '<b>Выберите</b> из указанного списка <b>портал Битрикс24</b>.<hr>Настроить список порталов можно в <a target="_blank" href="/bitrix/admin/settings.php?lang=ru&mid=skyweb24.popuppro&mid_menu=1">настройках модуля</a>.';

?>