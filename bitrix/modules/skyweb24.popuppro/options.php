<?
use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Config\Option,
	Bitrix\Main\Page\Asset,
    \Skyweb24\Popuppro\CrmServer;

Loc::loadMessages(__FILE__);
Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BR_ROOT.'/modules/main/options.php');

$module_id='skyweb24.popuppro';
\Bitrix\Main\Loader::includeModule($module_id);
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('sale');

Asset::getInstance()->addJs('/bitrix/js/'.$module_id.'/script.js');
define("FORM_CRM_DEFAULT_PATH", "/crm/configs/import/lead.php");


if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['saveCrm'] && check_bitrix_sessid())
{

    if ($_REQUEST['ajax'])
        CUtil::JSPostUnEscape();

    $arAdditionalAuthData = array();
    $lastUpdated = '';
    if (is_array($_REQUEST['CRM']))
    {
        foreach ($_REQUEST['CRM'] as $ID => $arCrm)
        {
            if (is_array($arCrm))
            {
                if ($arCrm['DELETED'] == "Y" && $arCrm['ID'] > 0)
                {
                    $result = CrmServer::Del($arCrm['ID']);
                    if($result){
                        echo \Bitrix\Main\Web\Json::encode([
                            "result" => "201",
                            "ID" => $arCrm['ID']
                        ]);
                    } else {
                        echo \Bitrix\Main\Web\Json::encode([
                            "result" => "false",
                        ]);
                    }


                }
                else if($arCrm['CHECK'] == "Y")
                {
                    $hash = CrmServer::getHash($arCrm['ID']);
                    $result = CrmServer::Check([
                            "ID" => $arCrm['ID'],
                            "HASH" => $hash,
                            "URL" => $arCrm['URL']
                    ]);

                    if($result['error'] == "201"){
                        echo \Bitrix\Main\Web\Json::encode([
                            "result" => "ok",
                            "error" => GetMessage("skyweb24.popuppro_CRM_SERVER_SUCCESS_CONNECTION")
                        ]);
                    } else {
                        echo \Bitrix\Main\Web\Json::encode([
                                "result" => $result['error'],
                                "error" => $result['error_message']
                        ]);
                    }

                }
                else
                {
                    $arCrmFields = array(
                        'NAME' => trim($arCrm['NAME']),
                        'URL' => trim($arCrm['URL']),
                        'LOGIN' => trim($arCrm['LOGIN']),
                        'PASSWORD' => trim($arCrm['PASSWORD']),
                    );
                    if ($arCrm['ID'] <= 0)
                    {
                        $check = CrmServer::Check($arCrmFields);

                        if($check['error'] == "201") {
                            $arCrmFields['AUTO_HASH'] = $check['AUTH'];
                            $result = CrmServer::Add($arCrmFields);
                            $result['result'] = "201";
                            echo \Bitrix\Main\Web\Json::encode($result);
                        } else {
                            echo \Bitrix\Main\Web\Json::encode([
                                "result" => $check['error'],
                                "error" => $check['error_message']
                            ]);
                        }

                    }
                    else
                    {
                        $arCrmFields['ID'] = $arCrm['ID'];
                        if(!empty($arCrmFields['PASSWORD']) AND !empty($arCrmFields['LOGIN'])){
                            $check = CrmServer::Check($arCrmFields);
                        }
                        if(isset($check)){
                            if($check['error'] == "201") {
                                $arCrmFields['HASH'] = CrmServer::getHash($arCrm['ID']);
                                $result = CrmServer::Update($arCrmFields);
                                echo $result;
                            } else {
                                echo \Bitrix\Main\Web\Json::encode([
                                    "result" => $check['error'],
                                    "error" => $check['error_message']
                                ]);
                            }
                        } else {
                            $result = CrmServer::Update($arCrmFields);
                            echo $result;
                        }


                    }

                }
            }
        }
    }

    exit();
}

$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();
$aTabs = [
	[
		"DIV" => "sw24_general_settings_main",
		"TAB" => Loc::getMessage("skyweb24.popuppro_GENERAL_MAIN"),
		"TITLE" => Loc::getMessage("skyweb24.popuppro_GENERAL_MAIN_TITLE"),
		"OPTIONS"=>[
			['popup_active', Loc::getMessage("skyweb24.popuppro_PARAM_ACTIVE"), '', ['checkbox']],
			['popup_fontawesome_active', Loc::getMessage("skyweb24.popuppro_FONTAWESOME_ACTIVE"), '', ['checkbox']],
		]
	],
    [
        "DIV" => "sw24_general_settings_crm",
        "TAB" => Loc::getMessage("skyweb24.popuppro_CRM_OPTION"),
        "TITLE" => Loc::getMessage("skyweb24.popuppro_CRM_OPTION"),
        "OPTIONS"=>[
            ['crm_active', "ACTIVE", '', ['checkbox']],
        ]
    ],
];
if($request->isPost() && $request['Update'] && check_bitrix_sessid()){
	foreach($aTabs as $aTab){
		if(!empty($aTab['OPTIONS'])){
			__AdmSettingsSaveOptions($module_id, $aTab['OPTIONS']);
		}
	}
}
?><form class="multiparser_settings" method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($module_id)?>&amp;lang=<?=LANGUAGE_ID?>"><?
$tabControl = new CAdminTabControl("tabControl_sw24", $aTabs);
$tabControl->Begin();
?>

<?
$tabControl->BeginNextTab();
$notifyOverduechecked = (Bitrix\Main\Config\Option::get($module_id, 'popup_active')=='Y')?' checked="checked"':'';
$fontAwesomeActive = (Bitrix\Main\Config\Option::get($module_id, 'popup_fontawesome_active') == 'Y')?' checked="checked"':'';
?>

    <tr><td>
    <table width="100%">
        <tr class="heading">
            <td colspan="2"><?=GetMessage("skyweb24.popuppro_PARAM_MAIN")?></td>
        </tr>
        <tr>
            <td width="50%" class="adm-detail-content-cell-l"><?=GetMessage("skyweb24.popuppro_PARAM_ACTIVE")?></td>
            <td width="50%" class="adm-detail-content-cell-r"><input type="checkbox" id="popup_active" name="popup_active" value="Y"<?=$notifyOverduechecked?>></td>
        </tr>
        <tr>
            <td width="50%" class="adm-detail-content-cell-l"><?=GetMessage("skyweb24.popuppro_FONTAWESOME_ACTIVE")?></td>
            <td width="50%" class="adm-detail-content-cell-r"><input type="checkbox" id="popup_fontawesome_active" name="popup_fontawesome_active" value="Y"<?=$fontAwesomeActive?>></td>
        </tr>
        </table>
    </td></tr>







    <? $tabControl->BeginNextTab(); ?>
    <? CJSCore::Init(array('popup', 'ajax')); ?>
    <?
        $arCRMServers = CrmServer::getList();
    ?>


    <tr class="heading">
        <td valign="top" align="center" colspan="2"><b><?=GetMessage('FORM_TAB_CRM_SECTION_TITLE')?></b></td>
    </tr>
    <tr>
        <td colspan="2">
            <style>
                .form-crm-settings {width: 300px;}
                .form-crm-settings table {width: 100%;}
                .form-crm-settings table td {padding: 4px;}
                .form-crm-settings, .form-crm-settings table {font-size: 11px;}
                .form-crm-settings-hide-auth .form-crm-auth {display: none;}
                .form-crm-settings input {width: 180px;}
                .form-action-button {display: inline-block; height: 17px; width: 17px;}
                .action-edit {background: scroll transparent url(/bitrix/images/form/options_buttons.gif) no-repeat 0 0; }
                .action-delete {background: scroll transparent url(/bitrix/images/form/options_buttons.gif) no-repeat -29px 0; }
            </style>
            <table class="internal" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="crm_table">
                <thead>
                <tr class="heading">
                    <td><?=GetMessage('FORM_TAB_CRM_ROW_TITLE');?></td>
                    <td><?=GetMessage('FORM_TAB_CRM_ROW_URL');?></td>
                    <td><?=GetMessage('FORM_TAB_CRM_ROW_AUTH');?></td>
                    <td width="34"></td>
                </tr>
                </thead>
                <tbody>
                <? if (count($arCRMServers) > 0): ?>
                    <? foreach ($arCRMServers as $server) : ?>
                    <tr data-id="<?=$server['id'];?>" data-url="<?=$server['url'];?>" data-name="<?=$server['name'];?>">
                        <td align="center"><?=$server['name'];?></td>
                        <td align="center"><?=$server['url'];?></td>
                        <td align="center" id="crm_auth_cell_<?=$server['id'];?>"><a href="javascript: void(0)" onclick="CRMCheck(<?=$server['id'];?>, '<?=$server["url"];?>')"><?=GetMessage("FORM_TAB_CRM_CHECK")?></a></td>
                        <td align="center">
                            <a href="javascript: void(0)" onclick="CRMupdate(<?=$server['id'];?>)" class="form-action-button action-edit" title="редактировать"></a>
                            <a href="javascript: void(0)" onclick="CRMDelete(<?=$server['id'];?>);" class="form-action-button action-delete" title="удалить"></a></td>
                    </tr>
                    <? endforeach; ?>
                <? endif; ?>

                </tbody>
                <tfoot>
                <tr>
                    <td colspan="4" align="left"><input type="button" onclick="CRM(); return false;" value="<?=htmlspecialcharsbx(GetMessage('FORM_TAB_CRM_ADD_BUTTON'));?>"></td>
                </tr>
                </tfoot>
            </table>
        </td>
    </tr>

    <script type="text/javascript">
        function _showPass(el)
        {
            el.parentNode.replaceChild(BX.create('INPUT', {
                props: {
                    type: el.type == 'text' ? 'password' : 'text',
                    name: el.name,
                    value: el.value
                }
            }), el);
        }
        function CRMupdate(id) {
            CRM({
                ID: id,
                NAME: document.querySelectorAll("tr[data-id='"+id+"']")[0].getAttribute("data-name"),
                URL: document.querySelectorAll("tr[data-id='"+id+"']")[0].getAttribute("data-url"),
            })
        }
        function CRM(data)
        {
            var popup_id = Math.random();

            data = data || {ID:'new_' + popup_id}

            if (data && data.URL)
            {
                var r = /^(http|https):\/\/([^\/]+)(.*)$/i,
                    res = r.exec(data.URL);
                if (!res)
                {
                    var proto = data.URL.match(/\.bitrix24\./) ? 'https' : 'http';

                    data.URL = proto + '://' + data.URL;
                    res = r.exec(data.URL);
                }

                if (res)
                {
                    data.URL_SERVER = res[1]+'://'+res[2];
                    data.URL_PATH = res[3];
                }
            }

            if (!data.AUTH_HASH)
            {
                var content = '<div class="form-crm-settings"><form name="form_'+popup_id+'"><table cellpadding="0" cellspacing="2" border="0"><tr><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_TITLE'))?>:</td><td><input type="text" name="NAME" value="'+BX.util.htmlspecialchars(data.NAME||'')+'"></td></tr><tr><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_FORM_URL_SERVER'))?>:</td><td><input type="text" name="URL_SERVER" value="'+BX.util.htmlspecialchars(data.URL_SERVER||'')+'"></td></tr><tr><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_FORM_URL_PATH'))?>:</td><td><input type="text" name="URL_PATH" value="'+BX.util.htmlspecialchars(data.URL_PATH||'<?=FORM_CRM_DEFAULT_PATH?>')+'"></td></tr><tr><td colspan="2" align="center"><b><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_AUTH'))?></b></td></tr><tr><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_AUTH_LOGIN'))?>:</td><td><input type="text" name="LOGIN" value="'+BX.util.htmlspecialchars(data.LOGIN||'')+'"></td></tr><tr><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_AUTH_PASSWORD'))?>:</td><td><input type="password" name="PASSWORD" value="'+BX.util.htmlspecialchars(data.PASSWORD||'')+'"></td></tr><tr><td></td><td><a href="javascript:void(0)" onclick="_showPass(document.forms[\'form_'+popup_id+'\'].PASSWORD); BX.hide(this.parentNode);"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_AUTH_PASSWORD_SHOW'))?></a></td></tr></table></form></div>';
            }
            else
            {
                var content = '<div class="form-crm-settings form-crm-settings-hide-auth" id="popup_cont_'+popup_id+'"><form name="form_'+popup_id+'"><table cellpadding="0" cellspacing="2" border="0"><tr><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_TITLE'))?>:</td><td><input type="text" name="NAME" value="'+BX.util.htmlspecialchars(data.NAME||'')+'"></td></tr><tr><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_FORM_URL_SERVER'))?>:</td><td><input type="text" name="URL_SERVER" value="'+BX.util.htmlspecialchars(data.URL_SERVER||'')+'"></td></tr><tr><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_FORM_URL_PATH'))?>:</td><td><input type="text" name="URL_PATH" value="'+BX.util.htmlspecialchars(data.URL_PATH||'<?=FORM_CRM_DEFAULT_PATH?>')+'"></td></tr><tr class="form-crm-auth"><td colspan="2" align="center"><b><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_AUTH'))?></b></td></tr><tr class="form-crm-auth"><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_AUTH_LOGIN'))?>:</td><td><input type="text" name="LOGIN" value="'+BX.util.htmlspecialchars(data.LOGIN||'')+'"></td></tr><tr class="form-crm-auth"><td align="right"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_AUTH_PASSWORD'))?>:</td><td><input type="password" name="PASSWORD" value="'+BX.util.htmlspecialchars(data.PASSWORD||'')+'"></td></tr><tr><td align="right"></td><td><a href="javascript:void(0)" onclick="_showPass(document.forms[\'form_'+popup_id+'\'].PASSWORD);BX.hide(this);" class="form-crm-auth"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_AUTH_PASSWORD_SHOW'))?></a><a href="javascript:void(0)" onclick="BX.removeClass(BX(\'popup_cont_'+popup_id + '\'), \'form-crm-settings-hide-auth\'); BX.hide(this);"><?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_ROW_AUTH_SHOW'))?></a></td></tr></table></form></div>';
            }

            var wnd = new BX.PopupWindow('popup_' + popup_id, window, {
                titleBar: {content: BX.create('SPAN', {text: !isNaN(parseInt(data.ID)) ? '<?=CUtil::JSEscape(GetMessage('FORM_CRM_TITLEBAR_EDIT'))?>' : '<?=CUtil::JSEscape(GetMessage('FORM_CRM_TITLEBAR_NEW'))?>'})},
                draggable: true,
                autoHide: false,
                closeIcon: true,
                closeByEsc: true,
                content: content,
                buttons: [
                    new BX.PopupWindowButton({
                        text : BX.message('JS_CORE_WINDOW_SAVE'),
                        className : "popup-window-button-accept",
                        events : {
                            click : function(){CRMSave(wnd, data, document.forms['form_'+popup_id])}
                        }
                    }),
                    new BX.PopupWindowButtonLink({
                        text : BX.message('JS_CORE_WINDOW_CANCEL'),
                        className : "popup-window-button-link-cancel",
                        events : {
                            click : function() {wnd.close()}
                        }
                    })
                ]
            });

            wnd.show();
        }


        function CRMRedraw(data)
        {

            var table = BX('crm_table').tBodies[0];

            // while (table.rows.length > 0)
            //     table.removeChild(table.rows[0]);

            var tr = table.insertRow(-1);

            tr.id = 'crm_row_' + data.ID;
            tr.setAttribute("data-id", data.ID);
            tr.setAttribute("align", "center");

            tr.insertCell(-1).appendChild(document.createTextNode(data.NAME||'<?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_UNTITLED'))?>'));
            tr.insertCell(-1).appendChild(document.createTextNode(data.URL));

            var authCell = tr.insertCell(-1);
            authCell.id = 'crm_auth_cell_' + data.ID;

            //if (data.AUTH_HASH)
            //{
            //
            //    authCell.appendChild(BX.create('A', {
            //        props: {BXCRMID: data.ID},
            //        attrs: {href: 'javascript: void(0)'},
            //        events: {click: function() {CRMCheck(this.BXCRMID)}},
            //        text: '<?//=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_CHECK'))?>//'
            //    }));
            //}
            //else
            //{
            // authCell.appendChild(document.createTextNode('OK'));
            //}


            CRMCheck(data.ID, data.URL);

            BX.adjust(tr.insertCell(-1), {
                children: [
                    BX.create('A', {
                        props: {
                            className: 'form-action-button action-edit',
                            title: '<?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_EDIT'))?>'
                        },

                        attrs: {href: 'javascript: void(0)'},
                        events: {click: BX.delegate(function() {CRM(this);}, data)}
                    }),
                    BX.create('A', {
                        props: {
                            BXCRMID: data.ID,
                            className: 'form-action-button action-delete',
                            title: '<?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_DELETE'))?>'
                        },
                        attrs: {href: 'javascript: void(0)'},
                        events: {
                            click: function(e) {
                                CRMDelete(this.BXCRMID);
                            }
                        }
                    })
                ]
            });

        }

        function CRMSave(wnd, data_old, form)
        {
            var URL = form.URL_SERVER.value;
            if (URL.substring(URL.length-1,1) != '/' && form.URL_PATH.value.substring(0,1) != '/')
                URL += '/';
            URL += form.URL_PATH.value;

            var flds = ['ID', 'NAME', 'URL', 'ACTIVE','LOGIN','PASSWORD'],
                data = {
                    ID: data_old.ID,
                    NAME: form.NAME.value,
                    URL:  URL,
                    ACTIVE: 'Y', //form.ACTIVE.checked ? 'Y' : 'N',
                    LOGIN: !!form.LOGIN ? form.LOGIN.value : '',
                    PASSWORD: !!form.PASSWORD ? form.PASSWORD.value : ''
                };


            var res = false, r = /^(http|https):\/\/([^\/]+)(.*)$/i;
            if (data.URL)
            {
                res = r.test(data.URL);
                if (!res)
                {
                    var proto = data.URL.match(/\.bitrix24\./) ? 'https' : 'http';
                    data.URL = proto + '://' + data.URL;
                    res = r.test(data.URL);
                }
            }

            if (!res)
            {
                alert('<?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_WRONG_URL'))?>');
            }
            else
            {
                var query_str = '';

                for (var i = 0; i < flds.length; i++)
                {
                    query_str += (query_str == '' ? '' : '&') + 'CRM['+data.ID+']['+flds[i]+']='+BX.util.urlencode(data[flds[i]]);
                }

                BX.ajax({
                    method: 'POST',
                    dataType: 'json',
                    url: '<?=CUtil::JSEscape($APPLICATION->GetCurPageParam('saveCrm=Y&ajax=Y&'.bitrix_sessid_get()))?>',
                    data: query_str,
                    onsuccess: function (data) {

                        if(data.OLD_ID){
                            document.querySelectorAll("tr[data-id='"+data.OLD_ID+"']")[0].remove();
                        }

                        if(data.result == "201"){
                            CRMRedraw(data);
                        } else {
                            alert(data.error);
                        }

                    }
                });

                if (!!wnd)
                    wnd.close();
            }
        }

        function CRMDelete(ID)
        {
            if (confirm('<?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_CONFIRM'))?>'))
            {
                var data = {
                    CRM:{
                        ID:{
                            DELETED: "Y",
                            ID: ID
                        }
                    }
                }
                BX.ajax({
                    method: 'POST',
                    dataType: 'json',
                    url: '<?=CUtil::JSEscape($APPLICATION->GetCurPageParam('saveCrm=Y&ajax=Y&'.bitrix_sessid_get()))?>',
                    data: data,
                    onsuccess: function (data) {
                        document.querySelectorAll("tr[data-id='"+data.ID+"']")[0].remove();
                    }
                });
                return false;
            }
        }

        function CRMCheck(ID, URL)
        {

            var c = BX('crm_auth_cell_' + ID);
            if (c)
            {
                c.innerHTML = '<?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_CHECK_LOADING'))?>';
            }
            var data = {
                CRM:{
                    ID:{
                        CHECK: "Y",
                        URL: URL,
                        ID: ID
                    }
                }
            }
            BX.ajax({
                method: 'POST',
                dataType: 'json',
                url: '<?=CUtil::JSEscape($APPLICATION->GetCurPageParam('saveCrm=Y&ajax=Y&'.bitrix_sessid_get()))?>',
                data: data,
                onsuccess: function (res) {
                    if (!!res)
                    {
                        if (res.result == 'ok')
                        {
                            BX('crm_auth_cell_' + ID).innerHTML = 'OK';
                        }
                        else
                        {
                            BX('crm_auth_cell_' + ID).innerHTML = '<?=CUtil::JSEscape(GetMessage('FORM_TAB_CRM_CHECK_ERROR'))?>'.replace('#ERROR#', res.error||'');
                        }
                    }
                }
            });
            return false;

        }

    </script>




	<? $tabControl->Buttons(); ?>
    <input type="submit" name="Update" class="adm-btn-save" value="<?=Loc::getMessage("MAIN_SAVE")?>">
    <input type="reset" name="reset" value="<?=Loc::getMessage("MAIN_RESET")?>">

    <?=bitrix_sessid_post();?>

<? $tabControl->End(); ?>
</form>
