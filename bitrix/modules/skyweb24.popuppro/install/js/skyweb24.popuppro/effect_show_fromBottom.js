skyweb24_effects.effects.fromBottom=function(){
	this.targetO.style.opacity=0;
	this.targetO.style.display='block';
	this.showChildren();
	var tmpPosObject=this.targetO.getBoundingClientRect(),
		startO={top:document.body.scrollHeight, opacity:0},
		finishO={top:(tmpPosObject.top + pageYOffset), opacity:100},
		currentO=this.targetO,
		anim = new BX.easing({
			duration : 800,
			start : startO,
			finish : finishO,
			transition : BX.easing.transitions.linear,
			step : function(state){
				currentO.style.opacity = state.opacity/100;
				currentO.style.top = state.top+'px';
			},
			complete : function() {}
		});
		anim.animate();
		
}