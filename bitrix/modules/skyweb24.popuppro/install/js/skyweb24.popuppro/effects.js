skyweb24_effects={
	targetO:'',
	show:function(obj, typeShow){
		this.targetO=obj.popupContainer;
		this.popup=obj;
		if(!typeShow || !this.effects[typeShow]){
			this.showChildren();
		}else{
			this.effects[typeShow].call(this);
		}
	},
	showChildren:function(){
		elems=this.targetO.childNodes;
		if(this.targetO){
			for(var i=0; i<elems.length; i++){
				elems[i].style.opacity=1;
			}
		}
	},
	effects:{}
}


skyweb24_buttonAnimation = {
	setButtonAnimation: function(buttonAnimation, buttonAnimationTime){
		if(buttonAnimation && buttonAnimation != "none" ){
			let button = document.querySelectorAll(".sw24-button-animate")[0];
			button.classList.add(buttonAnimation);
			button.classList.add("animated");
			button.classList.add("infinite");
			button.classList.add(buttonAnimationTime);
		}
	},
}

skyweb24_windowAnimation = {
	show: function(nameAnimation) {
		
		setTimeout(() => {

			var windowAnimation = document.querySelectorAll(".popup-window")[0];
			if( !nameAnimation ){
				nameAnimation = 'none';
			}

			if( windowAnimation ){
							
				windowAnimation.classList.remove("animated");
				windowAnimation.classList.add(nameAnimation);
				windowAnimation.classList.add("animated");

				setTimeout(() => {
					windowAnimation.classList.remove(nameAnimation);
				}, 1000);

			}	
	
		}, 300);

		
	},
	
	hide: function(nameAnimation){
		var windowAnimation = document.querySelectorAll(".popup-window")[0];

		if( !nameAnimation ){
			nameAnimation = 'none';
		}

		if( windowAnimation ){

			windowAnimation.classList.remove("animated");
			windowAnimation.classList.add(nameAnimation);
			windowAnimation.classList.add("animated");

		}
	}
}