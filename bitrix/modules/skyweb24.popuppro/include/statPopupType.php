<?
    use Bitrix\Main\Localization\Loc;
    Loc::loadMessages(__FILE__);

    $editableWindow=new popupproEdit();
    $CountCreateTypePopups = $editableWindow -> getCountTypePopups();
    $typePopups = $editableWindow -> getTypes();



    // combine array
    foreach ($typePopups as $key => $typePopup) {
        $typePopups[$key]['create_count'] = ($CountCreateTypePopups[$key]['count']) ? $CountCreateTypePopups[$key]['count'] : 0 ;    
    }
    
    $arrThemeFlexItems = [
        "banner"    => 'Beniukon-Bronze',
        "video"     => 'NYC-Taxi',
        "action"    => 'Turquoise-Topaz',
        "social"    => 'Gloomy-Purple',
        "contact"   => 'Algal-Fuel',
        "share"     => 'Gloomy-Purple',
        "html"      => 'Innuendo',
        "age"       => 'Blue-Horizon',
        "coupon"    => 'Royal-Blue',
        "roulette"  => 'Desire',
        "discount"  => 'Boyzone',
    ];
?>

<? if($typePopups AND !$_REQUEST['id']) : ?>
    <div class="soFlexItems popuppro">  
        <? foreach ($typePopups as $key => $typePopup) : ?>
            <? if(!$typePopup['create_count']) continue; ?>
            <? $countStatShow = $editableWindow  -> getCountStatShow($typePopup['code']); ?>
            <? $countStatAction = $editableWindow  -> getCountStatAction($typePopup['code']); ?>

            <?
            if(!$countStatShow) {
                $countStatShow = 0;
            }
            if(!$countStatAction){
                $countStatAction = 0;
            }

            $conversion = 0;
            if( $countStatShow != 0 AND $countStatAction != 0 ){
                $conversion = round((int)$countStatAction / (int)$countStatShow * 100, 1 );
            }
            ?>

            <article class="<?=$arrThemeFlexItems[$typePopup['code']];?>">
                <div>
                    <header><a target="_blank" href="<?=$typePopup['link_document'];?>"><?=$typePopup['name'];?></a></header>
                    <section><?=$typePopup['create_count']?></section>
                    <footer>
                        <div class="container stat-info">
                            <div title="<?=GetMessage("skyweb24.popuppro_TABLELIST_STAT_SHOW");?>" class="item"><img src="/bitrix/themes/.default/skyweb24.popuppro/images/eye.png" alt=""><span><?=number_format($countStatShow, 0, '', ' ');?></span></div>
                            <div title="<?=GetMessage("skyweb24.popuppro_TABLELIST_STAT_ACTION");?>" class="item"><img src="/bitrix/themes/.default/skyweb24.popuppro/images/target.png" alt=""><span><?=$countStatAction;?></span></div>
                            <div title="<?=GetMessage("SKWB24_TITLE_ICON_CONVERSION");?>" class="item"><img src="/bitrix/themes/.default/skyweb24.popuppro/images/funnel.png" alt=""><span><?=$conversion?>%</span></div>
                        </div>
                    </footer>
                </div>
            </article>
        <? endforeach; ?>   
    </div>
<? endif; ?>

