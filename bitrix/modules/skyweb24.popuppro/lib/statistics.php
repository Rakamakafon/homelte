<?
namespace Skyweb24\Popuppro;
use Bitrix\Main\Mail\Event,
	Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
class Statistics{
	
	/**
	* @param int $idPopup id current popup
	* @param string $type type statistic:
	*	stat_show - popup is open,
	*	stat_action - in popup target action occurred,
	*	stat_time - popup close (set time show and type close)
	* @param array $request[
	*	int popupTime - popup show time (second),
	*	string closeType[
	*		closeWindow - the user close browser window
	*		closeByEsc - the user push escape key
	*		closeByTextButton the user click textButoon in popup
	*		closeByPopupIcon - the user click BX.PopupWindow.closeIcon
	*		closeByPopupOveray - - the user click in BX.PopupWindow.closeIcon
	*	] - type of close popup,
	*	string site_url - domain name,
	*	string url - url where popup is show
	* ]
	*/
	private $tableStat;
	
	public function __construct($idPopup, $type, $request){
		
		$this->tableStat='skyweb24_popuppro_stat';
		
		$this->id=$idPopup;
		
		$detect= new Detectdevice;
		$this->os=$detect->getOS();
		$this->browser=$detect->getBrowser();
		$this->device=$detect->getDevice();
		$this->remoteIp=$_SERVER['REMOTE_ADDR'];
		
		if($type=='stat_show'){
			$this->openPopup($request);
		}elseif($type=='stat_action'){
			$this->setActionPopup($request);
		}elseif($type=='stat_time'){
			$this->setClosePopup($request);
		}
		
	}
	
	public function setClosePopup($request){
		global $DB;
		$fields=[
			'id_popup'=>$this->id,
			'url'=>"'".$DB->ForSql($request['url'])."'",
			'site_url'=>"'".$DB->ForSql($request['site_url'])."'",
			'device'=>"'".$this->device."'",
			'os'=>"'".$this->os."'",
			'browser'=>"'".$this->browser."'",
			'ip'=>"'".$this->remoteIp."'",
			'show_time'=>(int) $request['popupTime'],
			'close_type'=>"'".$DB->ForSql($request['closeType'])."'"
		];
		$this->updateStatistic($fields);
		$this->removeCurrentStat();
	}
	
	public function setActionPopup($request){
		global $DB;
		$fields=[
			'id_popup'=>$this->id,
			'device'=>"'".$this->device."'",
			'os'=>"'".$this->os."'",
			'browser'=>"'".$this->browser."'",
			'ip'=>"'".$this->remoteIp."'",
			'target_action'=>'"Y"'
		];
		if(!empty($request['url'])){
			$fields['url']="'".$DB->ForSql($request['url'])."'";
		}
		if(!empty($request['url'])){
			$fields['site_url']="'".$DB->ForSql($request['site_url'])."'";
		}
		$this->updateStatistic($fields);
	}
	
	public function openPopup($request){
		global $DB;
		$id = $DB->Insert($this->tableStat, [
			'id_popup'=>$this->id,
			'url'=>"'".$DB->ForSql($request['url'])."'",
			'site_url'=>"'".$DB->ForSql($request['site_url'])."'",
			'device'=>"'".$this->device."'",
			'os'=>"'".$this->os."'",
			'browser'=>"'".$this->browser."'",
			'ip'=>"'".$this->remoteIp."'",
		], $err_mess.__LINE__);
		$this->setCurrentStat($id);
	}
	
	private function setCurrentStat($id){
		$_SESSION['sw24PopupCurrentStatId']=$id;
	}
	
	private function getCurrentStat(){
		return empty($_SESSION['sw24PopupCurrentStatId'])?0:$_SESSION['sw24PopupCurrentStatId'];
	}
	
	private function removeCurrentStat(){
		unset($_SESSION['sw24PopupCurrentStatId']);
	}
	
	private function updateStatistic($fields){
		global $DB;
		$currentStatId=$this->getCurrentStat();
		if($currentStatId==0){
			$DB->Insert($this->tableStat, $fields, $err_mess.__LINE__);
		}else{
			$DB->Update($this->tableStat, $fields,  "WHERE id=".$currentStatId, $err_mess.__LINE__);
		}
	}
	
}

?>