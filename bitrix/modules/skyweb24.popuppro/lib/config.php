<? 

    namespace Skyweb24\Popuppro;
    use \Bitrix\Main\Localization\Loc;
    Loc::loadMessages(__FILE__);

    class Config{

        public function getButtonAnimation(){
            $arAnimations = [
                'none'       => GetMessage("skyweb24.popuppro_NOT_EFFECTS"),
                'bounce'     => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_NAME_BOUNCE"),
                'flash'      => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_NAME_FLASH"),
                'pulse'      => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_NAME_PULSE"),
                'rubberBand' => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_NAME_RUBBERBAND"),
                'shake'      => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_NAME_SHAKE"),
                'swing'      => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_NAME_SWING"),
                'tada'       => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_NAME_TADA"),
                'wobble'     => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_NAME_WOBBLE"),
            ];
            return $arAnimations;
        }
        public function getTimeAnimationButton(){
            $arTimeAnimation = [
                'faster'  => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_TIME_FAST"),
                'normal'  => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_TIME_NORMAL"),
                'slow'    => GetMessage("skyweb24.popuppro_BUTTON_ANIMATION_TIME_SLOW"),
            ];
            return $arTimeAnimation;
        }
        public function getShowAnimationWindow(){
            $arAnimations = [
                'none'          => GetMessage("skyweb24.popuppro_NOT_EFFECTS"),
                'bounceIn'      => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEIN"),
                'bounceInDown'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEINDOWN"),
                'bounceInLeft'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEINLEFT"),
                'bounceInRight' => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEINRIGHT"),
                'bounceInUp'    => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEINUP"),

                'fadeInDownBig'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEINDOWNBIG"),
                'fadeInLeftBig'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEINLEFTBIG"),
                'fadeInRightBig' => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEINRIGHTBIG"),
                'fadeInUpBig'    => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEINUPBIG"),

                'rotateIn'          => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEIN"),
                'rotateInDownLeft'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEINDOWNLEFT"),
                'rotateInDownRight' => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEINDOWNRIGHT"),
                'rotateInUpLeft'    => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEINUPLEFT"),
                'rotateInUpRight'   => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEINUPRIGHT"),

                'zoomIn'      => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMIN"),
                'zoomInDown'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMINDOWN"),
                'zoomInLeft'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMINLEFT"),
                'zoomInRight' => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMINRIGHT"),
                'zoomInUp'    => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMINUP"),

                'rollIn'        => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROLLIN"),


            ];
            return $arAnimations;
        }
        public function getHideAnimationWindow(){
            $arAnimations = [
                'none'           => GetMessage("skyweb24.popuppro_NOT_EFFECTS"),
                'bounceOut'      => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUT"),
                'bounceOutDown'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUTDOWN"),
                'bounceOutLeft'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUTLEFT"),
                'bounceOutRight' => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUTRIGHT"),
                'bounceOutUp'    => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_BOUNCEOUTUP"),

                'fadeOutDownBig'   => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEOUTDOWNBIG"),
                'fadeOutLeftBig'   => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEOUTLEFTBIG"),
                'fadeOutRightBig'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEOUTRIGHTBIG"),
                'fadeOutUpBig'     => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_FADEOUTUPBIG"),

                'rotateOut'          => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUT"),
                'rotateOutDownLeft'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUTDOWNLEFT"),
                'rotateOutDownRight' => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUTDOWNRIGHT"),
                'rotateOutUpLeft'    => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUTUPLEFT"),
                'rotateOutUpRight'   => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROTATEOUTUPRIGHT"),

                'zoomOut'      => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUT"),
                'zoomOutDown'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUTDOWN"),
                'zoomOutLeft'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUTLEFT"),
                'zoomOutRight' => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUTRIGHT"),
                'zoomOutUp'    => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ZOOMOUTUP"),

                'rollOut'  => GetMessage("skyweb24.popuppro_ANIMATION_WINDOW_SHOW_ROLLOUT"),

            ];
            return $arAnimations;
        }
        public function getListCRMServer(){
            $CRMServers = CrmServer::getList();

            $result = array();
            if($CRMServers){
                foreach ($CRMServers as $server){
                    $result[$server['id']] = $server['name'];
                }
                return $result;
            }
            return false;

        }
        public function getExceptionsType($key, $prop){
            // method for exception "type template"
            $exceptions = [
                "banner" => [
                    "BUTTON_ANIMATION" => true,
                    "INTEGRATION" => true
                ],
                "video" => [
                    "BUTTON_ANIMATION" => true,
                    "INTEGRATION" => true
                ],
                "social" => [
                    "BUTTON_ANIMATION" => true,
                    "INTEGRATION" => true
                ],
                "share" => [
                    "BUTTON_ANIMATION" => true,
                    "INTEGRATION" => true
                ],
                "age" => [
                    "BUTTON_ANIMATION" => true,
                    "INTEGRATION" => true
                ],
                "html" => [
                    "BUTTON_ANIMATION" => true,
                    "INTEGRATION" => true
                ],
                "action" => [
                    "INTEGRATION" => true
                ],
                "roulette" => [
                    "INTEGRATION" => true
                ],
                "coupon" => [
                    "INTEGRATION" => true
                ],
            ];

            return $exceptions[$key][$prop];

        }
        
    }

?>