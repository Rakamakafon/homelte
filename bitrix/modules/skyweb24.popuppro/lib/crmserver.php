<?
namespace Skyweb24\Popuppro;
\Bitrix\Main\Loader::includeModule('iblock');
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class CrmServer{
    public function Check($data){
        global $DB;

        $url = $data['URL'];

        if(isset($data['HASH']) AND !empty($data['HASH'])){
            $hash = $data['HASH'];
        } else {
            $login = $data['LOGIN'];
            $password = $data['PASSWORD'];
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_POST, 1);
        if(!$hash){

            curl_setopt($ch, CURLOPT_POSTFIELDS, "method=lead.get_fields&LOGIN=".$login."&PASSWORD=".$password);
            $result = curl_exec($ch);
            $curl_header = curl_getinfo($ch);


            if(strpos($curl_header['content_type'], "application/json") !== false AND strpos($curl_header['content_type'], "application/json") >= 0 ){
                $result = str_replace("'", '"', $result);
                //  $result = iconv(LANG_CHARSET, "UTF-8", $result);
                $result = \Bitrix\Main\Web\Json::decode($result);
            } else {
                $result = [
                    "error" => "400",
                    "error_message" => Loc::getMessage('skyweb24.popuppro_CRM_SERVER_ERROR_CONNECTION')
                ];
            }


            curl_close($ch);
            return $result;

        }


        curl_setopt($ch, CURLOPT_POSTFIELDS, "method=lead.get_fields&AUTH=".$hash);
        $result = curl_exec($ch);
        $curl_header = curl_getinfo($ch);

        if(strpos($curl_header['content_type'], "application/json") !== false AND strpos($curl_header['content_type'], "application/json") >= 0 ){
            $result = str_replace("'", '"', $result);
            $result = \Bitrix\Main\Web\Json::decode($result);
        } else {
            $result = [
                "error" => "400",
                "error_message" => Loc::getMessage('skyweb24.popuppro_CRM_SERVER_ERROR_CONNECTION')
            ];
        }

        curl_close($ch);
        return $result;

    }
    public function Add($data){
            global  $DB;

            $hash = serialize([ "hash" => $data['AUTO_HASH'] ]);

            $url = $data['URL'];
            $name = $data['NAME'];

            $arFields = [
                "name" => "'".trim($name)."'",
                "url" => "'".trim($url)."'",
                "params" => "'".trim($hash)."'",
            ];

            unset($data["LOGIN"]);
            unset($data["PASSWORD"]);


            if($id = $DB -> Insert('skyweb24_popuppro_crmserver', $arFields)){
                $data['ID'] = $id;
            }

            return $data;


    }
    public function getList(){
        global $DB;
        $results = $DB -> Query("SELECT * FROM `skyweb24_popuppro_crmserver`");
        while ($row = $results->Fetch()) {
           $result[] = $row;
        }

        return $result;

    }
    public function getServer($ID){
        global $DB;
        $results = $DB -> Query("SELECT * FROM `skyweb24_popuppro_crmserver` WHERE `id` = $ID");
        while ($row = $results->Fetch()) {
            return $row;
        }
    }
    public function Del($ID){
        global $DB;
        $sql = "DELETE FROM `skyweb24_popuppro_crmserver` WHERE `id` = $ID";
        $results = $DB -> Query($sql);
        return $results -> result;

    }
    public function Update($data){
        global $DB;

        if($data['HASH']){
            $hash = serialize([ "hash" => $data['HASH'] ]);
            $arFields['params'] = "'".trim($hash)."'";
        }


        $url = $data['URL'];
        $name = $data['NAME'];

        $arFields = [
            "name" => "'".trim($name)."'",
            "url" => "'".trim($url)."'",
        ];

        unset($data["LOGIN"]);
        unset($data["PASSWORD"]);

        if($DB -> Update('skyweb24_popuppro_crmserver', $arFields, "WHERE ID='".$data['ID']."'")){
            $data['OLD_ID'] = $data['ID'];
            $data['result'] = "201";
            $result = \Bitrix\Main\Web\Json::encode($data);
        }

        return $result;
    }
    public function getHash($ID){
        global $DB;
        $results = $DB -> Query("SELECT `params` FROM `skyweb24_popuppro_crmserver` WHERE `id` = " . $ID);
        while ($row = $results->Fetch()) {
            $result = unserialize($row['params'])['hash'];
        }

        return $result;
    }
    public function sendCRMdata($data){

        $arConfigCRM = CrmServer::getServer($data['CRM_SERVER_ID']);

        $hash = unserialize($arConfigCRM['params'])['hash'];
        $url = $arConfigCRM['url'];

        $arFields = [
            "AUTH" => $hash,
            "SOURCE_DESCRIPTION" => $data['SOURCE_DESCRIPTION'],
            "SOURCE_ID"    => "WEB",
            "STATUS_ID"    => "NEW",
            "EMAIL_HOME"   => $data['EMAIL_HOME'],
            "TITLE"        => $data['TITLE'],
            "NAME"         => $data['NAME'],
            "COMMENTS"     => $data['COMMENTS'],
            "PHONE_MOBILE" => $data['PHONE_MOBILE'],
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $arFields);
        $result = curl_exec($ch);
        $curl_header = curl_getinfo($ch);

        if(strpos($curl_header['content_type'], "application/json") !== false AND strpos($curl_header['content_type'], "application/json") >= 0 ){
            $result = str_replace("'", '"', $result);
            $result = \Bitrix\Main\Web\Json::decode($result);
//            if($result['error'] == "201"){
//                echo \Bitrix\Main\Web\Json::encode([
//                    "result" => "ok"
//                ]);
//            }
        }
        curl_close($ch);
    }
}

?>