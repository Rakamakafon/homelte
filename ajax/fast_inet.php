<?php
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);
define("IBLOCK_ID", 32);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule('iblock');

$application = Application::getInstance();

$context = $application->getContext();
$request = $context->getRequest();

$props = array(
  'PERSONAL_PHONE' => $request->getPost("phone"),
  'PERSONAL_NAME' => $request->getPost("name"),
  'COMPLECT_NAME' => $request->getPost("complect"),
);

$el = new CIBlockElement;

$props = array(
  'PERSONAL_NAME'  => $request->getPost("name"),
  'PERSONAL_PHONE' => $request->getPost("phone"),
  'ADDRESS'        => $request->getPost("address"),
  'ROOM_TYPE'      => $request->getPost("place"),
  'SQUARE'         => $request->getPost("square"),
  'FLOOR_COUNT'    => $request->getPost("floors"),
  'ROOM_COUNT'     => $request->getPost("rooms"),
  'DEVICE_COUNT'   => $request->getPost("gadgets"),
  'DESIRED_SPEED'  => $request->getPost("speed"),
  'EXTRA_SERVICES' => $request->getPost("services")
);

$arLoadProductArray = Array(
  "IBLOCK_SECTION_ID" => false,
  "IBLOCK_ID"      => IBLOCK_ID,
  "PROPERTY_VALUES"=> $props,
  "NAME"           => time(),
  "ACTIVE"         => "Y"
);

if ($PRODUCT_ID = $el->Add($arLoadProductArray)):?>
  <div class="content" style="padding: 24px;">
        <div class="head-wrap">
            <h4>Отлично!</h4>
        </div>
        <div class="body">Ваша заявка успешно отправлена.<br> В ближайшее время с вами свяжутся</div>
    </div>
<?else:?>
  <div class="content" style="padding: 24px;">
        <div class="head-wrap">
            <h4>Ошибочка!</h4>
        </div>
        <div class="body">Попробуйте позже!</div>
    </div>
<?endif?>