<?php
define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);
?>
<div class="content" style="padding: 24px;">
    <div class="head-wrap">
        <h4>Заказать комплект оборудования</h4>
    </div>
    <div class="body">
    	<div class="form-wraper">
		    <div class="under-title">WiFi роутер Стандарт, Универсальный 4G-модем, SIM-карта, Пигтейлы, Антенна Стандарт, Кронштейн, Монтаж</div>
		    <div class="left-col">
		        <div class="price-calc-col">
		            <div class="price">15 900 ₽</div>
		        </div>
		        <div class="form">
		            <div class="form-control-custom">
		                <input type="text" name="name" placeholder="Ваше имя">
		            </div>
		            <div class="form-control-custom">
		                <input type="text" name="phone" placeholder="Номер телефона">
		            </div>
		            <button class="btn btn-primary call-me">Отправить</button>
		            <div class="">
		                <label class="checkbox agreement"><a>Я принимаю условия передачи
		                        информации</a><input type="checkbox" checked><span
		                                    class="checkmark"></span></label>
		            </div>
		        </div>
		        <div class="sub-descr">или позвоните нам по номеру <a href="tel:74952041633">+7 (495) 204-16-33</a></div>
		    </div>
		    <div class="right-col">
		    	<img class="order_complect" src="/include/Montazh.jpg">
		    </div>
		</div>
    </div>
</div>