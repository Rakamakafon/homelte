<?php
define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);
?>
<div class="content_city" style="padding: 24px;">
    <div class="head-wrap">
        <h4 class="under_title">

        </h4>
    </div>
    <div class="body">
        <div class="form-wraper">
                <div class="form">
                    <div class="form-control-custom">
                        <input type="text" name="name" class="inputtext intec-ui intec-ui-control-input intec-ui-mod-block intec-ui-mod-round-3 intec-ui-size-2" placeholder="Ваше имя">
                    </div>
                    <div class="form-control-custom">
                        <input type="text" name="phone" class="inputtext intec-ui intec-ui-control-input intec-ui-mod-block intec-ui-mod-round-3 intec-ui-size-2" placeholder="Номер телефона">
                    </div>
                    <button class="intec-ui intec-ui-control-button intec-ui-mod-round-3 intec-ui-scheme-current intec-ui-size-1 call-me">Отправить</button>
                </div>
        </div>
    </div>
</div>