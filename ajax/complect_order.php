<?php
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);
define("IBLOCK_ID", 36);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule('iblock');

$application = Application::getInstance();

$context = $application->getContext();
$request = $context->getRequest();

$props = array(
	'PERSONAL_PHONE' => $request->getPost("phone"),
  'PERSONAL_NAME' => $request->getPost("name"),
  'COMPLECT_NAME' => $request->getPost("complect"),
);

$el = new CIBlockElement;

$arLoadProductArray = Array(
  "IBLOCK_SECTION_ID" => false,
  "IBLOCK_ID"      => IBLOCK_ID,
  "PROPERTY_VALUES"=> $props,
  "NAME"           => time(),
  "ACTIVE"         => "Y",
  "DETAIL_TEXT"    => $request->getPost("desc"),
);

if ($PRODUCT_ID = $el->Add($arLoadProductArray)):?>
	<div class="content" style="padding: 24px;">
        <div class="head-wrap">
            <h4>Ваше заявка успешно отправлена</h4>
        </div>
        <div class="body">В ближайшее время мы с Вами свяжемся!</div>
    </div>
<?else:?>
	<div class="content" style="padding: 24px;">
        <div class="head-wrap">
            <h4>Ошибочка!</h4>
        </div>
        <div class="body">Попробуйте позже!</div>
    </div>
<?endif?>