<?php
use Bitrix\Main\Loader;
use Bitrix\Iblock\ElementTable;

define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);
define('TYPE_PROPERTY_CODE', 'PART_TYPE');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule('iblock');

$obTypes = ElementTable::getList(array(
    'select' => array('DETAIL_TEXT', 'DETAIL_PICTURE', 'NAME'),
    'filter' => array('ID' => $_POST['types'])
));
?>

<div class="content" style="padding: 24px;">
    <div class="head-wrap"></div>
    <div class="body">
        <div class="product-details-wrap">
            <div class="slider-wrap">
                <?while ($arTypes = $obTypes->fetch()):?>
                    <?if (!empty($arTypes['DETAIL_TEXT'])):?>
                        <div class="slide row-flex">
                            <div class="left-col">
                                <img src="<?=CFile::GetPath($arTypes['DETAIL_PICTURE'])?>" alt="">
                             </div>
                             <div class="right-col">
                                <div class="descr-line">
                                    <div class="title"><?=$arTypes['NAME']?></div>
                                    <div class="descr"><?=$arTypes['DETAIL_TEXT']?></div>
                                </div>
                             </div>
                         </div>
                    <?endif?>
                <?endwhile?>
            </div>
        </div>
    </div>
</div>