<?php
use Bitrix\Main\Loader;

define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule('iblock');

$obProduct = CIBlockElement::GetByID($_POST["product"]);
if ($arProduct = $obProduct->GetNext()):?>
	<div class="content" style="padding: 24px;">
        <div class="body"><?=$arProduct['DETAIL_TEXT']?></div>
    </div>
<?endif?>